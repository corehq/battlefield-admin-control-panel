<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/21/20, 11:52 AM
 */

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes(['verify' => true]);

/**
 * Protected API routes
 */
Route::group(['namespace' => 'Api', 'prefix' => 'api/auth'], static function () {
    Route::group(['namespace' => 'Battlefield'], static function () {

        Route::group(['prefix' => 'player'], static function () {
            Route::get('{player}/commandOverview', 'Player@commandUsage');
            Route::get('{player}/linked', 'Player@linkedAccounts');
            Route::get('{player}/history', 'Player@history');
            Route::get('{player}/aliases', 'Player@aliases');
            Route::get('{player}/bans', 'Player@bans');
            Route::get('{player}/infractions', 'Player@infractions');
        });

        Route::group(['prefix' => 'server'], static function () {
            Route::get('{server}/punishments', 'Server@punishments');
        });

        Route::group(['namespace' => 'Rcon', 'prefix' => 'rcon'], static function () {
            Route::group(['prefix' => 'admin'], static function () {
                Route::post('server/{server}/player/{player}/ban', 'Admin@ban')
                    ->middleware(['permission:admin.rcon.ban.perm|admin.rcon.ban.temp']);
            });
        });
    });
});

Route::group(['namespace' => 'Guest'], static function () {
    Route::group(['middleware' => 'guest'], static function () {
        Route::get('/', 'LandingController')->name('guest.home');
    });

    Route::get('/terms', 'LegalController@termsAndConditions')->name('terms');
    Route::get('/privacy', 'LegalController@privacy')->name('privacy');
});

Route::group(['namespace' => 'Auth'], static function () {
    Route::get('login', 'LoginController@displayLogin')->name('user.login');
    Route::post('login', 'LoginController@login')->name('user.login.post');
    Route::get('logout', 'LoginController@logout')->name('user.logout');
});

/**
 * 2FA Routes
 */


Route::get('otp', 'Auth\LoginController@showotp');
Route::post('validateotp', 'Auth\LoginController@validateotp');
Route::post('validate2fa', 'Auth\Otp\OtpController@validate2fa');


/**
 * Routes that should be behind authentication.
 */
Route::group(['middleware' => 'verified'], static function () {
    Route::get('dashboard', 'DashboardController')
        ->middleware(['permission:view.dashboard'])
        ->name('dashboard');
    Route::get('profile', 'User\ProfileController@index')->name('profile');
    Route::group(['prefix' => 'servers', 'namespace' => 'Battlefield'], static function () {
        Route::get('/', 'ServerController@index');
    });
    Route::post('disable2fa', 'Auth\Otp\OtpController@disable2fa');
    Route::get('enable2fa', 'Auth\Otp\OtpController@enable2fa');

    /**
     * Site Admin
     */
    Route::group(['prefix' => 'admin'], static function () {
        Route::group(['prefix' => 'system', 'middleware' => 'auth'], static function () {
            Route::get('logs',
                ['uses' => '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index', 'name' => 'admin.system.logs']);
        });
    });

    /**
     * Adkats Admin
     */
    Route::group(['prefix' => 'adkats', 'namespace' => 'Admin\Adkats'], static function () {
        Route::resource('ban', 'BanController');
    });
});