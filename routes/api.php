<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/21/20, 5:07 AM
 */

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Public API Routes
 */
Route::group(['namespace' => 'Api'], static function () {
    Route::group(['prefix' => 'dashboard'], static function () {
        Route::get('players', 'Dashboard@players');
        Route::get('players/avg', 'Dashboard@avgDailyPlayers');
        Route::get('bans', 'Dashboard@bans');
        Route::get('reports', 'Dashboard@reports');
        Route::get('reports/avg', 'Dashboard@avgDailyReport');
        Route::get('activity', 'Dashboard@activityFeed');
        Route::get('adkats', 'Dashboard@adkats');
        Route::get('servers', 'Dashboard@servers');
        Route::get('reputation', 'Dashboard@reputation');
        Route::get('feedback', 'Dashboard@feedback');
    });

    Route::group(['namespace' => 'Battlefield'], static function () {
        Route::group(['namespace' => 'Rcon', 'prefix' => 'rcon'], static function () {
            Route::get('live/{server}/server', 'Live@serverinfo');
            Route::get('live/{server}/players', 'Live@players');
        });

        Route::group(['prefix' => 'server'], static function () {
            Route::get('listing', 'Server@listing');
        });

        Route::group(['prefix' => 'player'], static function () {
            Route::get('{player}/acs', 'Player@acs');
        });
    });

    Route::group(['namespace' => 'Adkats', 'prefix' => 'adkats'], static function () {
        Route::group(['prefix' => 'report'], static function () {
            Route::get('latest', 'ReportsController@latestReports');
        });

        Route::group(['prefix' => 'ban'], static function () {
            Route::get('/', 'BanController@index');
            Route::get('latest', 'BanController@latest');
        });
    });
});
