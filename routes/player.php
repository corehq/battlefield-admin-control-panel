<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/10/20, 6:24 AM
 */

use Illuminate\Support\Facades\Route;


/**
 * Player Views
 */
Route::group(['prefix' => 'player'], static function () {
    Route::group(['namespace' => 'Battlefield'], static function () {
        Route::get('/', 'PlayerController@index')
            ->middleware(['permission:view.player.listing'])
            ->name('player.list');

        Route::get('{player}', 'PlayerController@show')
            ->middleware(['permission:view.player.profile'])
            ->name('player.profile');
    });

    Route::group(['namespace' => 'Adkats'], static function () {
        Route::get('{player}/ban/add', 'BanController@create')
            ->name('player.ban.edit');
    });
});