CREATE TABLE `tbl_chatlog` (
  `ID` int(11) NOT NULL,
  `logDate` datetime NOT NULL,
  `ServerID` smallint(5) UNSIGNED DEFAULT NULL,
  `logSubset` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `logPlayerID` int(10) UNSIGNED DEFAULT NULL,
  `logSoldierName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logMessage` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tbl_currency` (
  `id` int(11) NOT NULL,
  `playername` varchar(16) CHARACTER SET latin1 DEFAULT NULL,
  `CC` int(10) NOT NULL DEFAULT 0,
  `guid` varchar(35) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tbl_currentplayers` (
  `ServerID` smallint(6) NOT NULL,
  `Soldiername` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `GlobalRank` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `ClanTag` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Score` int(11) NOT NULL DEFAULT 0,
  `Kills` int(11) NOT NULL DEFAULT 0,
  `Headshots` int(11) NOT NULL DEFAULT 0,
  `Deaths` int(11) NOT NULL DEFAULT 0,
  `Suicide` int(11) DEFAULT NULL,
  `Killstreak` smallint(6) DEFAULT 0,
  `Deathstreak` smallint(6) DEFAULT 0,
  `TeamID` tinyint(4) DEFAULT NULL,
  `SquadID` tinyint(4) DEFAULT NULL,
  `EA_GUID` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `PB_GUID` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `IP_aton` int(11) UNSIGNED DEFAULT NULL,
  `CountryCode` varchar(2) COLLATE utf8_unicode_ci DEFAULT '',
  `Ping` smallint(6) DEFAULT NULL,
  `PlayerJoined` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tbl_dogtags` (
  `KillerID` int(10) UNSIGNED NOT NULL,
  `VictimID` int(10) UNSIGNED NOT NULL,
  `Count` smallint(5) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tbl_extendedroundstats` (
  `roundstat_id` int(10) UNSIGNED NOT NULL,
  `server_id` smallint(5) UNSIGNED NOT NULL,
  `round_id` int(10) UNSIGNED NOT NULL,
  `round_elapsedTimeSec` int(10) UNSIGNED NOT NULL,
  `team1_count` int(10) UNSIGNED NOT NULL,
  `team2_count` int(10) UNSIGNED NOT NULL,
  `team1_score` int(10) NOT NULL,
  `team2_score` int(10) NOT NULL,
  `team1_spm` double NOT NULL,
  `team2_spm` double NOT NULL,
  `team1_tickets` int(10) NOT NULL,
  `team2_tickets` int(10) NOT NULL,
  `team1_tpm` double NOT NULL,
  `team2_tpm` double NOT NULL,
  `roundstat_time` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tbl_games` (
  `GameID` tinyint(4) UNSIGNED NOT NULL,
  `Name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tbl_mapstats` (
  `ID` int(10) UNSIGNED NOT NULL,
  `ServerID` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `TimeMapLoad` datetime DEFAULT NULL,
  `TimeRoundStarted` datetime DEFAULT NULL,
  `TimeRoundEnd` datetime DEFAULT NULL,
  `MapName` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Gamemode` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Roundcount` smallint(6) NOT NULL DEFAULT 0,
  `NumberofRounds` smallint(6) NOT NULL DEFAULT 0,
  `MinPlayers` smallint(6) NOT NULL DEFAULT 0,
  `AvgPlayers` double NOT NULL DEFAULT 0,
  `MaxPlayers` smallint(6) NOT NULL DEFAULT 0,
  `PlayersJoinedServer` smallint(6) NOT NULL DEFAULT 0,
  `PlayersLeftServer` smallint(6) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tbl_playerdata` (
  `PlayerID` int(10) UNSIGNED NOT NULL,
  `GameID` tinyint(4) UNSIGNED DEFAULT NULL,
  `ClanTag` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SoldierName` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GlobalRank` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `PBGUID` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EAGUID` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IP_Address` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DiscordID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CountryCode` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tbl_playerrank` (
  `PlayerID` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `ServerGroup` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `rankScore` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `rankKills` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tbl_playerstats` (
  `StatsID` int(10) UNSIGNED NOT NULL,
  `Score` int(11) NOT NULL DEFAULT 0,
  `Kills` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `Headshots` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `Deaths` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `Suicide` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `TKs` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `Playtime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `Rounds` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `FirstSeenOnServer` datetime DEFAULT NULL,
  `LastSeenOnServer` datetime DEFAULT NULL,
  `Killstreak` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `Deathstreak` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `HighScore` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `rankScore` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `rankKills` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `Wins` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `Losses` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tbl_server` (
  `ServerID` smallint(5) UNSIGNED NOT NULL,
  `ServerGroup` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `IP_Address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ServerName` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GameID` tinyint(4) UNSIGNED NOT NULL,
  `usedSlots` smallint(5) UNSIGNED DEFAULT 0,
  `maxSlots` smallint(5) UNSIGNED DEFAULT 0,
  `mapName` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullMapName` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Gamemode` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GameMod` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PBversion` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ConnectionState` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tbl_server_player` (
  `StatsID` int(10) UNSIGNED NOT NULL,
  `ServerID` smallint(5) UNSIGNED NOT NULL,
  `PlayerID` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tbl_server_stats` (
  `ServerID` smallint(5) UNSIGNED NOT NULL,
  `CountPlayers` bigint(20) NOT NULL DEFAULT 0,
  `SumScore` bigint(20) NOT NULL DEFAULT 0,
  `AvgScore` float NOT NULL DEFAULT 0,
  `SumKills` bigint(20) NOT NULL DEFAULT 0,
  `AvgKills` float NOT NULL DEFAULT 0,
  `SumHeadshots` bigint(20) NOT NULL DEFAULT 0,
  `AvgHeadshots` float NOT NULL DEFAULT 0,
  `SumDeaths` bigint(20) NOT NULL DEFAULT 0,
  `AvgDeaths` float NOT NULL DEFAULT 0,
  `SumSuicide` bigint(20) NOT NULL DEFAULT 0,
  `AvgSuicide` float NOT NULL DEFAULT 0,
  `SumTKs` bigint(20) NOT NULL DEFAULT 0,
  `AvgTKs` float NOT NULL DEFAULT 0,
  `SumPlaytime` bigint(20) NOT NULL DEFAULT 0,
  `AvgPlaytime` float NOT NULL DEFAULT 0,
  `SumRounds` bigint(20) NOT NULL DEFAULT 0,
  `AvgRounds` float NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tbl_sessions` (
  `SessionID` int(10) UNSIGNED NOT NULL,
  `StatsID` int(10) UNSIGNED NOT NULL,
  `StartTime` datetime NOT NULL,
  `EndTime` datetime NOT NULL,
  `Score` mediumint(9) NOT NULL DEFAULT 0,
  `Kills` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `Headshots` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `Deaths` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `TKs` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `Suicide` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `RoundCount` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `Playtime` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `Killstreak` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `Deathstreak` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `HighScore` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `Wins` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `Losses` tinyint(3) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tbl_teamscores` (
  `ServerID` smallint(5) UNSIGNED NOT NULL,
  `TeamID` smallint(5) NOT NULL DEFAULT 0,
  `Score` int(11) DEFAULT NULL,
  `WinningScore` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tbl_weaponcodes` (
  `weapon_id` int(11) NOT NULL,
  `weapon_code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `weapon_usage_count` int(11) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tbl_weapons` (
  `WeaponID` int(11) UNSIGNED NOT NULL,
  `GameID` tinyint(4) UNSIGNED NOT NULL,
  `Friendlyname` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Fullname` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Damagetype` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Slot` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Kitrestriction` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tbl_weapons_stats` (
  `StatsID` int(10) UNSIGNED NOT NULL,
  `WeaponID` int(11) UNSIGNED NOT NULL,
  `Kills` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `Headshots` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `Deaths` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `tbl_chatlog`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ServerID` (`ServerID`),
  ADD KEY `logPlayerID` (`logPlayerID`),
  ADD KEY `logDate` (`logDate`),
  ADD KEY `logSoldierName` (`logSoldierName`);
ALTER TABLE `tbl_chatlog` ADD FULLTEXT KEY `log_message` (`logMessage`);

ALTER TABLE `tbl_currency`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Unique_Player` (`playername`,`guid`),
  ADD KEY `playername` (`playername`);

ALTER TABLE `tbl_currentplayers`
  ADD PRIMARY KEY (`ServerID`,`Soldiername`);

ALTER TABLE `tbl_dogtags`
  ADD PRIMARY KEY (`KillerID`,`VictimID`),
  ADD KEY `fk_tbl_dogtags_tbl_server_player1` (`KillerID`),
  ADD KEY `fk_tbl_dogtags_tbl_server_player2` (`VictimID`);

ALTER TABLE `tbl_extendedroundstats`
  ADD PRIMARY KEY (`roundstat_id`),
  ADD KEY `tbl_extendedroundstats_server_id` (`server_id`),
  ADD KEY `round_id` (`round_id`),
  ADD KEY `roundstat_time` (`roundstat_time`);

ALTER TABLE `tbl_games`
  ADD PRIMARY KEY (`GameID`),
  ADD UNIQUE KEY `name_unique` (`Name`);

ALTER TABLE `tbl_mapstats`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ServerID_INDEX` (`ServerID`),
  ADD KEY `MapLoad_INDEX` (`TimeMapLoad`),
  ADD KEY `RoundStart_INDEX` (`TimeRoundStarted`),
  ADD KEY `RoundEnd_INDEX` (`TimeRoundEnd`);

ALTER TABLE `tbl_playerdata`
  ADD PRIMARY KEY (`PlayerID`),
  ADD UNIQUE KEY `UNIQUE_playerdata` (`EAGUID`,`GameID`),
  ADD KEY `INDEX_SoldierName` (`SoldierName`),
  ADD KEY `INDEX_IP` (`IP_Address`),
  ADD KEY `INDEX_CountryCode` (`CountryCode`),
  ADD KEY `PBGUID` (`PBGUID`),
  ADD KEY `created_at` (`created_at`),
  ADD KEY `updated_at` (`updated_at`),
  ADD KEY `GameID` (`GameID`);

ALTER TABLE `tbl_playerrank`
  ADD PRIMARY KEY (`PlayerID`,`ServerGroup`),
  ADD KEY `INDEX_SCORERANKING` (`rankScore`),
  ADD KEY `INDEX_KILLSRANKING` (`rankKills`);

ALTER TABLE `tbl_playerstats`
  ADD PRIMARY KEY (`StatsID`),
  ADD KEY `INDEX_RANK_SCORE` (`rankScore`),
  ADD KEY `INDEX_RANK_KILLS` (`rankKills`),
  ADD KEY `INDEX_SCORE` (`Score`),
  ADD KEY `INDEX_FIRST_SEEN` (`FirstSeenOnServer`),
  ADD KEY `INDEX_LAST_SEEN` (`LastSeenOnServer`);

ALTER TABLE `tbl_server`
  ADD PRIMARY KEY (`ServerID`),
  ADD UNIQUE KEY `IP_Address` (`IP_Address`),
  ADD KEY `INDEX_SERVERGROUP` (`ServerGroup`);

ALTER TABLE `tbl_server_player`
  ADD PRIMARY KEY (`StatsID`),
  ADD UNIQUE KEY `UNIQUE_INDEX` (`ServerID`,`PlayerID`),
  ADD KEY `fk_tbl_server_player_tbl_playerdata` (`PlayerID`),
  ADD KEY `fk_tbl_server_player_tbl_server` (`ServerID`);

ALTER TABLE `tbl_server_stats`
  ADD PRIMARY KEY (`ServerID`),
  ADD KEY `fk_tbl_server_stats_tbl_server` (`ServerID`);

ALTER TABLE `tbl_sessions`
  ADD PRIMARY KEY (`SessionID`),
  ADD KEY `INDEX_STATSID` (`StatsID`),
  ADD KEY `INDEX_STARTTIME` (`StartTime`),
  ADD KEY `INDEX_ENDTIME` (`EndTime`);

ALTER TABLE `tbl_teamscores`
  ADD PRIMARY KEY (`ServerID`,`TeamID`);

ALTER TABLE `tbl_weaponcodes`
  ADD PRIMARY KEY (`weapon_id`),
  ADD UNIQUE KEY `weapon_code_UNIQUE` (`weapon_code`);

ALTER TABLE `tbl_weapons`
  ADD PRIMARY KEY (`WeaponID`),
  ADD UNIQUE KEY `unique` (`GameID`,`Fullname`);

ALTER TABLE `tbl_weapons_stats`
  ADD PRIMARY KEY (`StatsID`,`WeaponID`),
  ADD KEY `weapon_index` (`WeaponID`) USING BTREE;

ALTER TABLE `tbl_currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `tbl_extendedroundstats`
  MODIFY `roundstat_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `tbl_games`
  MODIFY `GameID` tinyint(4) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `tbl_mapstats`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `tbl_playerdata`
  MODIFY `PlayerID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `tbl_server`
  MODIFY `ServerID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `tbl_server_player`
  MODIFY `StatsID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `tbl_sessions`
  MODIFY `SessionID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `tbl_weaponcodes`
  MODIFY `weapon_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `tbl_weapons`
  MODIFY `WeaponID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `tbl_chatlog`
  ADD CONSTRAINT `tbl_chatlog_ibfk_1` FOREIGN KEY (`ServerID`) REFERENCES `tbl_server` (`ServerID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_chatlog_ibfk_2` FOREIGN KEY (`logPlayerID`) REFERENCES `tbl_playerdata` (`PlayerID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `tbl_dogtags`
  ADD CONSTRAINT `fk_tbl_dogtags_tbl_server_player1` FOREIGN KEY (`KillerID`) REFERENCES `tbl_server_player` (`StatsID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_dogtags_tbl_server_player2` FOREIGN KEY (`VictimID`) REFERENCES `tbl_server_player` (`StatsID`) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE `tbl_extendedroundstats`
  ADD CONSTRAINT `tbl_extendedroundstats_server_id` FOREIGN KEY (`server_id`) REFERENCES `tbl_server` (`ServerID`) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE `tbl_playerdata`
  ADD CONSTRAINT `tbl_playerdata_ibfk_1` FOREIGN KEY (`GameID`) REFERENCES `tbl_games` (`GameID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `tbl_playerrank`
  ADD CONSTRAINT `fk_tbl_playerrank_tbl_playerdata` FOREIGN KEY (`PlayerID`) REFERENCES `tbl_playerdata` (`PlayerID`) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE `tbl_playerstats`
  ADD CONSTRAINT `fk_tbl_playerstats_tbl_server_player1` FOREIGN KEY (`StatsID`) REFERENCES `tbl_server_player` (`StatsID`) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE `tbl_server_player`
  ADD CONSTRAINT `fk_tbl_server_player_tbl_playerdata` FOREIGN KEY (`PlayerID`) REFERENCES `tbl_playerdata` (`PlayerID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_server_player_tbl_server` FOREIGN KEY (`ServerID`) REFERENCES `tbl_server` (`ServerID`) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE `tbl_server_stats`
  ADD CONSTRAINT `fk_tbl_server_stats_tbl_server` FOREIGN KEY (`ServerID`) REFERENCES `tbl_server` (`ServerID`) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE `tbl_sessions`
  ADD CONSTRAINT `fk_tbl_sessions_tbl_server_player` FOREIGN KEY (`StatsID`) REFERENCES `tbl_server_player` (`StatsID`) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE `tbl_weapons_stats`
  ADD CONSTRAINT `fk_tbl_weapons_stats_tbl_server_player_StatsID` FOREIGN KEY (`StatsID`) REFERENCES `tbl_server_player` (`StatsID`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;