CREATE TABLE `adkats_bans` (
  `ban_id` int(11) UNSIGNED NOT NULL,
  `player_id` int(11) UNSIGNED NOT NULL,
  `latest_record_id` int(11) UNSIGNED NOT NULL,
  `ban_notes` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NoNotes',
  `ban_status` enum('Active','Expired','Disabled') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `ban_startTime` datetime NOT NULL,
  `ban_endTime` datetime NOT NULL,
  `ban_enforceName` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ban_enforceGUID` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ban_enforceIP` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ban_sync` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '-sync-'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AdKats - Ban List';

CREATE TABLE `adkats_battlecries` (
  `player_id` int(10) UNSIGNED NOT NULL,
  `player_battlecry` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AdKats - Battlecries List';

CREATE TABLE `adkats_battlelog_players` (
  `player_id` int(10) UNSIGNED NOT NULL,
  `persona_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `gravatar` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `persona_banned` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `adkats_bounty` (
  `player_id` int(10) UNSIGNED NOT NULL,
  `balance` decimal(10,0) UNSIGNED NOT NULL DEFAULT 0,
  `current_bounty` decimal(10,0) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `adkats_bounty_servers` (
  `player_id` int(10) UNSIGNED NOT NULL,
  `server_id` smallint(5) UNSIGNED NOT NULL,
  `balance` decimal(10,0) UNSIGNED NOT NULL DEFAULT 0,
  `current_bounty` decimal(10,0) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `adkats_bounty_transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `server_id` smallint(5) UNSIGNED NOT NULL,
  `player_id` int(10) UNSIGNED NOT NULL,
  `income` decimal(10,0) UNSIGNED NOT NULL DEFAULT 0,
  `deficit` decimal(10,0) NOT NULL DEFAULT 0,
  `type` enum('Kill','KillHeadshot','CollectedBounty','Payroll','Mugging','Other') COLLATE utf8_unicode_ci DEFAULT NULL,
  `bounty_target` int(10) UNSIGNED DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `adkats_challenge_definition` (
  `ID` int(10) UNSIGNED NOT NULL,
  `Name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `CreateTime` datetime NOT NULL,
  `ModifyTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AdKats - Challenge Definitions';

CREATE TABLE `adkats_challenge_definition_detail` (
  `DefID` int(10) UNSIGNED NOT NULL,
  `DetailID` int(10) UNSIGNED NOT NULL,
  `Type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Damage` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WeaponCount` int(10) UNSIGNED NOT NULL,
  `Weapon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KillCount` int(10) UNSIGNED NOT NULL,
  `CreateTime` datetime NOT NULL,
  `ModifyTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AdKats - Challenge Definition Details';

CREATE TABLE `adkats_challenge_entry` (
  `ID` int(10) UNSIGNED NOT NULL,
  `PlayerID` int(10) UNSIGNED NOT NULL,
  `RuleID` int(10) UNSIGNED NOT NULL,
  `Completed` int(1) UNSIGNED NOT NULL,
  `Failed` int(1) UNSIGNED NOT NULL,
  `Canceled` int(1) UNSIGNED NOT NULL,
  `StartRound` int(10) UNSIGNED NOT NULL,
  `StartTime` datetime NOT NULL,
  `CompleteTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AdKats - Challenge Entries';

CREATE TABLE `adkats_challenge_entry_detail` (
  `EntryID` int(10) UNSIGNED NOT NULL,
  `DetailID` int(10) UNSIGNED NOT NULL,
  `VictimID` int(10) UNSIGNED NOT NULL,
  `Weapon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RoundID` int(10) UNSIGNED NOT NULL,
  `DetailTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AdKats - Challenge Entry Details';

CREATE TABLE `adkats_challenge_reward` (
  `ID` int(10) UNSIGNED NOT NULL,
  `ServerID` smallint(5) UNSIGNED NOT NULL,
  `Tier` int(10) UNSIGNED NOT NULL,
  `Reward` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'None',
  `Enabled` int(1) UNSIGNED NOT NULL DEFAULT 0,
  `DurationMinutes` int(10) UNSIGNED NOT NULL DEFAULT 60,
  `CreateTime` datetime NOT NULL,
  `ModifyTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AdKats - Challenge Rewards';

CREATE TABLE `adkats_challenge_rule` (
  `ID` int(10) UNSIGNED NOT NULL,
  `ServerID` smallint(5) UNSIGNED NOT NULL,
  `DefID` int(10) UNSIGNED NOT NULL,
  `Enabled` int(1) UNSIGNED NOT NULL DEFAULT 1,
  `Name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `Tier` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `CompletionType` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'None',
  `RoundCount` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `DurationMinutes` int(10) UNSIGNED NOT NULL DEFAULT 60,
  `DeathCount` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `CreateTime` datetime NOT NULL,
  `ModifyTime` datetime NOT NULL,
  `RoundLastUsedTime` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `PersonalLastUsedTime` datetime NOT NULL DEFAULT '1970-01-01 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AdKats - Challenge Rules';

CREATE TABLE `adkats_commands` (
  `command_id` int(11) UNSIGNED NOT NULL,
  `command_active` enum('Active','Disabled','Invisible') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `command_key` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `command_logging` enum('Log','Mandatory','Ignore','Unable') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Log',
  `command_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `command_text` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `command_playerInteraction` tinyint(1) NOT NULL,
  `command_access` enum('Any','AnyHidden','AnyVisible','GlobalVisible','TeamVisible','SquadVisible') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Any'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AdKats - Command List';

CREATE TABLE `adkats_infractions_global` (
  `player_id` int(11) UNSIGNED NOT NULL,
  `punish_points` int(11) NOT NULL,
  `forgive_points` int(11) NOT NULL,
  `total_points` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AdKats - Global Player Infraction Points';

CREATE TABLE `adkats_infractions_server` (
  `player_id` int(11) UNSIGNED NOT NULL,
  `server_id` smallint(5) UNSIGNED NOT NULL,
  `punish_points` int(11) NOT NULL,
  `forgive_points` int(11) NOT NULL,
  `total_points` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AdKats - Server Specific Player Infraction Points';

CREATE TABLE `adkats_orchestration` (
  `setting_id` int(10) NOT NULL,
  `setting_server` smallint(5) NOT NULL,
  `setting_plugin` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `setting_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `setting_value` varchar(2000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AdKats - Plugin Orchestration';

CREATE TABLE `adkats_player_reputation` (
  `player_id` int(10) UNSIGNED NOT NULL,
  `game_id` tinyint(4) UNSIGNED NOT NULL,
  `target_rep` float NOT NULL,
  `source_rep` float NOT NULL,
  `total_rep` float NOT NULL,
  `total_rep_co` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `adkats_records_debug` (
  `record_id` int(11) UNSIGNED NOT NULL,
  `server_id` smallint(5) UNSIGNED NOT NULL,
  `command_type` int(11) UNSIGNED NOT NULL,
  `command_action` int(11) UNSIGNED NOT NULL,
  `command_numeric` int(11) NOT NULL DEFAULT 0,
  `target_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NoTarget',
  `target_id` int(11) UNSIGNED DEFAULT NULL,
  `source_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NoSource',
  `source_id` int(11) UNSIGNED DEFAULT NULL,
  `record_message` varchar(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NoMessage',
  `record_time` datetime NOT NULL,
  `adkats_read` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `adkats_web` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AdKats - Debug Records';

CREATE TABLE `adkats_records_main` (
  `record_id` int(11) UNSIGNED NOT NULL,
  `server_id` smallint(5) UNSIGNED NOT NULL,
  `command_type` int(11) UNSIGNED NOT NULL,
  `command_action` int(11) UNSIGNED NOT NULL,
  `command_numeric` int(11) NOT NULL DEFAULT 0,
  `target_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NoTarget',
  `target_id` int(11) UNSIGNED DEFAULT NULL,
  `source_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NoSource',
  `source_id` int(11) UNSIGNED DEFAULT NULL,
  `record_message` varchar(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NoMessage',
  `record_time` datetime NOT NULL,
  `adkats_read` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `adkats_web` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AdKats - Main Records';
DELIMITER $$
CREATE TRIGGER `adkats_infraction_point_delete` AFTER DELETE ON `adkats_records_main` FOR EACH ROW BEGIN
        DECLARE command_type VARCHAR(45);
        DECLARE server_id INT(11);
        DECLARE player_id INT(11);
        SET command_type = OLD.command_type;
        SET server_id = OLD.server_id;
        SET player_id = OLD.target_id;

        IF(command_type = 9) THEN
            INSERT INTO `adkats_infractions_server`
                (`player_id`, `server_id`, `punish_points`, `forgive_points`, `total_points`)
            VALUES
                (player_id, server_id, 0, 0, 0)
            ON DUPLICATE KEY UPDATE
                `punish_points` = `punish_points` - 1,
                `total_points` = `total_points` - 1;
            INSERT INTO `adkats_infractions_global`
                (`player_id`, `punish_points`, `forgive_points`, `total_points`)
            VALUES
                (player_id, 0, 0, 0)
            ON DUPLICATE KEY UPDATE
                `punish_points` = `punish_points` - 1,
                `total_points` = `total_points` - 1;
        ELSEIF (command_type = 10) THEN
            INSERT INTO `adkats_infractions_server`
                (`player_id`, `server_id`, `punish_points`, `forgive_points`, `total_points`)
            VALUES
                (player_id, server_id, 0, 0, 0)
            ON DUPLICATE KEY UPDATE
                `forgive_points` = `forgive_points` - 1,
                `total_points` = `total_points` + 1;
            INSERT INTO `adkats_infractions_global`
                (`player_id`, `punish_points`, `forgive_points`, `total_points`)
            VALUES
                (player_id, 0, 0, 0)
            ON DUPLICATE KEY UPDATE
                `forgive_points` = `forgive_points` - 1,
                `total_points` = `total_points` + 1;
        END IF;
    END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `adkats_infraction_point_insert` BEFORE INSERT ON `adkats_records_main` FOR EACH ROW BEGIN
        DECLARE command_type VARCHAR(45);
        DECLARE server_id INT(11);
        DECLARE player_id INT(11);
        SET command_type = NEW.command_type;
        SET server_id = NEW.server_id;
        SET player_id = NEW.target_id;

		IF NEW.record_message = 'Player Listing Offline' THEN
				CALL func_nonexistent();
		END IF;

        IF(command_type = 9) THEN
            INSERT INTO `adkats_infractions_server`
                (`player_id`, `server_id`, `punish_points`, `forgive_points`, `total_points`)
            VALUES
                (player_id, server_id, 1, 0, 1)
            ON DUPLICATE KEY UPDATE
                `punish_points` = `punish_points` + 1,
                `total_points` = `total_points` + 1;
            INSERT INTO `adkats_infractions_global`
                (`player_id`, `punish_points`, `forgive_points`, `total_points`)
            VALUES
                (player_id, 1, 0, 1)
            ON DUPLICATE KEY UPDATE
                `punish_points` = `punish_points` + 1,
                `total_points` = `total_points` + 1;
        ELSEIF (command_type = 10) THEN
            INSERT INTO `adkats_infractions_server`
                (`player_id`, `server_id`, `punish_points`, `forgive_points`, `total_points`)
            VALUES
                (player_id, server_id, 0, 1, -1)
            ON DUPLICATE KEY UPDATE
                `forgive_points` = `forgive_points` + 1,
                `total_points` = `total_points` - 1;
            INSERT INTO `adkats_infractions_global`
                (`player_id`, `punish_points`, `forgive_points`, `total_points`)
            VALUES
                (player_id, 0, 1, -1)
            ON DUPLICATE KEY UPDATE
                `forgive_points` = `forgive_points` + 1,
                `total_points` = `total_points` - 1;
        END IF;
    END
$$
DELIMITER ;

CREATE TABLE `adkats_report_actions` (
  `id` int(10) UNSIGNED NOT NULL,
  `record_id` int(11) UNSIGNED NOT NULL,
  `admin_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_id` int(11) UNSIGNED DEFAULT NULL,
  `action_type` int(11) UNSIGNED NOT NULL,
  `action_detail` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NoDetail',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AdKats - Report Actions';

CREATE TABLE `adkats_report_actions_types` (
  `type_id` int(11) UNSIGNED NOT NULL,
  `type_key` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `type_text` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AdKats - Report Action Types';

CREATE TABLE `adkats_rolecommands` (
  `role_id` int(11) UNSIGNED NOT NULL,
  `command_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AdKats - Connection of commands to roles';

CREATE TABLE `adkats_rolegroups` (
  `role_id` int(11) UNSIGNED NOT NULL,
  `group_key` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AdKats - Connection of groups to roles';

CREATE TABLE `adkats_roles` (
  `role_id` int(11) UNSIGNED NOT NULL,
  `role_key` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `role_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AdKats - Role List';

CREATE TABLE `adkats_settings` (
  `server_id` smallint(5) UNSIGNED NOT NULL,
  `setting_key` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `setting_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SettingName',
  `setting_type` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SettingType',
  `setting_value` varchar(10000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AdKats - Server Setting List';

CREATE TABLE `adkats_specialplayers` (
  `specialplayer_id` int(10) UNSIGNED NOT NULL,
  `player_group` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `player_id` int(10) UNSIGNED DEFAULT NULL,
  `player_game` tinyint(4) UNSIGNED DEFAULT NULL,
  `player_server` smallint(5) UNSIGNED DEFAULT NULL,
  `player_identifier` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `player_effective` datetime NOT NULL,
  `player_expiration` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AdKats - Special Player List';

CREATE TABLE `adkats_statistics` (
  `stat_id` int(10) UNSIGNED NOT NULL,
  `server_id` smallint(5) UNSIGNED NOT NULL,
  `round_id` int(10) UNSIGNED NOT NULL,
  `stat_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `target_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `target_id` int(10) UNSIGNED DEFAULT NULL,
  `stat_value` float NOT NULL,
  `stat_comment` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `stat_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AdKats - Statistics';

CREATE TABLE `adkats_users` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `user_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_phone` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_role` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `user_expiration` datetime NOT NULL,
  `user_notes` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No Notes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AdKats - User List';

CREATE TABLE `adkats_usersoldiers` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `player_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AdKats - Connection of users to soldiers';

ALTER TABLE `adkats_bans`
  ADD PRIMARY KEY (`ban_id`),
  ADD UNIQUE KEY `player_id_UNIQUE` (`player_id`),
  ADD KEY `adkats_bans_fk_latest_record_id` (`latest_record_id`),
  ADD KEY `ban_startTime` (`ban_startTime`),
  ADD KEY `ban_endTime` (`ban_endTime`);

ALTER TABLE `adkats_battlecries`
  ADD PRIMARY KEY (`player_id`);

ALTER TABLE `adkats_battlelog_players`
  ADD PRIMARY KEY (`player_id`),
  ADD UNIQUE KEY `adkats_battlelog_players_player_id_persona_id_unique` (`player_id`,`persona_id`),
  ADD KEY `adkats_battlelog_players_persona_id_index` (`persona_id`),
  ADD KEY `adkats_battlelog_players_user_id_index` (`user_id`);

ALTER TABLE `adkats_bounty`
  ADD PRIMARY KEY (`player_id`),
  ADD KEY `balance_index` (`balance`),
  ADD KEY `bounty_index` (`current_bounty`);

ALTER TABLE `adkats_bounty_servers`
  ADD PRIMARY KEY (`player_id`),
  ADD KEY `server_id_fk_idx` (`server_id`);

ALTER TABLE `adkats_bounty_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `player_index` (`player_id`),
  ADD KEY `income_index` (`income`),
  ADD KEY `deficit_index` (`deficit`),
  ADD KEY `type_index` (`type`),
  ADD KEY `bounty_target_index` (`bounty_target`),
  ADD KEY `server_id_fk_idx` (`server_id`);

ALTER TABLE `adkats_challenge_definition`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `adkats_challenge_definition_idx_Name` (`Name`),
  ADD KEY `adkats_challenge_definition_idx_CreateTime` (`CreateTime`),
  ADD KEY `adkats_challenge_definition_idx_ModifyTime` (`ModifyTime`);

ALTER TABLE `adkats_challenge_definition_detail`
  ADD PRIMARY KEY (`DefID`,`DetailID`),
  ADD KEY `adkats_challenge_definition_detail_idx_CreateTime` (`CreateTime`),
  ADD KEY `adkats_challenge_definition_detail_idx_ModifyTime` (`ModifyTime`);

ALTER TABLE `adkats_challenge_entry`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `adkats_challenge_entry_idx_PlayerID` (`PlayerID`),
  ADD KEY `adkats_challenge_entry_idx_RuleID` (`RuleID`),
  ADD KEY `adkats_challenge_entry_idx_StartTime` (`StartTime`),
  ADD KEY `adkats_challenge_entry_idx_CompleteTime` (`CompleteTime`);

ALTER TABLE `adkats_challenge_entry_detail`
  ADD PRIMARY KEY (`EntryID`,`DetailID`),
  ADD KEY `adkats_challenge_entry_detail_idx_VictimID` (`VictimID`),
  ADD KEY `adkats_challenge_entry_detail_idx_DetailTime` (`DetailTime`);

ALTER TABLE `adkats_challenge_reward`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ServerID` (`ServerID`,`Tier`,`Reward`),
  ADD KEY `adkats_challenge_reward_idx_CreateTime` (`CreateTime`),
  ADD KEY `adkats_challenge_reward_idx_ModifyTime` (`ModifyTime`);

ALTER TABLE `adkats_challenge_rule`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `adkats_challenge_rule_idx_Name_Server` (`Name`,`ServerID`),
  ADD KEY `adkats_challenge_rule_idx_ServerID` (`ServerID`),
  ADD KEY `adkats_challenge_rule_idx_DefID` (`DefID`),
  ADD KEY `adkats_challenge_rule_idx_CreateTime` (`CreateTime`),
  ADD KEY `adkats_challenge_rule_idx_ModifyTime` (`ModifyTime`),
  ADD KEY `adkats_challenge_rule_idx_RoundLastUsedTime` (`RoundLastUsedTime`),
  ADD KEY `adkats_challenge_rule_idx_PersonalLastUsedTime` (`PersonalLastUsedTime`);

ALTER TABLE `adkats_commands`
  ADD PRIMARY KEY (`command_id`),
  ADD UNIQUE KEY `command_key_UNIQUE` (`command_key`),
  ADD UNIQUE KEY `command_text_UNIQUE` (`command_text`);

ALTER TABLE `adkats_infractions_global`
  ADD PRIMARY KEY (`player_id`);

ALTER TABLE `adkats_infractions_server`
  ADD PRIMARY KEY (`player_id`,`server_id`),
  ADD KEY `adkats_infractions_server_fk_server_id` (`server_id`);

ALTER TABLE `adkats_orchestration`
  ADD PRIMARY KEY (`setting_id`),
  ADD UNIQUE KEY `setting_server` (`setting_server`,`setting_plugin`,`setting_name`),
  ADD KEY `setting_server_2` (`setting_server`);

ALTER TABLE `adkats_player_reputation`
  ADD PRIMARY KEY (`player_id`),
  ADD KEY `game_id` (`game_id`);

ALTER TABLE `adkats_records_debug`
  ADD PRIMARY KEY (`record_id`),
  ADD KEY `adkats_records_debug_fk_server_id` (`server_id`),
  ADD KEY `adkats_records_debug_fk_command_type` (`command_type`),
  ADD KEY `adkats_records_debug_fk_command_action` (`command_action`);

ALTER TABLE `adkats_records_main`
  ADD PRIMARY KEY (`record_id`),
  ADD KEY `adkats_records_main_fk_server_id` (`server_id`),
  ADD KEY `adkats_records_main_fk_command_type` (`command_type`),
  ADD KEY `adkats_records_main_fk_command_action` (`command_action`),
  ADD KEY `adkats_records_main_fk_target_id` (`target_id`),
  ADD KEY `adkats_records_main_fk_source_id` (`source_id`),
  ADD KEY `adkats_records_main_fk_record_time` (`record_time`);

ALTER TABLE `adkats_report_actions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `report_action_type_idx` (`action_type`),
  ADD KEY `record_id` (`record_id`),
  ADD KEY `created_at` (`created_at`),
  ADD KEY `updated_at` (`updated_at`),
  ADD KEY `admin_id` (`admin_id`),
  ADD KEY `admin_name` (`admin_name`);

ALTER TABLE `adkats_report_actions_types`
  ADD PRIMARY KEY (`type_id`),
  ADD UNIQUE KEY `type_key` (`type_key`);

ALTER TABLE `adkats_rolecommands`
  ADD PRIMARY KEY (`role_id`,`command_id`),
  ADD KEY `adkats_rolecommands_fk_role` (`role_id`),
  ADD KEY `adkats_rolecommands_fk_command` (`command_id`);

ALTER TABLE `adkats_rolegroups`
  ADD PRIMARY KEY (`role_id`,`group_key`),
  ADD KEY `adkats_rolegroups_fk_role` (`role_id`),
  ADD KEY `adkats_rolegroups_fk_command` (`group_key`);

ALTER TABLE `adkats_roles`
  ADD PRIMARY KEY (`role_id`),
  ADD UNIQUE KEY `role_key_UNIQUE` (`role_key`);

ALTER TABLE `adkats_settings`
  ADD PRIMARY KEY (`server_id`,`setting_name`);

ALTER TABLE `adkats_specialplayers`
  ADD PRIMARY KEY (`specialplayer_id`),
  ADD KEY `adkats_specialplayers_game_id` (`player_game`),
  ADD KEY `adkats_specialplayers_server_id` (`player_server`),
  ADD KEY `adkats_specialplayers_player_id` (`player_id`);

ALTER TABLE `adkats_statistics`
  ADD PRIMARY KEY (`stat_id`),
  ADD KEY `server_id` (`server_id`),
  ADD KEY `stat_type` (`stat_type`),
  ADD KEY `target_id` (`target_id`),
  ADD KEY `stat_time` (`stat_time`),
  ADD KEY `idx_adkats_statistics_stat_type` (`stat_type`);

ALTER TABLE `adkats_users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `adkats_users_fk_role` (`user_role`);

ALTER TABLE `adkats_usersoldiers`
  ADD PRIMARY KEY (`user_id`,`player_id`),
  ADD KEY `adkats_usersoldiers_fk_user` (`user_id`),
  ADD KEY `adkats_usersoldiers_fk_player` (`player_id`);

  ALTER TABLE `adkats_bans`
  MODIFY `ban_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `adkats_challenge_definition`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `adkats_challenge_entry`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `adkats_challenge_reward`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `adkats_challenge_rule`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `adkats_orchestration`
  MODIFY `setting_id` int(10) NOT NULL AUTO_INCREMENT;

ALTER TABLE `adkats_records_debug`
  MODIFY `record_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `adkats_records_main`
  MODIFY `record_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `adkats_report_actions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `adkats_report_actions_types`
  MODIFY `type_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `adkats_roles`
  MODIFY `role_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `adkats_specialplayers`
  MODIFY `specialplayer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `adkats_statistics`
  MODIFY `stat_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `adkats_users`
  MODIFY `user_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `tbl_chatlog`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `adkats_bans`
  ADD CONSTRAINT `adkats_bans_fk_latest_record_id` FOREIGN KEY (`latest_record_id`) REFERENCES `adkats_records_main` (`record_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `adkats_bans_fk_player_id` FOREIGN KEY (`player_id`) REFERENCES `tbl_playerdata` (`PlayerID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `adkats_battlecries`
  ADD CONSTRAINT `adkats_battlecries_player_id` FOREIGN KEY (`player_id`) REFERENCES `tbl_playerdata` (`PlayerID`) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE `adkats_battlelog_players`
  ADD CONSTRAINT `adkats_battlelog_players_ibfk_1` FOREIGN KEY (`player_id`) REFERENCES `tbl_playerdata` (`PlayerID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `adkats_bounty_servers`
  ADD CONSTRAINT `bounty_server_id_fk` FOREIGN KEY (`server_id`) REFERENCES `tbl_server` (`ServerID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `adkats_bounty_transactions`
  ADD CONSTRAINT `transactions_server_id_fk` FOREIGN KEY (`server_id`) REFERENCES `tbl_server` (`ServerID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `adkats_challenge_definition_detail`
  ADD CONSTRAINT `adkats_challenge_definition_detail_fk_DefID` FOREIGN KEY (`DefID`) REFERENCES `adkats_challenge_definition` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `adkats_challenge_entry`
  ADD CONSTRAINT `adkats_challenge_entry_fk_Play erID` FOREIGN KEY (`PlayerID`) REFERENCES `tbl_playerdata` (`PlayerID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `adkats_challenge_entry_fk_RuleID` FOREIGN KEY (`RuleID`) REFERENCES `adkats_challenge_rule` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `adkats_challenge_entry_detail`
  ADD CONSTRAINT `adkats_challenge_entry_detail_fk_EntryID` FOREIGN KEY (`EntryID`) REFERENCES `adkats_challenge_entry` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `adkats_challenge_entry_detail_fk_VictimID` FOREIGN KEY (`VictimID`) REFERENCES `tbl_playerdata` (`PlayerID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `adkats_challenge_reward`
  ADD CONSTRAINT `adkats_challenge_reward_fk_ServerID` FOREIGN KEY (`ServerID`) REFERENCES `tbl_server` (`ServerID`) ON DELETE NO ACTION ON UPDATE CASCADE;

ALTER TABLE `adkats_challenge_rule`
  ADD CONSTRAINT `adkats_challenge_rule_fk_DefID` FOREIGN KEY (`DefID`) REFERENCES `adkats_challenge_definition` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `adkats_challenge_rule_fk_ServerID` FOREIGN KEY (`ServerID`) REFERENCES `tbl_server` (`ServerID`) ON DELETE NO ACTION ON UPDATE CASCADE;

ALTER TABLE `adkats_infractions_global`
  ADD CONSTRAINT `adkats_infractions_global_fk_player_id` FOREIGN KEY (`player_id`) REFERENCES `tbl_playerdata` (`PlayerID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `adkats_infractions_server`
  ADD CONSTRAINT `adkats_infractions_server_fk_player_id` FOREIGN KEY (`player_id`) REFERENCES `tbl_playerdata` (`PlayerID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `adkats_infractions_server_fk_server_id` FOREIGN KEY (`server_id`) REFERENCES `tbl_server` (`ServerID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `adkats_player_reputation`
  ADD CONSTRAINT `adkats_player_reputation_ibfk_1` FOREIGN KEY (`player_id`) REFERENCES `tbl_playerdata` (`PlayerID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `adkats_player_reputation_ibfk_2` FOREIGN KEY (`game_id`) REFERENCES `tbl_games` (`GameID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `adkats_records_debug`
  ADD CONSTRAINT `adkats_records_debug_fk_command_action` FOREIGN KEY (`command_action`) REFERENCES `adkats_commands` (`command_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `adkats_records_debug_fk_command_type` FOREIGN KEY (`command_type`) REFERENCES `adkats_commands` (`command_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `adkats_records_debug_fk_server_id` FOREIGN KEY (`server_id`) REFERENCES `tbl_server` (`ServerID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `adkats_records_main`
  ADD CONSTRAINT `adkats_records_main_fk_command_action` FOREIGN KEY (`command_action`) REFERENCES `adkats_commands` (`command_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `adkats_records_main_fk_command_type` FOREIGN KEY (`command_type`) REFERENCES `adkats_commands` (`command_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `adkats_records_main_fk_server_id` FOREIGN KEY (`server_id`) REFERENCES `tbl_server` (`ServerID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `adkats_records_main_fk_source_id` FOREIGN KEY (`source_id`) REFERENCES `tbl_playerdata` (`PlayerID`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `adkats_records_main_fk_target_id` FOREIGN KEY (`target_id`) REFERENCES `tbl_playerdata` (`PlayerID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `adkats_report_actions`
  ADD CONSTRAINT `adkats_report_actions_ibfk_1` FOREIGN KEY (`record_id`) REFERENCES `adkats_records_main` (`record_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `adkats_report_actions_ibfk_2` FOREIGN KEY (`admin_id`) REFERENCES `tbl_playerdata` (`PlayerID`) ON DELETE CASCADE,
  ADD CONSTRAINT `report_action_type` FOREIGN KEY (`action_type`) REFERENCES `adkats_report_actions_types` (`type_id`);

ALTER TABLE `adkats_rolecommands`
  ADD CONSTRAINT `adkats_rolecommands_fk_command` FOREIGN KEY (`command_id`) REFERENCES `adkats_commands` (`command_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `adkats_rolecommands_fk_role` FOREIGN KEY (`role_id`) REFERENCES `adkats_roles` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `adkats_rolegroups`
  ADD CONSTRAINT `adkats_rolegroups_fk_role` FOREIGN KEY (`role_id`) REFERENCES `adkats_roles` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `adkats_settings`
  ADD CONSTRAINT `adkats_settings_fk_server_id` FOREIGN KEY (`server_id`) REFERENCES `tbl_server` (`ServerID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `adkats_specialplayers`
  ADD CONSTRAINT `adkats_specialplayers_game_id` FOREIGN KEY (`player_game`) REFERENCES `tbl_games` (`GameID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `adkats_specialplayers_player_id` FOREIGN KEY (`player_id`) REFERENCES `tbl_playerdata` (`PlayerID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `adkats_specialplayers_server_id` FOREIGN KEY (`player_server`) REFERENCES `tbl_server` (`ServerID`) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE `adkats_statistics`
  ADD CONSTRAINT `adkats_statistics_server_id_fk` FOREIGN KEY (`server_id`) REFERENCES `tbl_server` (`ServerID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `adkats_statistics_target_id_fk` FOREIGN KEY (`target_id`) REFERENCES `tbl_playerdata` (`PlayerID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `adkats_users`
  ADD CONSTRAINT `adkats_users_fk_role` FOREIGN KEY (`user_role`) REFERENCES `adkats_roles` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `adkats_usersoldiers`
  ADD CONSTRAINT `adkats_usersoldiers_fk_player` FOREIGN KEY (`player_id`) REFERENCES `tbl_playerdata` (`PlayerID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `adkats_usersoldiers_fk_user` FOREIGN KEY (`user_id`) REFERENCES `adkats_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;
