<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/15/20, 3:38 AM
 */

namespace Tests\Feature\Routes;

use App\User;
use Tests\TestCase;

/**
 * Class AuthTest
 * @package Tests\Feature\Routes
 */
class AuthTest extends TestCase
{
    public function test_dashboard()
    {
        $this->markTestSkipped();
        $user = User::where('username', 'admin')->first();
        $this->actingAs($user);
        $this->assertAuthenticated();

        $response = $this->get('/dashboard');
        $response->assertSuccessful()->assertViewIs('pages.dashboard');
    }
}
