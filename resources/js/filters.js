/*
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/15/20, 2:59 AM
 */

Vue.filter('formatnumber', function (value) {
    return Number(value).toLocaleString(userLang);
});

Vue.filter('percentage', function (num1, num2) {
    let pct = 0;

    try {
        pct = ((num1 / num2) * 100).toFixed(2);
    } catch (e) {
        console.error("Tried to divide by zero. It didn't work.");
        pct = 0;
    }

    return pct;
});