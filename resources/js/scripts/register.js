/*
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/10/20, 10:36 AM
 */

new Vue({
    el: '#regForm',
    data() {
        return {
            username: '',
            displayName: '',
            email: '',
            password: '',
            passwordWarning: '',
            passwordClass: '',
            passwordIcon: '',
            passwordSuggestions: []
        }
    },
    watch: {
        password: function (v) {
            if (this.password.length < 8) {
                return false;
            }

            let x = zxcvbn(v, [this.username, this.email, this.displayName]);
            let score = x.score;
            let time = x.crack_times_display.offline_slow_hashing_1e4_per_second;
            let msg = '';

            if (score === 0) {
                this.passwordClass = 'text-error';
                this.passwordIcon = 'fa fa-times-circle-o';
                msg = 'Cracked:';
            } else if (score >= 1 && score <= 3) {
                this.passwordClass = 'text-warning';
                this.passwordIcon = 'fa fa-exclamation-circle';
            } else if (score > 3) {
                this.passwordClass = 'text-success';
                this.passwordIcon = 'fa fa-check-circle-o';
            }

            if (score > 0) {
                msg = 'Cracked in:'
            }

            this.passwordSuggestions = x.feedback.suggestions;

            if (x.feedback.warning.length > 0) {
                this.passwordWarning = x.feedback.warning;
            } else {
                this.passwordWarning = msg + ' ' + time;
            }
        }
    }
});