/*
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/22/20, 4:13 AM
 */

new Vue({
    el: '#dashboard-app',
    created() {
        let intervals = {
            serverlisting: null,
            activitytimeline: null
        };
        this.playerTileStats();
        this.banTileStats();
        this.reportsTileStats();
        this.activityTimeline();
        this.adkatsStats();
        this.serversListing();
        this.reputationTopTen();
        this.playerFeedback();
        // Update the server list every 30 seconds
        intervals.serverlisting = setInterval(this.serversListing, 30000);
        // Update the activity timeline every minute
        intervals.activitytimeline = setInterval(this.activityTimeline, 60000);
    },
    data: {
        players: {},
        servers: {},
        bans: {},
        reports: {},
        activitys: {},
        reputation: {},
        feedback: {},
        adkats: {},
        loading: {
            servers: false
        },
        population: {
            danger: false,
            warning: false,
            pct: 0
        }
    },
    methods: {
        playerTileStats() {
            axios.get('api/dashboard/players')
                .then(response => {
                    this.players = response.data.data;

                    this.sparklineAreaChart("#player-history-chart", [{
                        name: 'New Players',
                        data: this.players.history
                    }]);
                })
                .catch(error => {
                    setTimeout(this.playerTileStats, this.randomIntFromInterval(1, 60) * 1000);
                });
        },
        banTileStats() {
            axios.get('api/dashboard/bans')
                .then(response => {
                    this.bans = response.data.data;
                    this.sparklineAreaChart("#ban-history-chart", [{
                        name: 'Daily Bans',
                        data: this.bans.history
                    }]);
                })
                .catch(error => {
                    setTimeout(this.banTileStats, this.randomIntFromInterval(1, 60) * 1000);
                });
        },
        reportsTileStats() {
            axios.get('api/dashboard/reports')
                .then(response => {
                    this.reports = response.data.data;

                    const reportsChartOptions = {
                        chart: {
                            height: 270,
                            type: 'radialBar',
                        },
                        plotOptions: {
                            radialBar: {
                                size: 150,
                                startAngle: -150,
                                endAngle: 150,
                                offsetY: 20,
                                hollow: {
                                    size: '65%',
                                },
                                track: {
                                    background: "#fff",
                                    strokeWidth: '100%',

                                },
                                dataLabels: {
                                    value: {
                                        offsetY: 30,
                                        color: '#99a2ac',
                                        fontSize: '2rem'
                                    }
                                }
                            },
                        },
                        colors: ["#EA5455"],
                        fill: {
                            type: 'gradient',
                            gradient: {
                                // enabled: true,
                                shade: 'dark',
                                type: 'horizontal',
                                shadeIntensity: 0.5,
                                gradientToColors: ["#7367F0"],
                                inverseColors: true,
                                opacityFrom: 1,
                                opacityTo: 1,
                                stops: [0, 100]
                            },
                        },
                        stroke: {
                            dashArray: 8
                        },
                        series: [this.reports.percentage_completed],
                        labels: ['Confirmed Reports'],

                    };

                    const reportsChart = new ApexCharts(
                        document.querySelector("#reports-chart"),
                        reportsChartOptions
                    );

                    reportsChart.render();
                })
                .catch(error => {
                    setTimeout(this.reportsTileStats, this.randomIntFromInterval(1, 60) * 1000);
                });
        },
        activityTimeline() {
            axios.get('api/dashboard/activity').then(response => {
                this.activitys = response.data.data;
            }).catch(error => {
                setTimeout(this.activityTimeline, this.randomIntFromInterval(1, 60) * 1000);
            });
        },
        adkatsStats() {
            axios.get('api/dashboard/adkats').then(response => {
                this.adkats = response.data.data;
            }).catch(error => {
                setTimeout(this.adkatsStats, this.randomIntFromInterval(1, 60) * 1000);
            });
        },
        serversListing() {
            this.loading.servers = true;
            axios.get('api/dashboard/servers').then(response => {
                this.loading.servers = false;
                this.population.danger = false;
                this.population.warning = false;
                this.servers = response.data.data;
                this.population.pct = response.data.data.population.percentage;

                if (this.servers.population.percentage >= 0 && this.servers.population.percentage < 30) {
                    this.population.danger = true;
                } else if (this.servers.population.percentage >= 30 && this.servers.population.percentage < 75) {
                    this.population.warning = true;
                }
            }).catch(error => {
                setTimeout(this.serversListing, this.randomIntFromInterval(1, 60) * 1000);
            });
        },
        serverSpark(history, serverid) {
            var mapdata = [];

            history.forEach(function (v, k) {
                mapdata.push([
                    v['unix']['TimeRoundEnd'],
                    Math.round(v['AvgPlayers'])
                ]);
            });

            if (mapdata.length < 1) {
                return false;
            }

            var el = "server-" + serverid + "-chart";
            var _this = this;

            setTimeout(function () {
                document.getElementById(el).innerHTML = '';
                _this.sparklineAreaChart("#" + el, [{
                    name: 'Players Online',
                    data: mapdata
                }], 15);
            }, 200);

            return true;
        },
        reputationTopTen() {
            axios.get('api/dashboard/reputation')
                .then(response => {
                    this.reputation = response.data.data;
                })
                .catch(error => {
                    setTimeout(this.reputationTopTen, this.randomIntFromInterval(1, 60) * 1000);
                });
        },
        playerFeedback() {
            axios.get('api/dashboard/feedback')
                .then(response => {
                    this.feedback = response.data.data;
                    setTimeout(function () {
                        $('#feedbackTable').DataTable({
                            "order": [[0, 'desc']]
                        });
                    }, 200);
                })
                .catch(error => {
                    setTimeout(this.playerFeedback, this.randomIntFromInterval(1, 60) * 1000);
                });
        }
    }
});