/*
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/31/19, 8:56 PM
 */

$('.toast-toggler').on('click', function () {
    $(this).next('.toast').prependTo('.toast-bs-container .toast-position').toast('show')

    // if ($('.toast-bs-container .toast-position .toast').hasClass('hide')) {
    //   $('.toast-bs-container .toast-position .toast').toast('show')
    // }
});

$('.placement').on('click', function () {
    $('.toast-placement').toast('show');
    $('.toast-placement .toast').toast('show');
});

