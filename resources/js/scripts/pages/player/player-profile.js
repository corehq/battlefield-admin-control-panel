/*
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/12/20, 11:15 PM
 */


new Vue({
    el: '#player-overview-app',
    created() {
        this.recordHistoryF();
        this.acsCheck();
    },
    data: {
        recordHistory: {},
        acs: {}
    },
    methods: {
        recordHistoryF() {
            axios.get('api/auth/player/' + Player.PlayerID + '/history').then(response => {
                this.recordHistory = response.data;

                const columnDefs = [
                    {
                        headerName: 'ID',
                        field: 'record_id',
                        sortable: true,
                        filter: false,
                        hide: true,
                        sort: {direction: 'desc', priority: 0}
                    },
                    {
                        headerName: 'Server',
                        field: 'server.ServerName',
                        sortable: true,
                        filter: true,
                        resizable: true,
                    },
                    {
                        headerName: 'Date',
                        field: 'stamp',
                        sortable: true,
                        filter: true,
                        resizable: true,
                        cellRenderer: (data) => {
                            return moment(data.data.stamp).format('llll');
                        }
                    },
                    {
                        headerName: 'Type',
                        field: 'type.command_name',
                        filter: true,
                        resizable: true,
                    },
                    {
                        headerName: 'Action',
                        field: 'action.command_name',
                        filter: true,
                        resizable: true,
                    },
                    {
                        headerName: 'Source',
                        field: 'source_name',
                        filter: true,
                        resizable: true,
                        cellRenderer: (data) => {
                            if (data.data.source_id === null) {
                                return data.data.source_name;
                            }
                            return '<a href="/player/' + data.data.source_id + '" target="_blank">' + data.data.source_name + '</a>'
                        }
                    },
                    {
                        headerName: 'Target',
                        field: 'target_name',
                        filter: true,
                        resizable: true,
                        cellRenderer: (data) => {
                            if (data.data.target_id === null) {
                                return data.data.target_name;
                            }
                            return '<a href="/player/' + data.data.target_id + '" target="_blank">' + data.data.target_name + '</a>'
                        }
                    },
                    {
                        headerName: 'Message',
                        width: 350,
                        autoHeight: true,
                        field: 'record_message',
                        cellClass: 'text-wrap',
                        filter: true,
                        resizable: true,
                        pinned: 'left',
                        cellRenderer: (data) => {
                            let content = '';

                            if (data.data.is_web === true) {
                                content += '<span class="badge badge-pill badge-primary">Web</span>&nbsp;';
                            }

                            content += data.data.record_message;

                            return content;
                        }
                    },
                ];

                /*** GRID OPTIONS ***/
                let gridOptions = {
                    columnDefs: columnDefs,
                    rowSelection: "multiple",
                    floatingFilter: true,
                    pagination: true,
                    paginationPageSize: 100,
                    //sortingOrder: ['desc','asc',null],
                    pivotPanelShow: "always",
                    colResizeDefault: "shift",
                    animateRows: true,
                    onGridReady: function (params) {
                        // in this example, the CSS styles are loaded AFTER the grid is created,
                        // so we put this in a timeout, so height is calculated after styles are applied.
                        setTimeout(function () {
                            params.api.setRowData(response.data.data);
                        }, 2000);
                    }
                };
                /*** DEFINED TABLE VARIABLE ***/
                const gridTable = document.getElementById("recordHistoryTable");
                /*** INIT TABLE ***/
                new agGrid.Grid(gridTable, gridOptions);
            }).catch(error => {
                this.recordHistory = error.response.data;
            });
        },
        acsCheck() {
            axios.get('api/player/' + Player.PlayerID + '/acs').then(response => {
                this.acs = response.data;

                const columnDefs = [
                    {
                        headerName: 'Weapon',
                        field: 'slug',
                        resizable: true,
                        filter: true,
                        cellRenderer: (data) => {
                            return data.data.slug.toUpperCase();
                        },
                    },
                    {
                        headerName: 'Category',
                        field: 'category',
                        sortable: true,
                        filter: true,
                        resizable: true,
                        sort: {direction: 'asc', priority: 0}
                    },
                    {
                        headerName: 'DPS',
                        field: 'dps',
                        cellRenderer: (data) => {
                            return data.data.dps.toLocaleString() + '%';
                        },
                        cellStyle: function (data) {
                            if (data.data.triggered.DPS) {
                                return {color: 'white', backgroundColor: 'red'};
                            }

                            return {color: 'white', backgroundColor: 'green'}
                        }
                    },
                    {
                        headerName: 'HSKP',
                        field: 'hskp',
                        cellRenderer: (data) => {
                            return data.data.hskp.toLocaleString() + '%';
                        },
                        cellStyle: function (data) {
                            if (data.data.triggered.HKP) {
                                return {color: 'white', backgroundColor: 'red'};
                            }

                            return {color: 'white', backgroundColor: 'green'}
                        }
                    },
                    {
                        headerName: 'KPM',
                        field: 'kpm',
                        cellStyle: function (data) {
                            if (data.data.triggered.KPM) {
                                return {color: 'white', backgroundColor: 'red'};
                            }

                            return {color: 'white', backgroundColor: 'green'}
                        }
                    },
                    {
                        headerName: 'Kills',
                        field: 'kills',
                        sortable: true,
                        sort: {direction: 'desc', priority: 0},
                        cellRenderer: (data) => {
                            return data.data.kills.toLocaleString();
                        }
                    },
                    {
                        headerName: 'Fired',
                        field: 'fired',
                        cellRenderer: (data) => {
                            return data.data.fired.toLocaleString();
                        }
                    },
                    {
                        headerName: 'Hit',
                        field: 'hit',
                        cellRenderer: (data) => {
                            return data.data.hit.toLocaleString();
                        }
                    },
                    {
                        headerName: 'Headshots',
                        field: 'headshots',
                        cellRenderer: (data) => {
                            return data.data.headshots.toLocaleString();
                        }
                    },
                    {
                        headerName: 'Accuracy',
                        field: 'accuracy',
                        cellRenderer: (data) => {
                            return data.data.accuracy.toLocaleString() + '%';
                        }
                    },
                    {
                        headerName: 'Playtime',
                        field: 'timeEquipped',
                        cellRenderer: (data) => {
                            return moment.duration(data.data.timeEquipped, "s").humanize()
                        }
                    }
                ];

                /*** GRID OPTIONS ***/
                let gridOptions = {
                    columnDefs: columnDefs,
                    rowData: this.acs.data,
                    rowSelection: "single",
                    floatingFilter: true,
                    multiSortKey: 'ctrl',
                    //sortingOrder: ['desc','asc',null],
                    colResizeDefault: "shift",
                    animateRows: true
                };
                /*** DEFINED TABLE VARIABLE ***/
                const gridTable = document.getElementById("anticheatTable");
                /*** INIT TABLE ***/
                new agGrid.Grid(gridTable, gridOptions);
            }).catch(error => {
                this.acs = error.response.data;
            });
        }
    }
});
