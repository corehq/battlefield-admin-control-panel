<!-- BEGIN: Footer-->
<footer class="footer {{ $configData['footerType'] }} footer-light @if($configData["mainLayoutType"] == 'horizontal') navbar-shadow @endif">
    <p class="clearfix blue-grey lighten-2 mb-0">
        <button class="btn btn-primary btn-icon scroll-top" type="button">
            <i class="feather icon-arrow-up"></i>
        </button>

        <span class="float-md-left d-block d-md-inline-block">
            {{ app(\App\Helpers\Main::class)->executionTime(true) }}
        </span>
    </p>
</footer>
<!-- END: Footer-->