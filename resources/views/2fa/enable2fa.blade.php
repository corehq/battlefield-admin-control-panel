@extends('layouts/contentLayoutMaster')

@section('title', 'Google 2FA Setup')

@section('vendor-style')
    <!-- vednor css files -->
@endsection

@section('vendor-script')
    <!-- vednor files -->
@endsection

@section('mystyle')
    <!-- Page css files -->
@endsection


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="card">
                    <div class="card-header">Enable 2FA using Google Authenticator</div>
                    <div class="card-body" style="text-align: center;">
                        <p>Scan the below QR code using Authenticator App to set up your 2FA. Alternatively, you can use
                            the code</p>
                        <p><strong> {{ $secret }} </strong></p>
                        <div>
                            <img src="{{ $QRCode_img }}">
                        </div>
                    </div>
                    <div class="card-body pt-1">
                        <form class="form-horizontal" method="POST" action="{{ url('validate2fa') }}">
                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="number" class="form-control" id=otp" name="otp" placeholder="OTP" required>
                                <div class="form-control-position">
                                    <i class="feather icon-lock"></i>
                                </div>
                            </fieldset>
                            @if (session('error'))
                                <span class="help-block text-danger">
                            <strong>{{ session('error') }}</strong>
                        </span>
                            @endif
                            <button type="submit" class="btn btn-primary float-right btn-inline">
                                Authenticate
                            </button>
                            {!! csrf_field() !!}
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection