<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Register - {{ config('bfacp.community_name', 'Battlefield Admin Control Panel') }}</title>
    <link rel="apple-touch-icon" href="{{ asset('images/ico/apple-icon-120.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/ico/favicon.ico') }}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('vendors/css/vendors.min.css')) }}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/bootstrap.css')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/bootstrap-extended.css')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/colors.css')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/components.css')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/themes/dark-layout.css')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/themes/semi-dark-layout.css')) }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/core/menu/menu-types/vertical-menu.css')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/core/colors/palette-gradient.css')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/pages/authentication.css')) }}">
    <!-- END: Page CSS-->
    <style type="text/css">
        html body.bg-full-screen-image {
            background: url({{ asset('images/backgrounds/login-bf4-background.png') }}) no-repeat center center !important;
        }
    </style>

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout dark-layout 1-column footer-static bg-full-screen-image blank-page">
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-body">
            <section class="row flexbox-container">
                <div class="col-xl-3 d-flex justify-content-center">
                    <div class="card bg-authentication rounded-0 mb-0">
                        <div class="row m-0">
                            <div class="col-lg-12 p-0">
                                <div class="card rounded-0 mb-0 px-2">
                                    <div class="card-header pb-1">
                                        <div class="card-title">
                                            <h4 class="mb-0">Create Account</h4>
                                        </div>
                                    </div>
                                    <p class="px-2">Fill the below form to create a new account.</p>
                                    <div class="px-2">
                                        @if($errors->has('terms_accept'))
                                            <div class="alert alert-danger">
                                                <i class="fa fa-exclamation"></i>
                                                {{ $errors->first('terms_accept') }}
                                            </div>
                                        @endif

                                        @if($errors->has('privacy_accept'))
                                            <div class="alert alert-danger">
                                                <i class="fa fa-exclamation"></i>
                                                {{ $errors->first('privacy_accept') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body pt-1">
                                            <form action="{{ route('register') }}" method="POST" id="regForm">
                                                <fieldset
                                                        class="form-label-group form-group position-relative has-icon-left">
                                                    <input type="text"
                                                           class="form-control @if ($errors->has('username')) is-invalid @endif"
                                                           id="user-name" placeholder="Username" required
                                                           name="username" value="{{ old('username') }}"
                                                           v-model="username">
                                                    <label for="user-name">
                                                        Username
                                                    </label>
                                                    @if($errors->has('username'))
                                                        <div class="invalid-feedback">
                                                            {{ $errors->first('username') }}
                                                        </div>
                                                    @endif
                                                    <div class="help-block">
                                                        <small class="text-warning">
                                                            <i class="fa fa-warning"></i>
                                                            Choose carefully, your username cannot be changed later.
                                                        </small>
                                                    </div>
                                                </fieldset>

                                                <fieldset
                                                        class="form-label-group form-group position-relative has-icon-left">
                                                    <input type="text"
                                                           class="form-control @if ($errors->has('display_name')) is-invalid @endif"
                                                           id="display_name" placeholder="Display Name" required
                                                           name="display_name" value="{{ old('display_name') }}"
                                                           v-model="displayName">
                                                    <label for="display_name">Display Name</label>
                                                    @if($errors->has('display_name'))
                                                        <div class="invalid-feedback">
                                                            {{ $errors->first('display_name') }}
                                                        </div>
                                                    @endif
                                                </fieldset>

                                                <fieldset
                                                        class="form-label-group form-group position-relative has-icon-left">
                                                    <input type="email"
                                                           class="form-control @if ($errors->has('email')) is-invalid @endif"
                                                           id="email" placeholder="Email" required name="email"
                                                           value="{{ old('email') }}" v-model="email">
                                                    <label for="email">Email</label>
                                                    @if($errors->has('email'))
                                                        <div class="invalid-feedback">
                                                            {{ $errors->first('email') }}
                                                        </div>
                                                    @endif
                                                </fieldset>

                                                <fieldset
                                                        class="form-label-group form-group position-relative has-icon-left">
                                                    <select name="timezone" id="timezone"
                                                            class="form-control @if ($errors->has('timezone')) is-invalid @endif">
                                                        <option value=""
                                                                @if(session()->hasOldInput() === false) selected
                                                                @endif disabled>Select Timezone...
                                                        </option>
                                                        @foreach (timezone_identifiers_list() as $key => $timezone)
                                                            <option value="{{ $timezone }}"
                                                                    @if(old('timezone') == $timezone) selected @endif>{{ $timezone }}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="timezone">Timezone</label>
                                                    @if($errors->has('timezone'))
                                                        <div class="invalid-feedback">
                                                            {{ $errors->first('timezone') }}
                                                        </div>
                                                    @endif
                                                </fieldset>

                                                <fieldset class="form-label-group position-relative has-icon-left">
                                                    <input type="password"
                                                           class="form-control @if ($errors->has('password')) is-invalid @endif"
                                                           id="user-password"
                                                           placeholder="Password" required name="password"
                                                           v-model="password">
                                                    <label for="user-password">Password</label>
                                                    <div class="help-block" v-show="password.length >= 8">
                                                        <small v-bind:class="passwordClass">
                                                            <i v-bind:class="passwordIcon"></i>
                                                            <span v-text="passwordWarning"
                                                                  v-bind:class="passwordClass"></span>
                                                        </small>
                                                        <ul class="list-style-circle"
                                                            v-if="passwordSuggestions.length > 0">
                                                            <li class="text-small text-info"
                                                                v-for="s in passwordSuggestions">
                                                                @{{ s }}
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    @if($errors->has('password'))
                                                        <div class="invalid-feedback">
                                                            {{ $errors->first('password') }}
                                                        </div>
                                                    @endif
                                                </fieldset>

                                                <fieldset class="form-label-group position-relative has-icon-left">
                                                    <input type="password" class="form-control"
                                                           id="user-password-confirm"
                                                           placeholder="Password Confirmation" required
                                                           name="password_confirmation">
                                                    <label for="user-password-confirm">Password Confirmation</label>
                                                </fieldset>

                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <fieldset class="checkbox">
                                                            <div class="vs-checkbox-con vs-checkbox-primary">
                                                                <input type="checkbox" name="terms_accept" value="1"
                                                                       @if(old('terms_accept') == 1) checked @endif>
                                                                <span class="vs-checkbox">
                                                                    <span class="vs-checkbox--check">
                                                                      <i class="vs-icon feather icon-check"></i>
                                                                    </span>
                                                                  </span>
                                                                <span class="">I accept the terms & conditions.</span>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-12">
                                                        <fieldset class="checkbox">
                                                            <div class="vs-checkbox-con vs-checkbox-primary">
                                                                <input type="checkbox" name="privacy_accept" value="1"
                                                                       @if(old('privacy_accept') == 1) checked @endif>
                                                                <span class="vs-checkbox">
                                                                    <span class="vs-checkbox--check">
                                                                      <i class="vs-icon feather icon-check"></i>
                                                                    </span>
                                                                  </span>
                                                                <span class="">I accept the privacy policy.</span>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-12 mt-1">
                                                        <a href="{{ route('terms') }}" class="btn btn-primary btn-sm"
                                                           target="_blank">Terms &amp; Conditions</a>
                                                        <a href="{{ route('privacy') }}" class="btn btn-primary btn-sm"
                                                           target="_blank">Privacy Policy</a>
                                                    </div>
                                                </div>

                                                <a href="{{ route('user.login') }}"
                                                   class="btn btn-outline-primary float-left btn-inline">Login</a>
                                                <button type="submit" class="btn btn-primary float-right btn-inline">
                                                    Register
                                                </button>
                                                {!! csrf_field() !!}
                                            </form>
                                        </div>
                                    </div>
                                    <div class="login-footer">
                                        &nbsp;
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>
</div>
<!-- END: Content-->

<!-- BEGIN: Vendor JS-->
<script src="{{ asset(mix('vendors/js/vendors.min.js')) }}"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{{ asset(mix('js/core/app-menu.js')) }}"></script>
<script src="{{ asset(mix('js/core/app.js')) }}"></script>
<script src="{{ asset(mix('js/app.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/components.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/register.js')) }}"></script>
<!-- END: Theme JS-->

</body>
<!-- END: Body-->

</html>