<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Forgot Password - {{ config('bfacp.community_name', 'Battlefield Admin Control Panel') }}</title>
    <link rel="apple-touch-icon" href="{{ asset('images/ico/apple-icon-120.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/ico/favicon.ico') }}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('vendors/css/vendors.min.css')) }}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/bootstrap.css')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/bootstrap-extended.css')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/colors.css')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/components.css')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/themes/dark-layout.css')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/themes/semi-dark-layout.css')) }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/core/menu/menu-types/vertical-menu.css')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/core/colors/palette-gradient.css')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/pages/authentication.css')) }}">
    <!-- END: Page CSS-->
    <style type="text/css">
        html body.bg-full-screen-image {
            background: url({{ asset('images/backgrounds/login-bf4-background.png') }}) no-repeat center center !important;
        }
    </style>

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout dark-layout 1-column footer-static bg-full-screen-image blank-page">
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-body">
            <section class="row flexbox-container">
                <div class="col-xl-3 d-flex justify-content-center">
                    <div class="card bg-authentication rounded-0 mb-0">
                        <div class="row m-0">
                            <div class="col-lg-12 p-0">
                                <div class="card rounded-0 mb-0 px-2">
                                    <div class="card-header pb-1">
                                        <div class="card-title">
                                            <h4 class="mb-0">Recover your password</h4>
                                        </div>
                                    </div>
                                    <p class="px-2">Please enter your email address and we'll send you instructions on
                                        how to reset your password.</p>
                                    <div class="card-content">
                                        <div class="card-body pt-1">
                                            @if (session('status'))
                                                <div class="alert alert-success" role="alert">
                                                    {{ session('status') }}
                                                </div>
                                            @endif
                                            <form action="{{ route('password.email') }}" method="POST">
                                                <div class="form-label-group">
                                                    <!-- <input type="email" id="inputEmail" class="form-control" placeholder="Email"> -->
                                                    <input id="email" type="email"
                                                           class="form-control @error('email') is-invalid @enderror"
                                                           name="email" value="{{ old('email') }}" placeholder="Email"
                                                           required autocomplete="email" autofocus>

                                                    <label for="email">Email</label>

                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>

                                                <div class="float-md-left d-block mb-1">
                                                    <a href="{{ route('user.login') }}"
                                                       class="btn btn-outline-primary btn-block px-75">Back
                                                        to Login</a>
                                                </div>
                                                <div class="float-md-right d-block mb-1">
                                                    <button type="submit" class="btn btn-primary btn-block px-75">
                                                        Recover Password
                                                    </button>
                                                </div>
                                                {!! csrf_field() !!}
                                            </form>
                                        </div>
                                    </div>
                                    <div class="login-footer">
                                        &nbsp;
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>
</div>
<!-- END: Content-->

<!-- BEGIN: Vendor JS-->
<script src="{{ asset(mix('vendors/js/vendors.min.js')) }}"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{{ asset(mix('js/core/app-menu.js')) }}"></script>
<script src="{{ asset(mix('js/core/app.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/components.js')) }}"></script>
<!-- END: Theme JS-->

</body>
<!-- END: Body-->

</html>