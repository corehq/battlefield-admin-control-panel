@extends('layouts/contentLayoutMaster')

@section('title', 'Ban List')

@section('vendor-style')
    <!-- vednor css files -->
@endsection

@section('mystyle')
    <!-- Page css files -->
@endsection

@section('vendor-script')
    <!-- vednor files -->
@endsection

@section('myscript')
    <!-- Page js files -->
    <script type="text/javascript">
        new Vue({
            el: '#banList'
        });
    </script>
@endsection

@section('content')
    <section id="banList">
        <div class="row">
            <div class="col-12">
                <admin-adkats-ban-list></admin-adkats-ban-list>
            </div>
        </div>
    </section>
@endsection