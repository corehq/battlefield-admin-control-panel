@extends('layouts/contentLayoutMaster')

@section('title', 'Profile')

@section('vendor-style')
    <!-- vednor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
@endsection

@section('mystyle')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/pages/dashboard-analytics.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/pages/card-analytics.css')) }}">
@endsection

@section('vendor-script')
    <!-- vednor files -->
    <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
@endsection


@section('content')
    <section id="dashboard-app">
        {{-- 2FA--}}
        <div class="row">
            @if (Auth::user()->google2fa_enabled)
                <div class="col-12 mb-1">
                    <div class="card">
                        <div class="card-content">
                            <div class="col-sm-6 col-12">

                                <div class="text-bold-600 font-medium-2 mb-1">
                                    Enter your Current Password to disable 2FA
                                </div>
                                <form method="POST" action="{{ url('disable2fa') }}">
                                    @csrf
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <input type="password" class="form-control" id="password" name="password"
                                               placeholder="Current Password" required>
                                        <div class="form-control-position">
                                            <i class="feather icon-lock"></i>
                                        </div>
                                    </fieldset>
                                    <button type="submit" class="btn btn-warning mr-1 mb-1 waves-effect waves-light">
                                        Disable 2FA
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <a href="{{ url('enable2fa') }}" class="btn btn-primary">Enable 2FA</a>
            @endif
        </div>
    </section>
@endsection