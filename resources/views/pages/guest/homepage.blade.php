@extends('layouts.guest.master')

@section('title', 'Homepage')

@section('scripts')
    <script src="{{ asset('frontend/js/moment.js') }}"></script>
    <script src="{{ asset('frontend/js/app.js') }}"></script>
@stop

@section('content')
    <section id="app">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">Active Server List</div>
                        <table class="table table-striped table-bordered table-hover" id="serverlist">
                            <thead>
                            <th scope="col">Server</th>
                            <th scope="col">Game</th>
                            <th scope="col">Players</th>
                            <th scope="col">Map</th>
                            <th scope="col">Mode</th>
                            </thead>

                            <tbody>
                            @foreach ($servers as $server)
                                <tr>
                                    <td>{{ $server->server_name_short ?? $server->ServerName }}</td>
                                    <td>{{ $server->game->Name }}</td>
                                    <td>{{ $server->usedSlots }} / {{ $server->maxSlots }}</td>
                                    <td>{{ $server->current_map }}</td>
                                    <td>{{ $server->current_gamemode }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">Recent Bans</div>
                        <table class="table table-striped table-hover" id="banlist">
                        <thead>
                        <th scope="col">ID</th>
                        <th scope="col">Server</th>
                        <th scope="col">Player</th>
                        <th scope="col">Expires</th>
                        <th scope="col">Reason</th>
                        </thead>

                        <tbody>
                        @foreach ($latestBans as $ban)
                            <tr class="{{ $ban->color_class['frontend']['table'] }}">
                                <td>{{ $ban->ban_id }}</td>
                                <td>
                                    <span class="badge">
                                        {{ $ban->record->server->game->Name }}
                                    </span>
                                    {{ $ban->record->server->server_name_short ?? $ban->record->server->ServerName }}
                                </td>
                                <td>{{ $ban->player->SoldierName }}</td>
                                <td>
                                    @if ($ban->is_perm)
                                        <span class="label {{ $ban->color_class['frontend']['label'] }}">
                                            Permanent
                                        </span>
                                    @else
                                        <span class="label {{ $ban->color_class['frontend']['label'] }}"
                                              title="{{ $ban->ban_endTime->toRfc850String() }}">
                                                @{{ moment("@php echo $ban->ban_expires @endphp").fromNow() }}
                                            </span>
                                    @endif
                                </td>
                                <td>{{ $ban->record->record_message }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">Recent Player Reports</div>
                        <table class="table table-striped table-hover" id="recent_reports">
                            <thead>
                            <th scope="col">Server</th>
                            <th scope="col">Time</th>
                            <th scope="col">Reported</th>
                            <th scope="col">Reason</th>
                            </thead>

                            <tbody>
                            @foreach($recentReports as $report)
                                <tr>
                                    <td>
                                    <span class="badge">
                                        {{ $report->server->game->Name }}
                                    </span>
                                        {{ $report->server->server_name_short ?? $report->server->ServerName }}
                                    </td>
                                    <td>
                                    <span title="{{ $report->record_time->toRfc850String() }}">
                                        @{{ moment("@php echo $report->record_time->toIso8601String() @endphp").fromNow() }}
                                    </span>
                                    </td>
                                    <td>
                                        <span title="From: {{ $report->source_name }}">
                                            {{ $report->target_name ?? 'N/A' }}
                                        </span>
                                    </td>
                                    <td>{{ $report->record_message }}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop