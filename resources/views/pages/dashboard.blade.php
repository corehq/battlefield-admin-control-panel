@extends('layouts/contentLayoutMaster')

@section('title', 'Dashboard')

@section('vendor-style')
    <!-- vednor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
@endsection

@section('mystyle')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/pages/dashboard-analytics.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/pages/card-analytics.css')) }}">
@endsection

@section('vendor-script')
    <!-- vednor files -->
    <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
@endsection
@section('myscript')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/pages/dashboard.js')) }}"></script>
@endsection

@section('content')
    <section id="dashboard-app">
        {{-- START Row 1 Stats --}}
        <div class="row">
            <div class="col-lg-4 col-md-6 col-12">
                <div class="card">
                    <div class="card-header d-flex flex-column align-items-start pb-0">
                        <div class="avatar bg-rgba-primary p-50 m-0">
                            <div class="avatar-content">
                                <i class="feather icon-users text-primary font-medium-5"></i>
                            </div>
                        </div>
                        <h2 class="text-bold-700 mt-1 mb-25">
                            @{{ players.total | formatnumber }}
                        </h2>
                        <p class="mb-0">Players &verbar; <small><span v-if="servers.population"
                                                                      v-text="servers.population.used"></span> &frasl;
                                <span v-if="servers.population" v-text="servers.population.total"></span></small></p>
                    </div>
                    <div class="card-content">
                        <div id="player-history-chart"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-12">
                <div class="card">
                    <div class="card-header d-flex flex-column align-items-start pb-0">
                        <div class="avatar bg-rgba-warning p-50 m-0">
                            <div class="avatar-content">
                                <i class="fa fa-gavel text-danger font-medium-5"></i>
                            </div>
                        </div>
                        <h2 class="text-bold-700 mt-1 mb-25">
                            @{{ bans.yesterday | formatnumber }}
                        </h2>
                        <p class="mb-0">Bans Issued Yesterday &verbar;
                            <small>Avg:
                                <span>@{{ bans.avg | formatnumber }}</span>
                            </small>
                        </p>
                    </div>
                    <div class="card-content">
                        <div id="ban-history-chart"></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-5 col-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between pb-0">
                        <h4 class="card-title">Reports Tracker</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body pt-0">
                            <div class="row">
                                <div class="col-sm-2 col-12 d-flex flex-column flex-wrap text-center">
                                    <h1 class="font-large-2 text-bold-700 mt-2 mb-0">@{{ reports.total | formatnumber
                                        }}</h1>
                                    <small>Total Reports</small>
                                </div>
                                <div class="col-sm-10 col-12 d-flex justify-content-center">
                                    <div id="reports-chart"></div>
                                </div>
                            </div>
                            <div class="chart-info d-flex justify-content-between">
                                <div class="text-center">
                                    <p class="mb-50">Confirmed Reports</p>
                                    <span class="font-large-1">@{{ reports.acked | formatnumber }}</span>
                                </div>
                                <div class="text-center">
                                    <p class="mb-50">Open Reports</p>
                                    <span class="font-large-1">@{{ reports.unacked | formatnumber }}</span>
                                </div>
                                <div class="text-center">
                                    <p class="mb-50">Expired Reports</p>
                                    <span class="font-large-1">@{{ reports.expired | formatnumber }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- END Row 1 Stats --}}

        <div class="row">
            <div class="col-xl-2 col-md-4 col-sm-6">
                <card :use-body="true" :classes="'text-center'">
                    <div class="avatar bg-rgba-warning p-50 m-0 mb-1">
                        <div class="avatar-content">
                            <i class="feather icon-log-out text-info font-medium-5"></i>
                        </div>
                    </div>
                    <h2 class="text-bold-700">
                        @{{ adkats.kicked_players | formatnumber }}
                        <small class="warning">@{{ adkats.kicked_players | percentage(players.total)
                            }}&percnt;</small>
                    </h2>
                    <p class="mb-0 line-ellipsis">Kicked Players</p>
                </card>
            </div>

            <div class="col-xl-2 col-md-4 col-sm-6">
                <card :use-body="true" :classes="'text-center'">
                    <div class="avatar bg-rgba-warning p-50 m-0 mb-1">
                        <div class="avatar-content">
                            <i class="fa fa-bar-chart text-info font-medium-5"></i>
                        </div>
                    </div>
                    <h2 class="text-bold-700">
                        @{{ adkats.killed_players | formatnumber }}
                        <small class="warning">@{{ adkats.killed_players | percentage(players.total)
                            }}&percnt;</small>
                    </h2>
                    <p class="mb-0 line-ellipsis">Killed Players</p>
                </card>
            </div>

            <div class="col-xl-2 col-md-4 col-sm-6">
                <card :use-body="true" :classes="'text-center'">
                    <div class="avatar bg-rgba-warning p-50 m-0 mb-1">
                        <div class="avatar-content">
                            <i class="fa fa-gavel text-danger font-medium-5"></i>
                        </div>
                    </div>
                    <h2 class="text-bold-700">
                        @{{ adkats.banned_active | formatnumber }}
                        <small class="warning">@{{ adkats.banned_active | percentage(players.total)
                            }}&percnt;</small>
                    </h2>
                    <p class="mb-0 line-ellipsis">Banned Players</p>
                </card>
            </div>

            <div class="col-xl-2 col-md-4 col-sm-6">
                <average-daily-reports-stat></average-daily-reports-stat>
            </div>

            <div class="col-xl-2 col-md-4 col-sm-6">
                <average-daily-players></average-daily-players>
            </div>

        </div>

        {{-- START Row 2 --}}
        <div class="row">
            <div class="col-lg-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="mb-0">Servers</h4>
                    </div>
                    <div class="card-content">
                        <div class="table-responsive mt-1">
                            <table class="table table-dark mb-0">
                                <thead>
                                <tr>
                                    <th colspan="2">Name</th>
                                    <th style="width: 80px">Online</th>
                                    <th colspan="2">Map</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="server in servers.servers" :value="server.ServerID">
                                    <td>
                                        <span v-text="server.game.Name" v-bind:class="server.game.class_css"></span>
                                    </td>
                                    <td v-text="server.server_name_short || server.ServerName"></td>
                                    <td>
                                        <span v-text="server.usedSlots"></span> &frasl;
                                        <span v-text="server.maxSlots"></span>
                                    </td>
                                    <td v-text="server.current_map"></td>
                                    <td>
                                        <div v-if="serverSpark(server.maps, server.ServerID)"
                                             v-bind:id="'server-'+server.ServerID+'-chart'"></div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="card-footer">
                        <div class="progress progress-lg"
                             v-bind:class="[{ 'progress-bar-danger': population.danger, 'progress-bar-warning': population.warning, 'progress-bar-success': !population.danger && !population.warning}]">
                            <div class="progress-bar" role="progressbar"
                                 v-bind:style="{ width: population.pct + '%' }"
                                 v-bind:aria-valuenow="population.pct" aria-valuemin="0" aria-valuemax="100">
                                @{{ population.pct }}&percnt;
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Activity Timeline</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <ul class="activity-timeline timeline-left list-unstyled">
                                <li v-for="r in activitys">
                                    <div class="timeline-icon bg-primary">
                                        <i class="feather icon-plus font-medium-2 align-middle"></i>
                                    </div>
                                    <div class="timeline-info">
                                        <p class="font-weight-bold mb-0" v-bind:title="r.server.ServerName">
                                            <a v-if="r.source_id !== null" v-text="r.source_name"
                                               v-bind:href="r.source.profile_url"></a>
                                            <span v-else v-text="r.source_name"></span>&nbsp;&rightarrow;&nbsp;
                                            <a v-if="r.target_id !== null" v-text="r.target_name"
                                               v-bind:href="r.target.profile_url"></a>
                                            <span v-else v-text="r.target_name"></span>
                                        </p>
                                        <span class="font-small-3">
                                            @{{ r.type.command_name }} &rightarrow; @{{ r.record_message }}
                                        </span>
                                    </div>
                                    <small class="text-muted" v-text="moment(r.stamp).fromNow()"
                                           v-bind:title="moment(r.stamp).format('llll')"></small>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- END Row 2 --}}

        {{-- START Row 3 --}}
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <latest-bans></latest-bans>

            </div>
            <div class="col-lg-6 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="mb-0">Top 10 Reputable Players</h4>
                    </div>
                    <div class="card-content">
                        <div class="table-responsive mt-1">
                            <table class="table mb-0">
                                <thead>
                                <tr>
                                    <th>Player</th>
                                    <th>Game</th>
                                    <th>Reputation</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="rep in reputation.players">
                                    <td>
                                        <a v-bind:href="rep.player.profile_url">
                                            <div class="avatar avatar-sm" v-if="rep.player.battlelog !== null">
                                                <img v-bind:src="rep.player.battlelog.gravatar_img"/>
                                            </div>
                                            <span v-if="rep.player.ClanTag !== null"
                                                  v-text="'['+rep.player.ClanTag+']'"></span>
                                            <span v-text="rep.player.SoldierName"></span>
                                        </a>
                                    </td>
                                    <td>
                                        <span v-text="rep.player.game.Name"
                                              v-bind:class="rep.player.game.class_css"></span>
                                    </td>
                                    <td v-text="rep.total_rep_co" v-bind:class="rep.color"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- END Row 3 --}}

        <div class="row">
            <div class="col-6">

            </div>
        </div>

        {{-- START Row 4 --}}
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">
                            Player Feedback
                        </h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" id="feedbackTable" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Server</th>
                                        <th>Timestamp</th>
                                        <th>Player</th>
                                        <th>Feedback</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="record in feedback">
                                        <td v-text="record.record_id"></td>
                                        <td>
                                            <span v-text="record.server.game.Name"
                                                  v-bind:class="record.server.game.class_css"></span>
                                            <span v-text="record.server.server_name_short || record.server.ServerName"></span>
                                        </td>
                                        <td v-text="moment(record.stamp).fromNow()"
                                            v-bind:title="moment(record.stamp).format('llll')"></td>
                                        <td v-if="record.source !== null"><a v-bind:href="record.source.profile_url"
                                                                             v-text="record.source_name"></a></td>
                                        <td v-else v-text="record.source_name"></td>
                                        <td v-text="record.record_message"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- END Row 4 --}}
    </section>
@endsection