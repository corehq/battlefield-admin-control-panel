@extends('layouts/contentLayoutMaster')

@section('title', trans('player.profile.title', [
    'name' => $player->SoldierName,
    'game' => $player->game->Name
]))

@section('vendor-style')
    <!-- vednor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/ag-grid/ag-grid.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/ag-grid/ag-theme-material.css')) }}">
@endsection

@section('vendor-script')
    <!-- vednor files -->
    <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/ag-grid/ag-grid-community.min.noStyle.js')) }}"></script>
@endsection

@section('mystyle')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/pages/users.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/pages/aggrid.css')) }}">
@endsection

@section('myscript')
    <!-- Page js files -->
    <script type="text/javascript">
        window.Player = {
            PlayerID: {{ $player->PlayerID }},
            SoldierName: '{{ $player->SoldierName }}',
            Game: {!! $player->game->toJson() !!}
        };
    </script>
    <script type="module" src="{{ asset(mix('js/scripts/pages/player/player-profile.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/navs/navs.js')) }}"></script>
@endsection

@section('content')
    <div class="content-wrapper" id="player-overview-app">
        <div class="content-body">
            <div id="user-profile">
                <div class="row">
                    <div class="col-12">
                        <div class="profile-header mb-2">
                            <div class="relative">
                                <div class="cover-container">
                                </div>
                                <div class="profile-img-container d-flex align-items-center justify-content-between">
                                    <img src="{{ $player->battlelog->gravatar_img ?? app(\App\Helpers\Main::class)->gravatar() }}"
                                         class="rounded-circle img-border box-shadow-1">
                                </div>
                            </div>
                            <div class="d-flex justify-content-end align-items-center profile-header-nav">
                                <nav class="navbar navbar-expand-sm w-100 pr-0">
                                    <div class="navbar-collapse">
                                        <ul class="navbar-nav justify-content-around w-75 ml-sm-auto">
                                            @unless(!$previous)
                                                <li class="nav-item px-sm-0">
                                                    <a href="{{ route('player.profile', [$previous]) }}"
                                                       class="nav-link font-small-3"><i class="fa fa-arrow-left"></i>
                                                        Previous Player</a>
                                                </li>
                                            @endunless
                                            <li class="nav-item px-sm-0">
                                                <a href="{{ $player->links['battlelog'] }}"
                                                   class="nav-link font-small-3" target="_blank">Battlelog</a>
                                            </li>
                                            <li class="nav-item px-sm-0">
                                                <a href="{{ $player->links['fairplay'] }}#"
                                                   class="nav-link font-small-3" target="_blank">Fairplay</a>
                                            </li>
                                            <li class="nav-item px-sm-0">
                                                <a href="{{ $player->links['aci'] }}" class="nav-link font-small-3"
                                                   target="_blank">ACI</a>
                                            </li>
                                            @if($player->game->Name == 'BF4')
                                                <li class="nav-item px-sm-0">
                                                    <a href="{{ $player->links['bf4db'] }}"
                                                       class="nav-link font-small-3" target="_blank">BF4DB</a>
                                                </li>
                                            @endif
                                            @unless(!$next)
                                                <li class="nav-item px-sm-0">
                                                    <a href="{{ route('player.profile', [$next]) }}"
                                                       class="nav-link font-small-3">Next Player <i
                                                                class="fa fa-arrow-right"></i> </a>
                                                </li>
                                            @endunless
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <section id="profile-info">
                    <div class="row">
                        <div class="col-lg-4 col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{ trans('player.profile.overview.title') }}</h4>
                                </div>
                                <div class="card-content" aria-expanded="true">
                                    <div class="card-body">
                                        @empty(!$player->ClanTag)
                                            <div class="mt-1">
                                                <h6 class="mb-0">{{ trans('player.profile.overview.clan') }}&colon;</h6>
                                                <p>
                                                <span class="badge badge-glow">
                                                    {{ $player->ClanTag }}
                                                </span>
                                                </p>
                                            </div>
                                        @endunless
                                        <div class="mt-1">
                                            <h6 class="mb-0" data-toggle="tooltip" data-placement="left"
                                                data-original-title="{{ trans('player.profile.overview.reputation.tooltip') }}">
                                                {{ trans('player.profile.overview.reputation.title') }}&colon;
                                                <i class="fa fa-info-circle text-info" data-toggle="modal"
                                                   data-target="#playerRepGuideModal"></i>
                                            </h6>
                                            <p>
                                                @if($player->reputation)
                                                    <span class="{{ $player->reputation->color }} font-weight-bolder">
                                                    {{ $player->reputation->total_rep_co }}
                                                </span>
                                                @else
                                                    <span class="badge badge-warning">
                                                        No {{ trans('player.profile.overview.reputation.title') }}
                                                    </span>
                                                @endif
                                            </p>
                                        </div>
                                        @can('view.player.pbguid')
                                            @empty(!$player->PBGUID)
                                                <div class="mt-1">
                                                    <h6 class="mb-0">
                                                        {{ trans('player.profile.overview.guid.pb') }}&colon;</h6>
                                                    <p>
                                                        <a href="{{ $player->links['pbbans'] }}" target="_blank">
                                                            {{ $player->PBGUID }}
                                                        </a>
                                                    </p>
                                                </div>
                                            @endempty
                                        @endcan
                                        @can('view.player.eaguid')
                                            @empty(!$player->EAGUID)
                                                <div class="mt-1">
                                                    <h6 class="mb-0">
                                                        {{ trans('player.profile.overview.guid.ea') }}&colon;</h6>
                                                    <p>
                                                        {{ $player->EAGUID }}
                                                    </p>
                                                </div>
                                            @endempty
                                        @endcan
                                        @can('view.player.ip')
                                            @empty(!$player->IP_Address)
                                                <div class="mt-1">
                                                    <h6 class="mb-0">
                                                        {{ trans('player.profile.overview.ip') }}&colon;</h6>
                                                    <p>
                                                        {{ $player->IP_Address }}
                                                    </p>
                                                </div>
                                            @endempty
                                        @endcan
                                        <div class="mt-1">
                                            <h6 class="mb-0">
                                                {{ trans('player.profile.overview.country') }}&colon;</h6>
                                            <p>
                                                <img src="{{ asset($player->country_flag) }}"/>
                                                {{ $player->country_name }}
                                            </p>
                                        </div>
                                        @if(count($player->stats) > 0)
                                            <div class="mt-1">
                                                <h6 class="mb-0">
                                                    {{ trans('player.profile.overview.stats.first_seen') }}&colon;</h6>
                                                <p>
                                                    @php
                                                        $lastSeen = $player->stats->min('FirstSeenOnServer');
                                                        $stamp = \Carbon\Carbon::parse($lastSeen)->toIso8601String();
                                                    @endphp
                                                    <span v-text="moment('{{ $stamp }}').fromNow()"
                                                          v-bind:title="moment('{{ $stamp }}').format('llll')"></span>
                                                </p>
                                            </div>
                                            <div class="mt-1">
                                                <h6 class="mb-0">
                                                    {{ trans('player.profile.overview.stats.last_seen') }}&colon;</h6>
                                                <p>
                                                    @php
                                                        $lastSeen = $player->stats->max('LastSeenOnServer');
                                                        $stamp = \Carbon\Carbon::parse($lastSeen)->toIso8601String();
                                                    @endphp
                                                    <span v-text="moment('{{ $stamp }}').fromNow()"
                                                          v-bind:title="moment('{{ $stamp }}').format('llll')"></span>
                                                </p>
                                            </div>
                                            <div class="mt-1">
                                                <h6 class="mb-0">
                                                    {{ trans('player.profile.overview.stats.playtime') }}&colon;</h6>
                                                <p>
                                                    <span v-text="moment.duration({{ $player->stats->sum('Playtime') }}, 'seconds').humanize()"></span>
                                                </p>
                                            </div>
                                        @endif

                                        @empty(!$player->specialGroups)
                                            <div class="mt-1">
                                                <h6 class="mb-0">
                                                    {{ trans_choice('player.profile.overview.adkats.groups', count($player->specialGroups)) }}&colon;</h6>
                                                <ul class="list-inline">
                                                    @foreach ($player->specialGroups as $group)
                                                        <li class="list-inline-item badge-pill badge-success mb-1">
                                                            {{ $group['group']['group_name'] }}
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endempty
                                        <div class="mt-1">
                                            <h6 class="mb-0">{{ trans('player.profile.overview.rank') }}&colon;</h6>
                                            <p>
                                                <img src="{{ asset($player->rank_image) }}"
                                                     title="Rank {{ $player->GlobalRank }}" style="width: 100px"/>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-12">
                            <div class="card overflow-hidden">
                                <div class="card-content" aria-expanded="true">
                                    <div class="card-body">
                                        <overview-tabs></overview-tabs>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="card overflow-hidden">
                                <div class="card-content" aria-expanded="true">
                                    <div class="card-body">
                                        <ul class="nav nav-tabs nav-justified nav-fill">
                                            <li class="nav-item">
                                                <a class="nav-link" href="#acs"
                                                   aria-controls="acs" role="tab" aria-selected="true"
                                                   data-toggle="tab">
                                                    Anticheat
                                                    <span v-if="acs.status == 'ok'" v-text="acs.data.length"
                                                          class="badge badge-pill"
                                                          v-bind:class="[{'badge-danger': acs.data.length > 0 }, 'badge-success']"></span>
                                                </a>
                                            </li>

                                            <li class="nav-item">
                                                <a class="nav-link active" href="#records"
                                                   aria-controls="records" role="tab" aria-selected="true"
                                                   data-toggle="tab">
                                                    Records
                                                    <span class="badge badge-pill badge-sm"
                                                          v-if="recordHistory.status == 'ok'"
                                                          v-text="recordHistory.data.length.toLocaleString()"></span>
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane" id="acs" aria-labelledby="acs" role="tabpanel">
                                                <div class="alert bg-gradient-info" role="alert"
                                                     v-show="acs.status == 'ok'">
                                                    <h4 class="alert-heading"><i
                                                                class="fa fa-info-circle text-white"></i>&nbsp;
                                                        Weapons shown are not always suspicious and is only there to
                                                        help you decided if the player is cheating. Shotguns and Snipers
                                                        may trigger more frequently.
                                                    </h4>
                                                </div>
                                                <div class="alert bg-gradient-danger" role="alert"
                                                     v-if="acs.status == 'error'">
                                                    <h4 class="alert-heading"><i
                                                                class="fa fa-info-circle text-white"></i>&nbsp;
                                                        @{{ acs.message }}
                                                    </h4>
                                                </div>
                                                <div id="anticheatTable" class="aggrid ag-theme-material"
                                                     v-show="acs.status == 'ok'"></div>
                                            </div>
                                            <div class="tab-pane active" id="records" aria-labelledby="records"
                                                 role="tabpanel">
                                                @if(\Illuminate\Support\Facades\Cache::has(sprintf('player.%s.records', $player->PlayerID)))
                                                    <div class="alert bg-gradient-primary" role="alert">
                                                        <h4 class="alert-heading"><i class="fa fa-info-circle"></i>&nbsp;
                                                            You are viewing a cached version of this data.
                                                        </h4>
                                                    </div>
                                                @endif
                                                <div id="recordHistoryTable" class="aggrid ag-theme-material"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    @include('snippets.modals.reputation-guide')
@endsection