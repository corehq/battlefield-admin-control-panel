@extends('layouts/contentLayoutMaster')

@section('title', 'Player Listing')

@section('vendor-style')
    <!-- vednor css files -->
@endsection

@section('vendor-script')
    <!-- vednor files -->
@endsection

@section('mystyle')
    <!-- Page css files -->
@endsection

@section('myscript')
    <!-- Page js files -->
    <script src="{{ asset('js/scripts/pages/player/player-list.js') }}"></script>
@endsection

@section('content')
    <section id="player-list-app">
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <div class="card">
                    <form action="{{ route('player.list') }}" method="get" class="form form-vertical">
                        <div class="card-header">
                            <h4 class="card-title">Search</h4>
                        </div>
                        <div class="card-content" aria-expanded="true">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <fieldset>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">
                                                        <div class="vs-checkbox-con">
                                                            <input type="checkbox" value="1" id="wildcard"
                                                                   name="wildcard"
                                                                   @if (request()->has('wildcard') && request()->get('wildcard') == 1)
                                                                   checked="checked"
                                                                    @endif>
                                                            <span class="vs-checkbox vs-checkbox-sm">
                                                            <span class="vs-checkbox--check">
                                                              <i class="vs-icon feather icon-check"></i>
                                                            </span>
                                                          </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="text" id="playerName" name="playerName"
                                                       class="form-control round @if ($errors->has('playerName')) is-invalid @endif"
                                                       placeholder="Player Name"
                                                       value="{{ request()->get('playerName') }}"
                                                       autocomplete="off"/>
                                            </div>
                                        </fieldset>
                                        <p>
                                            <small class="text-muted">Search for multiple players by separating the
                                                names with a comma (&comma;).<br/>Check the box to enable a wildcard
                                                search.<br/>
                                                <span class="text-warning">
                                                <i class="fa fa-warning"></i>
                                                This is a more intensive operation so request will take longer.
                                            </span>
                                            </small><br/>
                                            <small class="text-info">
                                                <i class="fa fa-info-circle"></i>
                                                Only player names support multiple entries.
                                            </small>
                                        </p>
                                        @if($errors->has('playerName'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('playerName') }}
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="row mt-0">
                                    <div class="col-sm-12">
                                        <input type="text" id="playerIp" name="playerIp"
                                               class="form-control round @if ($errors->has('playerIp')) is-invalid @endif"
                                               placeholder="Player IP" value="{{ request()->get('playerIp') }}"
                                               autocomplete="off">
                                        @if($errors->has('playerIp'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('playerIp') }}
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="row mt-1">
                                    <div class="col-sm-12">
                                        <input type="text" id="eaguid" name="eaguid"
                                               class="form-control round @if ($errors->has('eaguid')) is-invalid @endif"
                                               placeholder="EA GUID" value="{{ request()->get('eaguid') }}"
                                               autocomplete="off">
                                        @if($errors->has('eaguid'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('eaguid') }}
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="row mt-1">
                                    <div class="col-sm-12">
                                        <input type="text" id="pbguid" name="pbguid"
                                               class="form-control round @if ($errors->has('pbguid')) is-invalid @endif"
                                               placeholder="PB GUID" value="{{ request()->get('pbguid') }}"
                                               autocomplete="off">
                                        @if($errors->has('pbguid'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('pbguid') }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <p>
                                <small class="text-muted">
                                    You can do multiple searches by filling in the fields above. <br/>
                                    <span class="text-info">
                                        <i class="fa fa-info"></i>
                                        If a name matches the players old name it will be shown in a red badge.
                                    </span>
                                </small>
                            </p>
                            <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Search
                            </button>
                            <a href="{{ route('player.list') }}"
                               class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-9 col-md-12">
                <div class="card">
                    <div class="card-header align-self-center">
                        {!! $players->links() !!}
                    </div>
                    <div class="card-content" aria-expanded="true">
                        <div class="card-body table-responsive">
                            <table class="table table-dark table-striped">
                                <thead>
                                <tr>
                                    <th colspan="3">Player</th>
                                    <th>First Seen</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach ($players as $player)
                                    <tr>
                                        <td width="50px">
                                        <span class="{{ $player->game->class_css }}">
                                            {{ $player->game->Name }}
                                        </span>
                                        </td>
                                        <td>
                                            <div class="avatar avatar-sm">
                                                <img src="{{ $player->battlelog->gravatar_img ?? app(\App\Helpers\Main::class)->gravatar() }}">
                                            </div>
                                            <a href="{{ $player->profile_url }}">
                                                @empty(!$player->ClanTag)
                                                    <span class="badge badge-glow badge">
                                                    {{ $player->ClanTag }}
                                                </span>
                                                @endempty
                                                {{ $player->SoldierName }}
                                            </a>

                                            @if ($player->ban !== null && $player->ban->is_active)
                                                <span class="badge-pill badge badge-danger" data-toggle="tooltip"
                                                      title="{{ $player->ban->record->record_message }}">
                                                    Banned
                                                </span>
                                            @endif

                                            @if(request()->has('playerName'))
                                                @php
                                                    $p = $player->aliases->filter(static function ($v) {
                                                        if (in_array($v['record_message'], array_map('trim', explode(',', request()->get('playerName'))), true)) {
                                                            return $v;
                                                        }
                                                    })->first();
                                                @endphp

                                                @unless(!$p)
                                                    <span class="badge-pill badge badge-danger">{{ $p->record_message }}</span>
                                                @endunless
                                            @endif
                                        </td>
                                        @if ($player->hasReputation())
                                            <td class="{{ $player->reputation->color }}">
                                                {{ $player->reputation->total_rep_co }}
                                            </td>
                                        @else
                                            <td>No Reputation Set Yet</td>
                                        @endif
                                        <td>
                                        <span v-text="moment('{{ $player->created_at->toIso8601String() }}').fromNow()"
                                              v-bind:title="moment('{{ $player->created_at->toIso8601String() }}').format('llll')"></span>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer align-self-center">
                        {!! $players->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection