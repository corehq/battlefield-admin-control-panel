@extends('layouts/contentLayoutMaster')

@section('title', 'Server List')

@section('vendor-style')
    <!-- vednor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
@endsection

@section('vendor-script')
    <!-- vednor files -->
    <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
@endsection

@section('mystyle')
    <!-- Page css files -->
@endsection

@section('myscript')
    <!-- Page js files -->
@endsection

@section('content')
    <section>
        @foreach($servers as $server)
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header mb-1">
                            <h4 class="card-title">
                            <span class="badge badge-pill {{ $server->game->class_css }}">
                                {{ $server->game->Name }}
                            </span>
                                {{ $server->ServerName }}
                            </h4>
                        </div>
                        <div class="card-content">
                            <img class="card-img img-fluid" src="{{ asset($server->map_image_paths['wide']) }}"
                                 alt="{{ $server->current_map }}">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <p class="card-text">
                                            <span class="badge badge-pill rounded-pill badge-primary">Map</span>
                                            {{ $server->current_map }}
                                        </p>
                                        <p class="card-text">
                                            <span class="badge badge-pill rounded-pill badge-primary">Gamemode</span>
                                            {{ $server->current_gamemode }}
                                        </p>
                                        <p class="card-text">
                                            <span class="badge badge-pill rounded-pill badge-primary">Players</span>
                                            {{ $server->usedSlots }} / {{ $server->maxSlots }}
                                            @unless($server->in_queue === null || $server->in_queue == 0)
                                                <span class="badge badge-sm badge-warning">
                                                {{ $server->in_queue }}
                                            </span>
                                            @endunless
                                        </p>
                                    </div>
                                    <div class="col-6">
                                        <div id="serverGraph{{ $server->ServerID }}"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </section>
@endsection