<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/7/20, 9:51 AM
 */

return [
    'profile' => [
        'title' => '[:game] :name Profile',
        'overview' => [
            'title' => 'Overview',
            'clan' => 'Clan',
            'reputation' => [
                'title' => 'Reputation',
                'tooltip' => 'Click on the information icon to learn more about the player reputation system',
            ],
            'guid' => [
                'pb' => 'PB GUID',
                'ea' => 'EA GUID',
            ],
            'ip' => 'IP Address',
            'country' => 'Country',
            'stats' => [
                'first_seen' => 'First Seen',
                'last_seen' => 'Last Seen',
                'playtime' => 'Total Playtime',
            ],
            'adkats' => [
                'groups' => '[0,1] AdKats Group|[2,*] AdKats Groups (:count)',
            ],
            'aliases' => 'Aliases',
            'rank' => 'Rank',
        ],
    ],
];