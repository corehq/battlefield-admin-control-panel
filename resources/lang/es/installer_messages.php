<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/22/19, 4:18 AM
 */

return [

    /**
     *
     * Traducciones compartidas.
     *
     */
    'title' => 'Instalador de Laravel',
    'next' => 'Siguiente',
    'finish' => 'Instalar',


    /**
     *
     * Traducciones de la página principal.
     *
     */
    'welcome' => [
        'title' => 'Bienvenido al instalador',
        'message' => 'Bienvenido al asistente de configuración',
    ],


    /**
     *
     * Tranducciones de la página de requisitos.
     *
     */
    'requirements' => [
        'title' => 'Requisitos',
    ],


    /**
     *
     * Traducciones de la pagina de permisos.
     *
     */
    'permissions' => [
        'title' => 'Permisos',
    ],


    /**
     *
     * Traducciones de la página de entorno.
     *
     */
    'environment' => [
        'title' => 'Configuraciones del entorno',
        'save' => 'Guardar archivo .env',
        'success' => 'Los cambios en tu archivo .env han sido guardados.',
        'errors' => 'No es posible crear el archivo .env, por favor intentalo manualmente.',
    ],


    /**
     *
     * Traducciones de la página final.
     *
     */
    'final' => [
        'title' => 'Finalizado.',
        'finished' => 'La aplicación ha sido instalada con éxito!',
        'exit' => 'Haz click aquí para salir.',
    ],
];
