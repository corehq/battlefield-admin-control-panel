<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/22/19, 4:18 AM
 */

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'Instalador Laravel',
    'next' => 'Próximo Passo',
    'finish' => 'Instalar',


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title' => 'Bem-vindo ao Instalador',
        'message' => 'Bem-vindo ao assistente de configuração.',
    ],


    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'Requisitos',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'Permissões',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => 'Configurações de Ambiente',
        'save' => 'Salvar .env',
        'success' => 'Suas configurações de arquivo .env foram salvas.',
        'errors' => 'Não foi possível salvar o arquivo .env, por favor crie-o manualmente.',
    ],


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => 'Terminado',
        'finished' => 'Aplicação foi instalada com sucesso',
        'exit' => 'Clique aqui para sair',
    ],
];
