<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/22/19, 4:18 AM
 */

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'Установка Laravel',
    'next' => 'Следующий шаг',


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title' => 'Установка Laravel',
        'message' => 'Добро пожаловать в первоначальную настройку фреймворка Laravel.',
    ],


    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'Необходимые модули',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'Проверка прав на папках',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => 'Настройки окружения',
        'save' => 'Сохранить .env',
        'success' => 'Настройки успешно сохранены в файле .env',
        'errors' => 'Произошла ошибка при сохранении файла .env, пожалуйста, сохраните его вручную',
    ],


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => 'Готово',
        'finished' => 'Приложение успешно настроено.',
        'exit' => 'Нажмите для выхода',
    ],
];
