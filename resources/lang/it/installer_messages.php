<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/22/19, 4:18 AM
 */

return [
    /**
     *
     * Shared translations.
     *
     */
    'title' => 'Laravel Installer',
    'next' => 'Passo successivo',
    'finish' => 'Installa',
    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title' => 'Benvenuto al programma di installazione',
        'message' => 'Benvenuto alla configurazione guidata.',
    ],
    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'Requisiti',
    ],
    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'Permessi',
    ],
    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => 'Configurazione ambiente',
        'save' => 'Salva .env',
        'success' => 'La configurazione del file .env &egrave; stata salvata correttamente.',
        'errors' => 'Impossibile salvare il file .env, per favore crealo manualmente.',
    ],
    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => 'Finito',
        'finished' => 'L\'applicazione è stata configurata correttamente.',
        'exit' => 'Clicca qui per uscire',
    ],
];
