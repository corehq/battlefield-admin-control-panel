<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/22/19, 4:18 AM
 */

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'Laravel安装程序',
    'next' => '下一步',
    'finish' => '安装',


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title' => '欢迎来到Laravel安装程序',
        'message' => '欢迎来到安装向导.',
    ],


    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => '环境要求',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => '权限',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => '环境设置',
        'save' => '保存 .env',
        'success' => '.env 文件保存成功.',
        'errors' => '无法保存 .env 文件, 请手动创建它.',
    ],


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => '完成',
        'finished' => '应用已成功安装.',
        'exit' => '点击退出',
    ],
];
