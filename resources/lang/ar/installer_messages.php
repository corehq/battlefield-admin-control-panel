<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/22/19, 4:18 AM
 */

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'تنصيب Laravel',
    'next' => 'متابعة',


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title' => 'تنصيب Laravel',
        'message' => 'أهلا بك في صفحة تنصيب Laravel',
    ],


    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'المتطلبات',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'تراخيص المجلدات',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => 'الإعدادات',
        'save' => 'حفظ ملف .env',
        'success' => 'تم حفظ الإعدادات بنجاح',
        'errors' => 'حدث خطأ أثناء إنشاء ملف .env. رجاءا قم بإنشاءه يدويا',
    ],


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => 'النهاية',
        'finished' => 'تم تنصيب البرنامج بنجاح...',
        'exit' => 'إضغط هنا للخروج',
    ],
];
