<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/22/19, 4:18 AM
 */

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'نصب کننده لاراول',
    'next' => 'قدم بعدی',


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title' => 'به نصب کننده خوش آمدید',
        'message' => 'به جادوگر نصب خوش آمدید .',
    ],


    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'نیازمندی ها',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'مجوز های دسترسی',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => 'تنظیمات پیکربندی',
        'save' => 'ذخیره کردن .env',
        'success' => 'فایل .env برای شما ذخیره شد.',
        'errors' => 'ذخیره کردن فایل .env امکان پذیر نیست، لطفا آن را به صورت دستی ایجاد کنید.',
    ],


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => 'تمام شد',
        'finished' => 'اپلیکیشن با موفقیت نصب شد.',
        'exit' => 'برای خروج اینجا را کلیک کنید',
    ],
];
