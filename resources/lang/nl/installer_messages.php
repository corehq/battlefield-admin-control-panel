<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/22/19, 4:18 AM
 */

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'Laravel Installer',
    'next' => 'Volgende stap',


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title' => 'Welkom bij het installatie proces...',
        'message' => 'Welkom bij de installatiewizard',
    ],


    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'Vereisten',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'Permissies',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => 'Environment Settings',
        'save' => '.env Opslaan',
        'success' => 'Uw .env bestand is opgeslagen.',
        'errors' => 'Het is niet mogelijk om een .env bestand aan te maken, maak a.u.b het bestand zelf aan.',
    ],


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => 'Voltooid',
        'finished' => 'Applicatie is succesvol geïnstalleerd.',
        'exit' => 'Klik hier om af te sluiten.',
    ],
];
