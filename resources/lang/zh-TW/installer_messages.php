<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/22/19, 4:18 AM
 */

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'Laravel安裝程序',
    'next' => '下一步',
    'finish' => '安裝',


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title' => '歡迎來到Laravel安裝程序',
        'message' => '歡迎來到安裝嚮導.',
    ],


    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => '環境要求',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => '權限',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => '環境設置',
        'save' => '保存 .env',
        'success' => '.env 文件保存成功.',
        'errors' => '無法保存 .env 文件, 請手動創建它.',
    ],


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => '完成',
        'finished' => '應用已成功安裝.',
        'exit' => '點擊退出',
    ],
];
