<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/22/19, 4:18 AM
 */

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'Laravel Instalator',
    'next' => 'Następny krok',


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title' => 'Instalacja Laravel',
        'message' => 'Witaj w kreatorze instalacji.',
    ],


    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'Wymagania systemowe ',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'Uprawnienia',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => 'Ustawnienia środowiska',
        'save' => 'Zapisz .env',
        'success' => 'Plik .env został poprawnie zainstalowany.',
        'errors' => 'Nie można zapisać pliku .env, Proszę utworzyć go ręcznie.',
    ],


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => 'Instalacja zakończona',
        'finished' => 'Aplikacja została poprawnie zainstalowana.',
        'exit' => 'Kliknij aby zakończyć',
    ],
];
