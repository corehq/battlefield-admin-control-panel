<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/22/19, 4:18 AM
 */

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'Εγκατάσταση Laravel',
    'next' => 'Επόμενο',


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title' => 'Καλωσήρθαστε στο Installer',
        'message' => 'Καλωσήρθατε στον οδηγό εγκατάστασης.',
    ],


    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'Απαιτήσεις συστήματος',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'Δικαιώματα',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => 'Ρυθμίσεις Περιβάλλοντος',
        'save' => 'Αποθήκευση .env αρχείου',
        'success' => 'Το αρχείο ρυθμίσεων .env έχει αποθηκευτεί με επιτυχία.',
        'errors' => 'Το αρχείο ρυθμίσεων .env ΔΕΝ μπόρεσε να αποθηκευτεί με επιτυχία. Παρακαλώ δημιουργίστε το χειροκίνητα.',
    ],


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => 'Ολοκληρώθηκε',
        'finished' => 'Το πρόγραμμά σας εγκαταστάθηκε με επιτυχία.',
        'exit' => 'Πατήστε εδώ για έξοδο.',
    ],
];
