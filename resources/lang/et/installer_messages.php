<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/22/19, 4:18 AM
 */

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'Laraveli installer',
    'next' => 'Järgmine samm',


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title' => 'Tere tulemast Laraveli installerisse',
        'message' => 'Tere tulemast installatsiooniviisardisse.',
    ],


    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'Nõuded',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'Õigused',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => 'Keskkonna seaded',
        'save' => 'Salvesta .env',
        'success' => 'Sinu .env faili seaded on salvestatud.',
        'errors' => 'Ei saanud .env faili salvesta, palun loo see manuaalselt.',
    ],


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => 'Lõpetatud',
        'finished' => 'Laravel on edukalt installitud',
        'exit' => 'Väljumiseks vajuta siia',
    ],
];
