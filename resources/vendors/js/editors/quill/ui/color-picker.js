/*
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/31/19, 8:57 PM
 */

import Picker from './picker';


class ColorPicker extends Picker {
    constructor(select, label) {
        super(select);
        this.label.innerHTML = label;
        this.container.classList.add('ql-color-picker');
        [].slice.call(this.container.querySelectorAll('.ql-picker-item'), 0, 7).forEach(function (item) {
            item.classList.add('ql-primary');
        });
    }

    buildItem(option) {
        let item = super.buildItem(option);
        item.style.backgroundColor = option.getAttribute('value') || '';
        return item;
    }

    selectItem(item, trigger) {
        super.selectItem(item, trigger);
        let colorLabel = this.label.querySelector('.ql-color-label');
        let value = item ? item.getAttribute('data-value') || '' : '';
        if (colorLabel) {
            if (colorLabel.tagName === 'line') {
                colorLabel.style.stroke = value;
            } else {
                colorLabel.style.fill = value;
            }
        }
    }
}


export default ColorPicker;
