<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/10/20, 12:58 AM
 */

return [
    /**
     * Should HTTPS be enforced when browsing? Set to "true" to enforce.
     *
     * Default: false
     */
    'force_https' => env('BFACP_FORCE_HTTPS', false),

    /**
     * Enter your community name. Leave blank to disable
     *
     * Uncomment to enable
     */
    'community_name' => env('BFACP_COMMUNITY_NAME', 'Battlefield Admin Control Panel'),

    /**
     * Enter your community abbreviation, should be less than 5 chars. Leave blank to disable
     *
     * Uncomment to enable
     */
    'community_slug' => env('BFACP_COMMUNITY_SLUG', 'BFACP'),

    /**
     * Specify the URL to your community logo. If nothing preset it will use the default BFACP logo.
     *
     * Uncomment to enable
     */
    'community_logo' => env('BFACP_COMMUNITY_LOGO', '/images/backgrounds/bfacp_logo.png'),
];