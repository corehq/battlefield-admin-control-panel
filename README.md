|                 	| |
|-------------------------	|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|
| Pipeline                	| [![pipeline status](https://gitlab.com/Prophet731/battlefield-admin-control-panel/badges/v3.0/pipeline.svg)](https://gitlab.com/Prophet731/battlefield-admin-control-panel/commits/v3.0) 	|
| Coverage                	| [![coverage report](https://gitlab.com/Prophet731/battlefield-admin-control-panel/badges/v3.0/coverage.svg)](https://gitlab.com/Prophet731/battlefield-admin-control-panel/commits/v3.0) 	|
| Latest Stable Version   	| [![Latest Stable Version](https://poser.pugx.org/adkats/bfacp/v/stable)](https://packagist.org/packages/adkats/bfacp)                                                                    	|
| Latest Unstable Version 	| [![Latest Unstable Version](https://poser.pugx.org/adkats/bfacp/v/unstable)](https://packagist.org/packages/adkats/bfacp)                                                                	|
| License                 	| [![License](https://poser.pugx.org/adkats/bfacp/license)](https://packagist.org/packages/adkats/bfacp)                                                                                 	|
| Monthly Downloads       	| [![Monthly Downloads](https://poser.pugx.org/adkats/bfacp/d/monthly)](https://packagist.org/packages/adkats/bfacp)                                                                       	|
| Composer Lock           	| [![composer.lock](https://poser.pugx.org/adkats/bfacp/composerlock)](https://packagist.org/packages/adkats/bfacp)                                                                        	|
| Dependencies              | [![Depfu](https://badges.depfu.com/badges/bf97a835092b4548e0e4db94f4fc4fb0/count.svg)](https://depfu.com/gitlab/Prophet731/battlefield-admin-control-panel?project_id=10110)              |

# Battlefield Admin Control Panel

The Battlefield Admin Control Panel (BFACP) is a web based application
designed to interact with the plugin
[AdKats](https://myrcon.net/topic/153-free-advanced-in-game-admin-and-ban-enforcer-adkats-7500/)
for [ProCon](https://myrcon.net).

It's purpose is it make administrating your server with the AdKats
plugin easier.

This project was started back in 2013 at the end of life of Battlefield
3 and didn't fully become what it until Battlefield 4. What started as a
tool for just internal use at my former gaming community (ADK Gamers)
turned into something that could benefit many gaming communities that
used the AdKats plugin.

### 2FA Support 

The Battlefield Admin Control Panel (BFACP) comes with 2FA features. Users can enable and disable 2FA for the account.
Users have to use Authenticator app to use this 2FA feature. Users can go to their profile page to enable and disable 2FA.

#### Enable 2FA

To enable 2FA for your account please follow the below steps.
Markup : - After you login to your account, go to profile page and click on enable 2FA button
         - You may have to scan the QR code using Authenticator app (ex.Google Authenticator App)
            - If you are unable to scan the QR code, you can copy the code and update it on the Authenticator App
         - Once you are done with scanning, Authenticator app shows you 6 digit code, enter the code and click on Authenticate button
         - If you enter the correct code, 2FA will be enabled for your account else you will have to retry with the correct code.
 #### Disable 2FA
 To disable 2FA for your account you have to enter your current password and then click on disable 2FA button. If you enter the correct password, 2FA will be disabled for your account
 
 #### Validate OTP
 If your account has 2FA enabled, you will be redirected to otp page soon after your login. You need to open Authenticator App, copy the code and then paste it on the otp page.
 If you enter the correct otp or code, you will be redirected to dashboard else you will be redirected back to otp page. 
