<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/21/20, 11:04 AM
 */

namespace App\Libraries;

use App\Exceptions\RCONException;
use App\Models\Battlefield\Server\Server;

/**
 * Class BFRCON
 *
 * @package App\Libraries
 */
class BFRCON
{
    /**
     * @var mixed|null
     */
    private $_serverIP;

    /**
     * @var mixed|null
     */
    private $_serverRconQueryPort;

    /**
     * @var int
     */
    private $_clientSequenceNr = 0;

    /**
     * @var null
     */
    private $_sock;

    /**
     * @var bool|resource|null
     */
    private $_connection = false;

    /**
     * @var null
     */
    private $_playerdata; // used for caching

    /**
     * @var null
     */
    private $_playerdata_admin; // used for caching

    /**
     * @var null
     */
    private $_serverdata; // used for caching

    /**
     * @var bool
     */
    private $_isLoggedIn = false;

    /**
     * @var null
     */
    private $_sockType;

    /*-- configuration vars --*/

    /**
     * @var array
     */
    private $_globalMsg = [
        'PLAYER_NOT_FOUND' => 'PlayerNotFoundError',
        'TEAM_NOT_FOUND' => 'TeamNameNotFoundError',
        'SQUAD_NOT_FOUND' => 'SquadNameNotFoundError',
        'PLAYMODE_NOT_FOUND' => 'PlaymodeNameNotFoundError',
        'MAPNAME_NOT_FOUND' => 'MapNameNotFoundError',
        'ADMIN_YELL_DURATION_MAX' => 60,
        'NOT_LOGGED_IN' => 'NotLoggedInAsAdmin',
        'LOGIN_FAILED' => 'LoginFailed',
    ];

    /**
     * @var array
     */
    private $_globalVars;

    /*-- constructor and destructor --*/

    /**
     * @param  Server  $server
     * @param  string  $debug  for debugging, use "-d"
     */
    public function __construct(Server $server, $debug = '-d')
    {
        $this->_globalVars = [
            'mapsFileXML' => $server->maps_file_path,
            'playmodesFileXML' => $server->modes_file_path,
            'squadnamesFileXML' => $server->squads_file_path,
            'teamnamesFileXML' => $server->teams_file_path,
            'defaultServerResponse' => 'OK',
            'cachingEnabled' => true, // change this to false if you want caching disabled
        ];

        if ($this->_serverIP === null) {
            $this->_serverIP = $server->ip;
            $this->_serverRconQueryPort = $server->port;
            $this->_connection = $this->_openConnection($debug);
        }
    }

    /**
     * @param  null  $debug
     *
     * @return bool|null|resource
     */
    private function _openConnection($debug = null)
    {
        $connection = false;

        if (function_exists('socket_create') && function_exists('socket_connect') && function_exists('socket_strerror') && function_exists('socket_last_error') && function_exists('socket_set_block') && function_exists('socket_read') && function_exists('socket_write') && function_exists('socket_close')) {
            $this->_sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

            if (function_exists('socket_set_option')) {
                socket_set_option($this->_sock, SOL_SOCKET, SO_RCVTIMEO, ['sec' => 5, 'usec' => 0]);
                socket_set_option($this->_sock, SOL_SOCKET, SO_SNDTIMEO, ['sec' => 5, 'usec' => 0]);
            }

            @$connection = socket_connect($this->_sock, $this->_serverIP, $this->_serverRconQueryPort);
            if ($debug === '-d') {
                echo '[DEBUG]: '.socket_strerror(socket_last_error()).".\n";
            }
            if ($connection) {
                socket_set_block($this->_sock);
            }

            $this->_sockType = 1;
        } elseif (function_exists('fsockopen')) {
            if ($debug === '-d') {
                @$this->_sock = fsockopen('tcp://'.$this->_serverIP, $this->_serverRconQueryPort, $errno, $errstr,
                    10);
                if (!$this->_sock) {
                    echo '[DEBUG]: '.$errno.' - '.$errstr."\n";
                }
            } else {
                @$this->_sock = fsockopen('tcp://'.$this->_serverIP, $this->_serverRconQueryPort);
            }

            $connection = $this->_sock;

            $this->_sockType = 2;
        }

        return $connection;
    }

    /*-- required methods for communicating with the gameserver --*/

    /**
     *
     */
    public function __destruct()
    {
        if ($this->_connection) {
            $this->_closeConnection();
            $this->_connection = false;
        }
    }

    /**
     *
     */
    private function _closeConnection(): void
    {
        //      $this->_clientRequest("quit");

        if ($this->_sockType == 1) {
            socket_close($this->_sock);
        } else {
            fclose($this->_sock);
        }

        $this->_sockType = null;
    }

    /**
     * plain text login to gameserver<br />
     * [ RCON password MUST NOT contain whitespaces!! ].
     *
     * @param  string
     *
     * @return string
     * @throws RCONException
     */
    public function loginInsecure($rconPassword): string
    {
        $loginStatus = $this->_array2String($this->_clientRequest('login.plainText '.$rconPassword), 0);

        if ($loginStatus == $this->_globalVars['defaultServerResponse']) {
            $this->_isLoggedIn = true;

            return $loginStatus;
        }

        return $this->_globalMsg['LOGIN_FAILED'];
    }

    /**
     * @param       $array
     * @param  int  $key
     *
     * @return mixed
     */
    private function _array2String($array, $key = 1)
    {
        return $array[$key];
    }

    /**
     * @param $clientRequest
     *
     * @return mixed
     * @throws RCONException
     */
    private function _clientRequest($clientRequest)
    {
        $data = $this->_encodeClientRequest($clientRequest);

        if ($this->_sockType == 1) {
            socket_write($this->_sock, $data, strlen($data));
        } else {
            fwrite($this->_sock, $data, strlen($data));
        }

        $receiveBuffer = '';
        [$packet, $receiveBuffer] = $this->_receivePacket($receiveBuffer);
        [$isFromServer, $isResponse, $sequence, $requestAnswer] = $this->_decodePacket($packet);

        return $requestAnswer;
    }

    /**
     * @param $data
     *
     * @return string
     */
    private function _encodeClientRequest($data): string
    {
        $packet = $this->_encodePacket(false, false, $this->_clientSequenceNr, $data);
        $this->_clientSequenceNr = ($this->_clientSequenceNr + 1) & 0x3fffffff;

        return $packet;
    }

    /**
     * @param $isFromServer
     * @param $isResponse
     * @param $sequence
     * @param $data
     *
     * @return string
     */
    private function _encodePacket($isFromServer, $isResponse, $sequence, $data): string
    {
        $data = explode(' ', $data);
        if ($data[0] === 'admin.yell' && isset($data[1])) {
            $adminYell = [$data[0], '', '', ''];

            $yellStyle = '';
            $yellKey = 0;
            foreach ($data as $key => $content) {
                if ($key != 0) {
                    if ($content == '{%player%}') {
                        $yellStyle = 'player';
                        $yellKey = $key;
                        break;
                    }

                    if ($content == '{%team%}') {
                        $yellStyle = 'team';
                        $yellKey = $key;
                        break;
                    }

                    if ($content == '{%all%}') {
                        $yellStyle = 'all';
                        $yellKey = $key;
                        break;
                    }
                }
            }

            if ($yellStyle === 'all') {
                foreach ($data as $key => $content) {
                    if ($key != 0 && $key < $yellKey - 1) {
                        $adminYell[1] .= $content.' ';
                    } elseif ($key == $yellKey) {
                        $adminYell[3] = $yellStyle;
                    } elseif ($key == $yellKey - 1) {
                        $adminYell[2] = $data[$yellKey - 1];
                    }
                }

                $adminYell[1] = trim($adminYell[1]);
            } elseif ($yellStyle === 'player' || $yellStyle === 'team') {
                $adminYell[4] = '';

                foreach ($data as $key => $content) {
                    if ($key != 0 && $key < $yellKey - 1) {
                        $adminYell[1] .= $content.' ';
                    } elseif ($key == $yellKey) {
                        $adminYell[3] = $yellStyle;
                    } elseif ($key == $yellKey - 1) {
                        $adminYell[2] = $data[$yellKey - 1];
                    } elseif ($key > $yellKey) {
                        $adminYell[4] .= $content.' ';
                    }
                }

                $adminYell[4] = trim($adminYell[4]); // trim whitespaces
            }

            $data = $adminYell;
        } elseif ($data[0] === 'vars.serverDescription' && isset($data[1])) {
            $serverDesc = [$data[0], ''];
            foreach ($data as $key => $value) {
                if ($key != 0) {
                    $serverDesc[1] .= $value.' ';
                }
            }
            $serverDesc[1] = trim($serverDesc[1]);

            $data = $serverDesc;
        } elseif ($data[0] === 'admin.kickPlayer' && isset($data[1])) {
            $reason = false;
            foreach ($data as $key => $value) {
                if ($value === '{%reason%}') {
                    $reason = true;
                }
            }

            if (!$reason) {
                $kickPlayer = [$data[0], ''];
                foreach ($data as $key => $value) {
                    if ($key != 0) {
                        $kickPlayer[1] .= $value.' ';
                    }
                }
                $kickPlayer[1] = trim($kickPlayer[1]);
            } else {
                $kickPlayer = [$data[0], '', ''];
                $i = 0;
                foreach ($data as $key => $value) {
                    if ($key != 0) {
                        if ($value === '{%reason%}') {
                            $i = $key;
                        }

                        if ($i == 0) {
                            $kickPlayer[1] .= $value.' ';
                        } elseif ($key != $i) {
                            $kickPlayer[2] .= $value.' ';
                        }
                    }
                }
                $kickPlayer[1] = trim($kickPlayer[1]); // trim whitespaces
                $kickPlayer[2] = trim($kickPlayer[2]); // trim whitespaces
            }

            $data = $kickPlayer;
        } elseif ($data[0] === 'banList.add' || ($data[0] == 'banList.remove' && isset($data[1]))) {
            $dataCount = count($data) - 1;
            $banPlayer = [$data[0], $data[1], ''];
            foreach ($data as $key => $value) {
                if ($key != 0 && $key != 1) {
                    if ($data[0] === 'banList.add' && $key != $dataCount) {
                        $banPlayer[2] .= $value.' ';
                    } elseif ($data[0] === 'banList.remove') {
                        $banPlayer[2] .= $value.' ';
                    }
                }
            }

            $banPlayer[2] = trim($banPlayer[2]); // trim whitespace

            if ($data[0] === 'banList.add') {
                $banPlayer[3] = $data[$dataCount];
            }

            $data = $banPlayer;
        } elseif ($data[0] === 'admin.listPlayers' || ($data[0] == 'listPlayers' && isset($data[1]))) {
            $listPlayer = [$data[0]];
            $listPlayer[1] = $data[1];
            if ($data[1] !== 'all') {
                if ($data[1] === 'player') {
                    $listPlayer[2] = '';
                    foreach ($data as $key => $value) {
                        if ($key != 0 && $key != 1) {
                            $listPlayer[2] .= $value.' ';
                        }
                    }

                    $listPlayer[2] = trim($listPlayer[2]); // trim ending whitespace
                }
                if ($data[1] === 'team') {
                    $listPlayer[2] = '';
                    foreach ($data as $key => $value) {
                        if ($key != 0 && $key != 1) {
                            $listPlayer[2] .= $value.' ';
                        }
                    }

                    $listPlayer[2] = trim($listPlayer[2]); // trim ending whitespace
                }
            } else {
                $listPlayer[1] = $data[1];
            }

            $data = $listPlayer;
        } elseif ($data[0] === 'reservedSlots.addPlayer' || ($data[0] == 'reservedSlots.removePlayer' && isset($data[1]))) {
            $reservedSlots = [$data[0], ''];
            foreach ($data as $key => $value) {
                if ($key != 0) {
                    $reservedSlots[1] .= $value.' ';
                }
            }

            $reservedSlots[1] = trim($reservedSlots[1]); // trim whitespace

            $data = $reservedSlots;
        } elseif ($data[0] === 'admin.say' && isset($data[1])) {
            $adminSay = [$data[0], '', '', ''];
            $i = 0;
            foreach ($data as $key => $value) {
                if ($key != 0) {
                    if ($value === '{%player%}' || $value === '{%team%}' || $value === '{%all%}') {
                        $i = $key;
                        $adminSay[2] = preg_replace('/[{}%]/', '', $value);
                    }
                    if ($i == 0) {
                        $adminSay[1] .= $value.' ';
                    } elseif ($key != $i && $adminSay[2] !== 'all') {
                        $adminSay[3] .= $value.' ';
                    }
                }
            }

            $adminSay[1] = trim($adminSay[1]); // trim whitespace
            $adminSay[3] = trim($adminSay[3]); // trim whitespace

            if ($adminSay[2] === 'all') {
                unset($adminSay[3]);
            }

            $data = $adminSay;
        } elseif ($data[0] === 'admin.killPlayer' && isset($data[1])) {
            $adminKillPlayer = [$data[0], ''];
            $i = 0;
            foreach ($data as $key => $value) {
                if ($key != 0) {
                    $adminKillPlayer[1] .= $value.' ';
                }
            }

            $adminKillPlayer[1] = trim($adminKillPlayer[1]); // trim whitespace

            $data = $adminKillPlayer;
        } elseif ($data[0] === 'admin.movePlayer' && isset($data[1])) {
            $dataCount = count($data) - 3;
            $adminMovePlayer = [$data[0], '', '', '', ''];
            $i = 0;
            foreach ($data as $key => $value) {
                if ($key != 0 && $key < $dataCount) {
                    $adminMovePlayer[1] .= $value.' ';
                }
            }

            $adminMovePlayer[1] = trim($adminMovePlayer[1]); // trim whitespace
            $adminMovePlayer[2] = $data[$dataCount];
            $adminMovePlayer[3] = $data[$dataCount + 1];
            $adminMovePlayer[4] = $data[$dataCount + 2];

            $data = $adminMovePlayer;
        }

        $encodedHeader = $this->_encodeHeader($isFromServer, $isResponse, $sequence);
        $encodedNumWords = $this->_encodeInt32(count($data));
        [$wordsSize, $encodedWords] = $this->_encodeWords($data);
        $encodedSize = $this->_encodeInt32($wordsSize + 12);

        return $encodedHeader.$encodedSize.$encodedNumWords.$encodedWords;
    }

    /**
     * @param $isFromServer
     * @param $isResponse
     * @param $sequence
     *
     * @return string
     */
    private function _encodeHeader($isFromServer, $isResponse, $sequence): string
    {
        $header = $sequence & 0x3fffffff;
        if ($isFromServer) {
            $header += 0x80000000;
        }
        if ($isResponse) {
            $header += 0x40000000;
        }

        return pack('I', $header);
    }

    /**
     * @param $size
     *
     * @return string
     */
    private function _encodeInt32($size): string
    {
        return pack('I', $size);
    }

    /**
     * @param $words
     *
     * @return array
     */
    private function _encodeWords($words): array
    {
        $size = 0;
        $encodedWords = '';
        foreach ($words as $word) {
            $strWord = $word;
            $encodedWords .= $this->_encodeInt32(strlen($strWord));
            $encodedWords .= $strWord;
            $encodedWords .= "\x00";
            $size += strlen($strWord) + 5;
        }

        return [
            $size,
            $encodedWords,
        ];
    }

    /**
     * @param $receiveBuffer
     *
     * @return array
     * @throws RCONException
     */
    private function _receivePacket($receiveBuffer): array
    {
        $count = 0;
        while (!$this->_containsCompletePacket($receiveBuffer)) {
            if ($this->_sockType == 1) {
                $socketbuffer = @socket_read($this->_sock, 4096);
                if ($socketbuffer == false) {
                    throw new RCONException('Could not read packet data from game server.');
                }
                $receiveBuffer .= $socketbuffer;
            } else {
                $socketbuffer = fread($this->_sock, 4096);
                if ($socketbuffer == false) {
                    throw new RCONException('Could not read packet data from game server.');
                }
                $receiveBuffer .= $socketbuffer;
            }
        }

        $packetSize = $this->_decodeInt32(substr($receiveBuffer, 4, 4));

        $packet = substr($receiveBuffer, 0, $packetSize);
        $receiveBuffer = substr($receiveBuffer, $packetSize, strlen($receiveBuffer));

        return [
            $packet,
            $receiveBuffer,
        ];
    }

    /**
     * @param $data
     *
     * @return bool
     */
    private function _containsCompletePacket($data): bool
    {
        if (strlen($data) < 8) {
            return false;
        }

        if (strlen($data) < $this->_decodeInt32(substr($data, 4, 4))) {
            return false;
        }

        return true;
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    private function _decodeInt32($data)
    {
        $decode = unpack('I', $data);

        return $decode[1];
    }

    /**
     * @param $data
     *
     * @return array
     */
    private function _decodePacket($data): array
    {
        [$isFromServer, $isResponse, $sequence] = $this->_decodeHeader($data);
        $wordsSize = $this->_decodeInt32(substr($data, 4, 4)) - 12;
        $words = $this->_decodeWords($wordsSize, substr($data, 12));

        return [
            $isFromServer,
            $isResponse,
            $sequence,
            $words,
        ];
    }

    /**
     * @param $data
     *
     * @return array
     */
    private function _decodeHeader($data): array
    {
        $header = unpack('I', $data);

        return [
            $header & 0x80000000,
            $header & 0x40000000,
            $header & 0x3fffffff,
        ];
    }

    /*-- internal methods --*/

    /**
     * @param $size
     * @param $data
     *
     * @return array
     */
    private function _decodeWords($size, $data): array
    {
        $numWords = $this->_decodeInt32($data);
        $offset = 0;
        $words = [];
        while ($offset < $size) {
            $wordLen = $this->_decodeInt32(substr($data, $offset, 4));
            $word = substr($data, $offset + 4, $wordLen);
            $words[] = $word;
            $offset += $wordLen + 5;
        }

        return $words;
    }

    /**
     * salted hash login to gameserver<br />
     * [ RCON password MUST NOT contain whitespaces!! ].
     *
     * @param  string
     *
     * @return string
     * @throws RCONException
     */
    public function loginSecure($rconPassword): ?string
    {
        $salt = $this->_array2String($this->_clientRequest('login.hashed'));

        $hashedPW = $this->_hex_str($salt).$rconPassword;
        $saltedHashedPW = strtoupper(md5($hashedPW));

        $loginStatus = $this->_array2String($this->_clientRequest('login.hashed '.$saltedHashedPW), 0);

        if ($loginStatus == $this->_globalVars['defaultServerResponse']) {
            $this->_isLoggedIn = true;

            return $loginStatus;
        }

        return $this->_globalMsg['LOGIN_FAILED'];
    }

    /**
     * @param $hex
     *
     * @return string
     */
    private function _hex_str($hex): string
    {
        $string = '';
        for ($i = 0; $i < strlen($hex) - 1; $i += 2) {
            $string .= chr(hexdec($hex[$i].$hex[$i + 1]));
        }

        return $string;
    }

    /**
     * logging out.
     *
     * @return string
     * @throws RCONException
     */
    public function logout(): string
    {
        $this->_isLoggedIn = false;

        return $this->_array2String($this->_clientRequest('logout'), 0);
    }

    /*-- login and logout --*/

    /**
     * disconnecting from the gameserver.
     *
     * @return string
     * @throws RCONException
     */
    public function quit(): string
    {
        return $this->_array2String($this->_clientRequest('quit'), 0);
    }

    /**
     * if logged in with rcon && successful, return true, otherwise false.
     *
     * @return bool
     */
    public function isLoggedIn(): bool
    {
        return $this->_isLoggedIn;
    }

    /**
     * returns the name of the given squad<br /><br />
     * example: getSquadName(1) - will return "Bravo Squad".
     *
     * @param  int
     *
     * @return string
     */
    public function getSquadName($squadID): string
    {
        $squadNamesXML = simplexml_load_string(file_get_contents($this->_globalVars['squadnamesFileXML']));
        $squadName = $this->_globalMsg['SQUAD_NOT_FOUND'];

        for ($i = 0; $i <= (count($squadNamesXML->squad) - 1); $i++) {
            if ($squadID == $squadNamesXML->squad[$i]->attributes()->id) {
                $squadName = $squadNamesXML->squad[$i]->attributes()->name;
            }
        }

        return $squadName;
    }

    /**
     * gets the teamname of a given map, playmode and teamid (DIFFERENT IN SQDM!).
     *
     * @param $playmodeURI
     * @param $teamID
     *
     * @return string
     */
    public function getTeamName($playmodeURI, $teamID): string
    {
        $teamNameXML = simplexml_load_string(file_get_contents($this->_globalVars['teamnamesFileXML']));
        $teamName = $this->_globalMsg['TEAM_NOT_FOUND'];

        $playModes = count($teamNameXML->teamName[0]) - 1;
        if ($playModes >= $teamID) {
            for ($i = 0; $i <= $playModes; $i++) {
                if ($teamNameXML->teamName[0]->playMode[$i]->attributes()->uri == $playmodeURI) {
                    $teamName = $teamNameXML->teamName[0]->playMode[$i]->team[$teamID]->name;
                }
            }
        }

        return $teamName;
    }

    /**
     * returns the server ip as a string.
     *
     * @return string
     */
    public function getServerIP(): string
    {
        return $this->_serverIP;
    }

    /*-- replacements --*/

    /**
     * returns the server name as a string.
     *
     * @return string
     * @throws RCONException
     */
    public function getServerName(): string
    {
        $serverInfo = $this->getServerInfo();

        return $this->_array2String($serverInfo);
    }

    /**
     * returns the server information as an array.
     *
     * @return array
     * @throws RCONException
     */
    public function getServerInfo(): array
    {
        if ($this->_serverdata == null || !$this->_globalVars['cachingEnabled']) {
            $this->_serverdata = $this->_clientRequest('serverInfo');
        }

        return $this->_serverdata;
    }

    /**
     * returns the max amount of players allowed on server as an integer.
     *
     * @return int
     * @throws RCONException
     */
    public function getMaxPlayers(): int
    {
        $serverInfo = $this->getServerInfo();

        return (int) $this->_array2String($serverInfo, 3);
    }

    /**
     * returns the current playmode (human readable) as a string.
     *
     * @return string
     * @throws RCONException
     */
    public function getCurrentPlaymodeName(): string
    {
        $serverInfo = $this->getServerInfo();

        return $this->getPlaymodeName($this->getCurrentPlaymode());
    }

    /*-- server information --*/

    /**
     * returns the name of the given playmode<br /><br />
     * example: getPlaymodeName("RUSH").
     *
     * @param  string
     *
     * @return string name of the given playmode
     */
    public function getPlaymodeName($playmodeURI): string
    {
        $playModesXML = simplexml_load_string(file_get_contents($this->_globalVars['playmodesFileXML']));
        $playmodeName = $this->_globalMsg['PLAYMODE_NOT_FOUND'];

        for ($i = 0; $i <= (count($playModesXML->playmode) - 1); $i++) {
            if ($playmodeURI == $playModesXML->playmode[$i]->attributes()->uri) {
                $playmodeName = $playModesXML->playmode[$i]->attributes()->name;
                break;
            }
        }

        return !is_string($playmodeName) ? head($playmodeName) : $playmodeName;
    }

    /**
     * returns the current playmode as a string.
     *
     * @return string
     * @throws RCONException
     */
    public function getCurrentPlaymode(): string
    {
        $serverInfo = $this->getServerInfo();

        return $this->_array2String($serverInfo, 4);
    }

    /**
     * returns the current map (human readable) as a string.
     *
     * @return string
     * @throws RCONException
     */
    public function getCurrentMapName(): string
    {
        $serverInfo = $this->getServerInfo();

        return $this->getMapName($this->getCurrentMap());
    }

    /**
     * returns the name of the given map<br /><br />
     * example: getMapName("Levels/MP_002").
     *
     * @param  string
     *
     * @return string name of the given map
     * @throws RCONException
     */
    public function getMapName($mapURI): string
    {
        $mapNamesXML = simplexml_load_string(file_get_contents($this->_globalVars['mapsFileXML']));
        $mapName = $this->_globalMsg['MAPNAME_NOT_FOUND'];
        $currentPlaymode = $this->getCurrentPlaymode();

        for ($i = 0; $i <= (count($mapNamesXML->map) - 1); $i++) {
            if (strcasecmp($mapURI,
                    $mapNamesXML->map[$i]->attributes()->uri) == 0 && $currentPlaymode == $mapNamesXML->map[$i]->attributes()->playmode
            ) {
                $mapName = $mapNamesXML->map[$i]->attributes()->name;
                break;
            }
        }

        return !is_string($mapName) ? head($mapName) : $mapName;
    }

    /**
     * returns the current map as a string.
     *
     * @return string
     * @throws RCONException
     */
    public function getCurrentMap(): string
    {
        $serverInfo = $this->getServerInfo();

        return $this->_array2String($serverInfo, 5);
    }

    /**
     * returns the build-id of the game.
     *
     * @return int
     * @throws RCONException
     */
    public function getVersionID(): int
    {
        return (int) $this->_array2String($this->getVersion(), 2);
    }

    /**
     * returns the server version as an array.
     *
     * @return array
     * @throws RCONException
     */
    public function getVersion(): array
    {
        return $this->_clientRequest('version');
    }

    /**
     * returns the game type currently running.
     *
     * @return string
     * @throws RCONException
     */
    public function getGameType(): string
    {
        return $this->_array2String($this->getVersion());
    }

    /**
     * returns the current round of the game.
     *
     * @return int
     * @throws RCONException
     */
    public function getCurrentGameRound(): int
    {
        return (int) $this->_array2String($this->getServerInfo(), 6) + 1;
    }

    /**
     * returns the max amount of rounds of the current game.
     *
     * @return int
     * @throws RCONException
     */
    public function getGameMaxRounds(): int
    {
        return (int) $this->_array2String($this->getServerInfo(), 7);
    }

    /**
     * returns the current teamscores
     * FFIXX doesn't work yet.
     *
     * @return int
     * @throws RCONException
     */
    public function getTeamScores(): int
    {
        return (int) $this->_array2String($this->getServerInfo(), 8);
    }

    /**
     * returns the current online state of the gameserver
     * FFIXX doesn't work with the current bf3 server release.
     *
     * @return string
     * @throws RCONException
     */
    public function getOnlineState(): string
    {
        return $this->_array2String($this->getServerInfo(), 11);
    }

    /**
     * returns list of all players on the server with the team specified, but with zeroed out GUIDs.
     *
     * @param  string  $team
     *
     * @return array
     * @throws RCONException
     */
    public function getPlayerlistTeam($team = ''): array
    {
        if (!isset($team) || $team == '') {
            $team = 'all';
        } else {
            $team = 'team '.$team;
        }

        return $this->_clientRequest('listPlayers '.$team);
    }

    /**
     * returns list of all playernames on server (useful for other functions).
     *
     * @return array
     * @throws RCONException
     */
    public function getPlayerlistNames(): array
    {
        $players = $this->getPlayerlist();
        $playersAmount = $this->getCurrentPlayers();

        $playersNames = [];

        $playersParameters = (int) $players[1];
        for ($i = 0; $i < $playersAmount; $i++) {
            $playersNames[] = $players[($playersParameters) * $i + $playersParameters + 3];
        }

        return $playersNames;
    }

    /**
     * returns list of all players on the server, but with zeroed out GUIDs.
     *
     * @return array
     * @throws RCONException
     */
    public function getPlayerlist(): array
    {
        if ($this->_playerdata == null || !$this->_globalVars['cachingEnabled']) {
            $this->_playerdata = $this->_clientRequest('listPlayers all');
        }

        return $this->_playerdata;
    }

    /**
     * returns the current players on server as an integer.
     *
     * @return int
     * @throws RCONException
     */
    public function getCurrentPlayers(): int
    {
        $serverInfo = $this->getServerInfo();

        return (int) $this->_array2String($serverInfo, 2);
    }

    /**
     * returns true if server is available, otherwise false.
     *
     * @return bool
     * @see isConnected()
     */
    public function isServerOnline(): bool
    {
        return $this->isConnected();
    }

    /**
     * returns true if connected to a gameserver, otherwise false.
     *
     * @return bool
     * @see isServerOnline()
     */
    public function isConnected(): bool
    {
        return $this->_connection;
    }

    /**
     * returns the gamepassword as a string.
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarGetGamepassword(): string
    {
        return $this->_array2String($this->_clientRequest('vars.gamePassword'));
    }

    /**
     * gets the full gamedata of all players on the gameserver.
     *
     * @return array
     * @throws RCONException
     */
    public function adminGetPlayerlist(): array
    {
        if ($this->_playerdata_admin == null || !$this->_globalVars['cachingEnabled']) {
            $this->_playerdata_admin = $this->_clientRequest('admin.listPlayers all');
        }

        return $this->_playerdata_admin;
    }

    /**
     * returns all commands available on the server - requires login.
     *
     * @return array
     * @throws RCONException
     */
    public function adminGetAllCommands(): array
    {
        return $this->_clientRequest('serverInfo');
    }

    /*-- admin server information --*/

    /**
     * returns true/false, if server events are enabled in this connection or
     * not.
     *
     * @return bool
     * @throws RCONException
     */
    public function adminEventsEnabledStatusGet(): bool
    {
        return $this->_array2boolean($this->_clientRequest('admin.eventsEnabled'));
    }

    /**
     * @param       $array
     * @param  int  $key
     *
     * @return bool
     */
    private function _array2boolean($array, $key = 1): bool
    {
        return isset($array[$key]) && $array[$key] === 'true';
    }

    /**
     * sets the server events on/off in this connection<br />
     * [ Useless, if you set them on and use this class only ;) ].
     *
     * @param  bool
     *
     * @return array
     * @throws RCONException
     */
    public function adminEventsEnabledStatusSet($boolean): array
    {
        return $this->_clientRequest('admin.eventsEnabled '.$this->_bool2String($boolean));
    }

    /**
     * @param $boolean
     *
     * @return string
     */
    private function _bool2String(bool $boolean): string
    {
        return $boolean ? 'true' : 'false';
    }

    /**
     * returns the clantag of a given playername.
     *
     * @param  string
     *
     * @return string
     * @throws RCONException
     */
    public function getPlayerClantag($playerName): string
    {
        $playerInfo = $this->getPlayerdata($playerName);
        if (!isset($playerInfo[12])) {
            return $this->_globalMsg['PLAYER_NOT_FOUND'];
        }

        return $this->_array2String($playerInfo, 12);
    }

    /**
     * TODO: check for caching if playername = all
     * returns gamedata of given playername with zeroed out GUID.
     *
     * @param  string (if not set, all players will be listed)
     *
     * @return array
     * @throws RCONException
     */
    public function getPlayerdata($playerName = ''): array
    {
        if (!isset($playerName) || $playerName == '') {
            $playerName = 'all';
        } else {
            $playerName = 'player '.$playerName;
        }

        return $this->_clientRequest('listPlayers '.$playerName);
    }

    /**
     * returns the playername of a given playername.
     *
     * @param  string
     *
     * @return string
     * @throws RCONException
     */
    public function getPlayername($playerName): string
    {
        $playerInfo = $this->getPlayerdata($playerName);
        if (!isset($playerInfo[13])) {
            return $this->_globalMsg['PLAYER_NOT_FOUND'];
        }

        return $this->_array2String($playerInfo, 13);
    }

    /**
     * returns the current kills of a given playername.
     *
     * @param  string
     *
     * @return int
     * @throws RCONException
     */
    public function getPlayerKills($playerName): int
    {
        $playerInfo = $this->getPlayerdata($playerName);
        if (!isset($playerInfo[17])) {
            return $this->_globalMsg['PLAYER_NOT_FOUND'];
        }

        return (int) $this->_array2String($playerInfo, 17);
    }

    /**
     * returns the current deaths of a given playername.
     *
     * @param  string
     *
     * @return int
     * @throws RCONException
     */
    public function getPlayerDeaths($playerName): int
    {
        $playerInfo = $this->getPlayerdata($playerName);
        if (!isset($playerInfo[18])) {
            return $this->_globalMsg['PLAYER_NOT_FOUND'];
        }

        return (int) $this->_array2String($playerInfo, 18);
    }

    /**
     * returns the score of a given playername.
     *
     * @param  string
     *
     * @return int
     * @throws RCONException
     */
    public function getPlayerScore($playerName): int
    {
        $playerInfo = $this->getPlayerdata($playerName);
        if (!isset($playerInfo[18])) {
            return $this->_globalMsg['PLAYER_NOT_FOUND'];
        }

        return (int) $this->_array2String($playerInfo, 18);
    }

    /**
     * returns the ping of a given playername.
     *
     * @param  string
     *
     * @return int
     * @throws RCONException
     */
    public function getPlayerPing($playerName): int
    {
        $playerInfo = $this->getPlayerdata($playerName);
        if (!isset($playerInfo[21])) {
            return $this->_globalMsg['PLAYER_NOT_FOUND'];
        }

        return (int) $this->_array2String($playerInfo, 21);
    }

    /**
     * sends an admin-yell message to a specified player (or all)<br />
     * example: adminYellMessage("Storm the front!", "JLNNN") - send the message to player "JLNNN"
     * TODO: Need fix for sending messages to squads
     * TODO: Cut strings with length more than 100 chars.
     *
     * @param  string
     * @param  string  (optional) - if not set, message will be sent to all
     *                players
     * @param  int     (optional) - amount of time the message will be displayed,
     *                must be 1-60
     *
     * @return string
     * @throws RCONException
     */
    public function adminYellMessage($text, $playerName = '{%all%}', $durationInMS = 10): string
    {
        if ($durationInMS > $this->_globalMsg['ADMIN_YELL_DURATION_MAX']) {
            $durationInMS = $this->_globalMsg['ADMIN_YELL_DURATION_MAX'];
        }

        if ($playerName !== '{%all%}') {
            $playerName = '{%player%}'.' '.$playerName;
        }

        return $this->_array2String($this->_clientRequest('admin.yell '.$text.' '.$durationInMS.' '.$playerName), 0);
    }

    /**
     * sends an admin-yell message to a specified team<br />
     * example: adminYellMessageToTeam("Storm the front!", 1) - send the message to teamID 1
     * TODO: Cut strings with length more than 100 chars.
     *
     * @param  string
     * @param  int
     * @param  int     (optional) - amount of time the message will be displayed,
     *                must be 1-60
     *
     * @return string
     * @throws RCONException
     */
    public function adminYellMessageToTeam($text, $teamID, $durationInMS = 10): string
    {
        if ($durationInMS > $this->_globalMsg['ADMIN_YELL_DURATION_MAX']) {
            $durationInMS = $this->_globalMsg['ADMIN_YELL_DURATION_MAX'];
        }

        return $this->_array2String($this->_clientRequest('admin.yell '.$text.' '.$durationInMS.' {%team%} '.$teamID),
            0);
    }

    /**
     * sends a chat message to a player. the message must be less than 100 characters long.
     *
     * @param  string
     * @param  string
     *
     * @return string
     * @throws RCONException
     */
    public function adminSayMessageToPlayer($playerName, $text): string
    {
        return $this->_array2String($this->_clientRequest('admin.say '.$text.' {%player%} '.$playerName), 0);
    }

    /*-- admin commands --*/

    /**
     * sends a chat mesage to a team. the message must be less than 100 characters long.
     *
     * @param  string
     * @param  int
     *
     * @return string
     * @throws RCONException
     */
    public function adminSayMessageToTeam($teamID, $text): string
    {
        return $this->_array2String($this->_clientRequest('admin.say '.$text.' {%team%} '.$teamID), 0);
    }

    /**
     * sends a chat mesage to all. the message must be less than 100 characters long.
     *
     * @param  string
     *
     * @return string
     * @throws RCONException
     */
    public function adminSayMessageToAll($text): string
    {
        return $this->_array2String($this->_clientRequest('admin.say '.$text.' {%all%}'), 0);
    }

    /**
     * runs the next level on maplist.
     *
     * @return string
     * @throws RCONException
     */
    public function adminRunNextLevel(): string
    {
        return $this->_array2String($this->_clientRequest('admin.runNextLevel'), 0);
    }

    /**
     * restarts the current level.
     *
     * @return string
     * @throws RCONException
     */
    public function adminRestartMap(): string
    {
        return $this->_array2String($this->_clientRequest('mapList.restartRound'), 0);
    }

    /**
     * sets a new playlist<br /><br />
     * example: adminSetPlaylist("SQDM") - for setting playmode to 'Squad Deathmatch'.
     *
     * @param  string
     *
     * @return string
     * @throws RCONException
     */
    public function adminSetPlaylist($playmodeURI): string
    {
        return $this->_array2String($this->_clientRequest('admin.setPlaylist '.$playmodeURI), 0);
    }

    /**
     * returns all available playmodes on server.
     *
     * @return array
     * @throws RCONException
     */
    public function adminGetPlaylists(): array
    {
        return $this->_clientRequest('admin.getPlaylists');
    }

    /**
     * TODO: planned feature adminSetNextLevel() ?
     * sets the next level to play.
     *
     * @param  string
     *
     * @return string
     */
    //function adminSetNextLevel($mapURI) {
    //
    //}

    /**
     * returns current playmode on server.
     *
     * @return string
     * @throws RCONException
     * @see getCurrentPlaymode()
     */
    public function adminGetPlaylist(): string
    {
        return $this->_array2String($this->_clientRequest('admin.getPlaylist'));
    }

    /**
     * loads the maplist.
     *
     * @return array
     * @throws RCONException
     * @see adminMaplistList()
     */
    public function adminMaplistLoad(): array
    {
        return $this->_clientRequest('mapList.load');
    }

    /**
     * saves the current maplist to file.
     *
     * @return string
     * @throws RCONException
     */
    public function adminMaplistSave(): string
    {
        return $this->_array2String($this->_clientRequest('mapList.save'), 0);
    }

    /**
     * returns the maplist from map file.
     *
     * @return array
     * @throws RCONException
     */
    public function adminMaplistList(): array
    {
        return $this->_clientRequest('mapList.list');
    }

    /**
     * clears the maplist file.
     *
     * @return string
     * @throws RCONException
     */
    public function adminMaplistClear(): string
    {
        return $this->_array2String($this->_clientRequest('mapList.clear'), 0);
    }

    /**
     * removes a given map from maplist<br />
     * [ index = Integer! ].
     *
     * @param  int
     *
     * @return string
     * @throws RCONException
     */
    public function adminMaplistRemove($rowID): string
    {
        return $this->_array2String($this->_clientRequest('mapList.remove '.$rowID), 0);
    }

    /**
     * appends a given map to the end of the maplist file.
     *
     * @param  string
     *
     * @return string
     * @throws RCONException
     */
    public function adminMaplistAppend($mapURI): string
    {
        return $this->_array2String($this->_clientRequest('mapList.append '.$mapURI), 0);
    }

    /**
     * gets index of next map to be run.
     *
     * @return int
     * @throws RCONException
     */
    public function adminMaplistGetNextMapIndex(): int
    {
        return (int) $this->_array2String($this->_clientRequest('mapList.getMapIndices'), 2);
    }

    /**
     * sets index of next map to be run<br />
     * [ index = Integer! ].
     *
     * @param  int
     *
     * @return string
     * @throws RCONException
     */
    public function adminMaplistSetNextMapIndex($index): string
    {
        return $this->_array2String($this->_clientRequest('mapList.nextLevelIndex '.$index), 0);
    }

    /**
     * adds map with name at the specified index to the maplist.
     *
     * @param  int
     * @param  string
     *
     * @return string
     * @throws RCONException
     */
    public function adminMaplistInsertMapInIndex($index, $mapURI): string
    {
        return $this->_array2String($this->_clientRequest('mapList.insert '.$index.' '.$mapURI), 0);
    }

    /**
     * gets list of maps supported by given playmode.
     *
     * @param  string
     *
     * @return array
     * @throws RCONException
     */
    public function adminGetSupportedMaps($playmode): array
    {
        return $this->_clientRequest('admin.supportedMaps '.$playmode);
    }

    /**
     * kicks a specified player by playername.
     *
     * @param  string
     *
     * @return string
     * @throws RCONException
     */
    public function adminKickPlayer($playerName): string
    {
        return $this->_array2String($this->_clientRequest('admin.kickPlayer '.$playerName), 0);
    }

    /**
     * kicks a specified player by playername with a given kickreason.
     *
     * @param  string
     * @param  string (optional) - if not set, default kickreason is given
     *
     * @return string
     * @throws RCONException
     */
    public function adminKickPlayerWithReason($playerName, $reason = 'Kicked by administrator'): string
    {
        return $this->_array2String($this->_clientRequest('admin.kickPlayer '.$playerName.' {%reason%} '.$reason), 0);
    }

    /**
     * bans a specified player by playername for a given range of time.<br />
     * range of time can be: perm = permanent, round = current round<br />
     * if no range is given, the player will be banned permanently
     * TODO: ban for xx seconds
     * TODO: banreason.
     *
     * @param  string
     * @param  string (optional) - if not set, given player will be banned permanently
     *
     * @return string
     * @throws RCONException
     */
    public function adminBanAddPlayername($playerName, $timerange = 'perm'): string
    {
        return $this->_array2String($this->_clientRequest('banList.add name '.$playerName.' '.$timerange));
    }

    /**
     * bans a specified player by given playerip<br />
     * range of time can be: perm = permanent, round = current round<br />
     * if no range is given, the ip will be banned permanently
     * TODO: ban for xx seconds
     * TODO: banreason.
     *
     * @param  string
     * @param  string (optional) - if not set, ip will be banned permanently
     *
     * @return string
     * @throws RCONException
     */
    public function adminBanAddPlayerIP($playerIP, $timerange = 'perm'): string
    {
        return $this->_array2String($this->_clientRequest('banList.add ip '.$playerIP.' '.$timerange), 0);
    }

    /**
     * bans a specified player by given playerguid<br />
     * range of time can be: perm = permanent, round = current round<br />
     * if no range is given, the ip will be banned permanently
     * TODO: ban for xx seconds
     * TODO: banreason.
     *
     * @param  string
     * @param  int  $timerange
     *
     * @return string
     * @throws RCONException
     */
    public function adminBanAddPlayerGUID($playerName, $timerange = 2): ?string
    {
        $playerGUID = $this->adminGetPlayerGUID($playerName);
        if ($playerGUID != $this->_globalMsg['PLAYER_NOT_FOUND']) {
            return $this->_array2String($this->_clientRequest('banList.add GUID '.$playerGUID.' '.$timerange), 0);
        }

        return $this->_globalMsg['PLAYER_NOT_FOUND'];
    }

    /**
     * returns the GUID of a given playername.
     *
     * @param  string
     *
     * @return string
     * @throws RCONException
     */
    public function adminGetPlayerGUID($playerName): string
    {
        $playerInfo = $this->adminGetPlayerdata($playerName);
        if (!isset($playerInfo[14])) {
            return $this->_globalMsg['PLAYER_NOT_FOUND'];
        }

        return $this->_array2String($playerInfo, 14);
    }

    /**
     * gets the gamedata of a given playername on the gameserver
     * TODO: check for playerNotFound.
     *
     * @param  string (optional) - if not set, all players will be listed
     *
     * @return array
     * @throws RCONException
     */
    public function adminGetPlayerdata($playerName = ''): array
    {
        if (!isset($playerName) || $playerName == '') {
            $playerName = 'all';
        } else {
            $playerName = 'player '.$playerName;
        }

        return $this->_clientRequest('admin.listPlayers '.$playerName);
    }

    /**
     * saves the current banlist to banlist file.
     *
     * @return string
     * @throws RCONException
     */
    public function adminBanlistSave(): string
    {
        return $this->_array2String($this->_clientRequest('banList.save'), 0);
    }

    /**
     * loads the banlist from banlist file.
     *
     * @return string
     * @throws RCONException
     */
    public function adminBanlistLoad(): string
    {
        return $this->_array2String($this->_clientRequest('banList.load'), 0);
    }

    /**
     * unbans a player by playername.
     *
     * @param  string
     *
     * @return string
     * @throws RCONException
     */
    public function adminBanRemovePlayername($playerName): string
    {
        return $this->_array2String($this->_clientRequest('banList.remove name '.$playerName), 0);
    }

    /**
     * unbans a player by playerip.
     *
     * @param  string
     *
     * @return string
     * @throws RCONException
     */
    public function adminBanRemovePlayerIP($playerIP): string
    {
        return $this->_array2String($this->_clientRequest('banList.remove ip '.$playerIP), 0);
    }

    /**
     * unbans a player by playerip.
     *
     * @param  string
     *
     * @return string
     * @throws RCONException
     */
    public function adminBanRemovePlayerGUID($playerGUID): string
    {
        return $this->_array2String($this->_clientRequest('banList.remove GUID '.$playerGUID), 0);
    }

    /**
     * clears all bans from playername banlist.
     *
     * @return string
     * @throws RCONException
     */
    public function adminBanlistClear(): string
    {
        return $this->_array2String($this->_clientRequest('banList.clear'), 0);
    }

    /**
     * lists all bans from banlist.
     *
     * @param  string  $offset
     *
     * @return array
     * @throws RCONException
     */
    public function adminBanlistList($offset = '0'): array
    {
        return $this->_clientRequest('banList.list '.$offset);
    }

    /**
     * loads the file containing all reserved slots<br />
     * [ I don't know if this function is useful.. ].
     *
     * @return string
     * @throws RCONException
     */
    public function adminReservedSlotsLoad(): string
    {
        return $this->_array2String($this->_clientRequest('reservedSlotsList.load'), 0);
    }

    /**
     * saves made changes to reserved slots file.
     *
     * @return string
     * @throws RCONException
     */
    public function adminReservedSlotsSave(): string
    {
        return $this->_array2String($this->_clientRequest('reservedSlotsList.save'), 0);
    }

    /**
     * adds a player by given playername to reserved slots file.
     *
     * @param  string
     *
     * @return string
     * @throws RCONException
     */
    public function adminReservedSlotsAddPlayer($playerName): string
    {
        return $this->_array2String($this->_clientRequest('reservedSlotsList.addPlayer '.$playerName), 0);
    }

    /**
     * removes a player by given playername from reserved slots file.
     *
     * @param  string
     *
     * @return string
     * @throws RCONException
     */
    public function adminReservedSlotsRemovePlayer($playerName): string
    {
        return $this->_array2String($this->_clientRequest('reservedSlotsList.removePlayer '.$playerName), 0);
    }

    /**
     * clears the file containing all reserved slots.
     *
     * @return string
     * @throws RCONException
     */
    public function adminReservedSlotsClear(): string
    {
        return $this->_array2String($this->_clientRequest('reservedSlotsList.clear'), 0);
    }

    /**
     * lists all playernames in reserved slots file.
     *
     * @return array
     * @throws RCONException
     */
    public function adminReservedSlotsList(): array
    {
        return $this->_clientRequest('reservedSlotsList.list');
    }

    /**
     * kills the given player without counting this death on the playerstats.
     *
     * @param $playerName
     *
     * @return string
     * @throws RCONException
     */
    public function adminKillPlayer($playerName): string
    {
        return $this->_array2String($this->_clientRequest('admin.killPlayer '.$playerName), 0);
    }

    /**
     * moves the given player to the opponent team<br />
     * if $forceKill is true, the player will be killed<br />
     * [ Works only if the player is dead! Otherwise $forceKill has to be true! ].
     *
     * @param $playerName
     * @param $forceKill  (optional) - if not set, player will not be killed
     *
     * @return string
     * @throws RCONException
     */
    public function adminMovePlayerSwitchTeam($playerName, bool $forceKill = false): string
    {
        $playerTeam = $this->getPlayerTeamID($playerName);

        $forceKill = $this->_bool2String($forceKill);

        $newPlayerTeam = null;

        if ($playerTeam == '1') {
            $newPlayerTeam = '2';
        } elseif ($playerTeam == '2') {
            $newPlayerTeam = '1';
        }

        if ($newPlayerTeam === null) {
            throw new RCONException('Team ID cannot be null');
        }

        return $this->_array2String($this->_clientRequest('admin.movePlayer '.$playerName.' '.$newPlayerTeam.' 0 '.$forceKill),
            0);
    }

    /**
     * returns the teamid of a given playername.
     *
     * @param  string
     *
     * @return int
     * @throws RCONException
     */
    public function getPlayerTeamID($playerName): int
    {
        $playerInfo = $this->getPlayerdata($playerName);
        if (!isset($playerInfo[15])) {
            return $this->_globalMsg['PLAYER_NOT_FOUND'];
        }

        return (int) $this->_array2String($playerInfo, 15);
    }

    /**
     * moves the given player to another specific squad<br />
     * if $forceKill is true, the player will be killed<br />
     * [ Works only if the player is dead! Otherwise $forceKill has to be true! ].
     *
     * @param $playerName
     * @param $newSquadID
     * @param $forceKill  (optional) - if not set, player will not be killed
     * @param $newTeamID  (optional) - if not set, player will stay in his team
     *
     * @return string
     * @throws RCONException
     */
    public function adminMovePlayerSwitchSquad($playerName, $newSquadID, $forceKill = false, $newTeamID = null): string
    {
        if ($newTeamID == null || !is_int($newTeamID)) {
            $newTeamID = $this->getPlayerTeamID($playerName);
        }

        $forceKill = $this->_bool2String($forceKill);

        return $this->_array2String($this->_clientRequest('admin.movePlayer '.$playerName.' '.$newTeamID.' '.$newSquadID.' '.$forceKill),
            0);
    }

    /**
     * Gives player leader of their current squad.
     *
     * @param  string  $playerName
     *
     * @return string
     * @throws RCONException
     */
    public function adminSquadLeader($playerName): string
    {
        $teamID = $this->getPlayerTeamID($playerName);
        $squadID = $this->getPlayerSquadID($playerName);

        return $this->_array2String($this->_clientRequest('squad.leader '.$teamID.' '.$squadID.' '.$playerName), 0);
    }

    /**
     * returns the squadid of a given playername.
     *
     * @param  string
     *
     * @return int
     * @throws RCONException
     */
    public function getPlayerSquadID($playerName): int
    {
        $playerInfo = $this->getPlayerdata($playerName);
        if (!isset($playerInfo[16])) {
            return $this->_globalMsg['PLAYER_NOT_FOUND'];
        }

        return (int) $this->_array2String($playerInfo, 16);
    }

    /**
     * Checks if the squad is locked.
     *
     * @param  int  $teamID
     * @param  int  $squadID
     *
     * @return bool
     * @throws RCONException
     */
    public function adminGetSquadPrivate($teamID, $squadID): bool
    {
        return $this->_array2boolean($this->_clientRequest('squad.private '.$teamID.' '.$squadID));
    }

    /**
     * Set the squad to private or not.
     *
     * @param  int   $teamID
     * @param  int   $squadID
     * @param  bool  $private
     *
     * @return bool
     * @throws RCONException
     */
    public function adminSetSquadPrivate($teamID, $squadID, $private = true): bool
    {
        return $this->_array2boolean($this->_clientRequest('squad.private '.$teamID.' '.$squadID.' '.$this->_bool2String($private)));
    }

    /**
     * Get all squads that have players in them on a specific team.
     *
     * @param  int  $teamID
     *
     * @return array
     * @throws RCONException
     */
    public function adminSquadListActive($teamID): array
    {
        return $this->_clientRequest('squad.listActive '.$teamID);
    }

    /**
     * Get player count and names of soldiers in a specific squad.
     *
     * @param  int  $teamID
     * @param  int  $squadID
     *
     * @return array
     * @throws RCONException
     */
    public function adminSquadListPlayer($teamID, $squadID): array
    {
        return $this->_clientRequest('squad.listPlayers '.$teamID.' '.$squadID);
    }

    /**
     * ends the current round, declaring the given teamId as winner.
     *
     * @param $teamId
     *
     * @return string
     * @throws RCONException
     */
    public function adminEndRound($teamId): string
    {
        return $this->_array2String($this->_clientRequest('mapList.endRound '.$teamId), 0);
    }

    /*-- admin server settings --*/

    /**
     * sets 3d spotting on maps on/off.
     *
     * @param  bool
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSet3dSpotting($boolean): string
    {
        return $this->_array2String($this->_clientRequest('vars.3dSpotting '.$this->_bool2String($boolean)), 0);
    }

    /**
     * gets true/false, if 3d spotting is enabled or not.
     *
     * @return bool
     * @throws RCONException
     */
    public function adminVarGet3dSpotting(): bool
    {
        return $this->_array2boolean($this->_clientRequest('vars.3dSpotting'));
    }

    /**
     * sets the 3rd person vehicle cam on/off<br /><br />.
     *
     * @param  bool
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSet3rdPersonVehiCam($boolean): string
    {
        return $this->_array2String($this->_clientRequest('vars.3pCam '.$this->_bool2String($boolean)), 0);
    }

    /**
     * gets true/false, if 3rd person vehicle cam is enabled or not.
     *
     * @return bool
     * @throws RCONException
     */
    public function adminVarGet3rdPersonVehiCam(): bool
    {
        return $this->_array2boolean($this->_clientRequest('vars.3pCam'));
    }

    /**
     * sets if all unlocks are available on/off<br /><br />.
     *
     * @param  bool
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetAllUnlocksUnlocked($boolean): string
    {
        return $this->_array2String($this->_clientRequest('vars.allUnlocksUnlocked '.$this->_bool2String($boolean)), 0);
    }

    /**
     * gets true/false, if all unlocks are available or not.
     *
     * @return bool
     * @throws RCONException
     */
    public function adminVarGetAllUnlocksUnlocked(): bool
    {
        return $this->_array2boolean($this->_clientRequest('vars.allUnlocksUnlocked'));
    }

    /**
     * sets teambalance on/off.
     *
     * @param  bool
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetTeambalance($boolean): string
    {
        return $this->_array2String($this->_clientRequest('vars.autoBalance '.$this->_bool2String($boolean)), 0);
    }

    /**
     * gets true/false, if teambalance is enabled or not.
     *
     * @return bool
     * @throws RCONException
     */
    public function adminVarGetTeambalance(): bool
    {
        return $this->_array2boolean($this->_clientRequest('vars.autoBalance'));
    }

    /**
     * sets the bullet damage modifier in %.
     *
     * @param $integer
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetbulletDamageModifier($integer): string
    {
        return $this->_array2String($this->_clientRequest('vars.bulletDamage '.$integer), 0);
    }

    /**
     * gets the bullet damage modifier.
     *
     * @return int
     * @throws RCONException
     */
    public function adminVarGetbulletDamageModifier(): int
    {
        return (int) $this->_array2String($this->_clientRequest('vars.bulletDamage'));
    }

    /**
     * sets crosshair on/off.
     *
     * @param  bool
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetCrosshair($boolean): string
    {
        return $this->_array2String($this->_clientRequest('vars.crossHair '.$this->_bool2String($boolean)), 0);
    }

    /**
     * gets true/false, if crosshair is enabled or not.
     *
     * @return bool
     * @throws RCONException
     */
    public function adminVarGetCrosshair(): bool
    {
        return $this->_array2boolean($this->_clientRequest('vars.crossHair'));
    }

    /**
     * sets friendly fire on/off.
     *
     * @param  bool
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetFriendlyFire($boolean): string
    {
        return $this->_array2String($this->_clientRequest('vars.friendlyFire '.$this->_bool2String($boolean)), 0);
    }

    /**
     * gets true/false, if friendly fire is enabled or not.
     *
     * @return bool
     * @throws RCONException
     */
    public function adminVarGetFriendlyFire(): bool
    {
        return $this->_array2boolean($this->_clientRequest('vars.friendlyFire'));
    }

    /**
     * sets a new password on the gameserver.<br />
     * password MUST NOT contain whitespaces!!<br /><br />
     * to clear the password, use adminVarGetGamepassword("").
     *
     * @param  string
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetGamepassword($serverPassword): string
    {
        return $this->_array2String($this->_clientRequest('vars.gamePassword '.$serverPassword), 0);
    }

    /**
     * sets hud on/off.
     *
     * @param  bool
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetHud($boolean): string
    {
        return $this->_array2String($this->_clientRequest('vars.hud '.$this->_bool2String($boolean)), 0);
    }

    /**
     * gets true/false, if hud is enabled or not.
     *
     * @return bool
     * @throws RCONException
     */
    public function adminVarGetHud(): bool
    {
        return $this->_array2boolean($this->_clientRequest('vars.hud'));
    }

    /**
     * sets how many seconds a player can be idle before he/she is kicked from the server.
     *
     * @param $idleTimeoutInteger
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetIdleTimeout($idleTimeoutInteger): string
    {
        return $this->_array2String($this->_clientRequest('vars.idleTimeout '.$idleTimeoutInteger), 0);
    }

    /**
     * gets the current idle time allowed.
     *
     * @return int
     * @throws RCONException
     */
    public function adminVarGetIdleTimeout(): int
    {
        return (int) $this->_array2String($this->_clientRequest('vars.idleTimeout'));
    }

    /**
     * sets the number of rounds a player who is kicked for idle is banned
     * set to 0 to disable.
     *
     * @param $idleBanRoundsInteger
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetidleBanRounds($idleBanRoundsInteger): string
    {
        return $this->_array2String($this->_clientRequest('vars.idleBanRounds '.$idleBanRoundsInteger), 0);
    }

    /**
     * gets the number of rounds a player who is kicked for idle is banned.
     *
     * @return int
     * @throws RCONException
     */
    public function adminVarGetidleBanRounds(): int
    {
        return (int) $this->_array2String($this->_clientRequest('vars.idleBanRounds'));
    }

    /**
     * sets killcam on/off.
     *
     * @param  bool
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetKillCam($boolean): string
    {
        return $this->_array2String($this->_clientRequest('vars.killCam '.$this->_bool2String($boolean)), 0);
    }

    /**
     * gets true/false, if killcam is enabled or not.
     *
     * @return bool
     * @throws RCONException
     */
    public function adminVarGetKillCam(): bool
    {
        return $this->_array2boolean($this->_clientRequest('vars.killCam'));
    }

    /**
     * sets minimap on/off.
     *
     * @param  bool
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetMiniMap($boolean): string
    {
        return $this->_array2String($this->_clientRequest('vars.miniMap '.$this->_bool2String($boolean)), 0);
    }

    /**
     * gets true/false, if minimap is enabled or not.
     *
     * @return bool
     * @throws RCONException
     */
    public function adminVarGetMiniMap(): bool
    {
        return $this->_array2boolean($this->_clientRequest('vars.miniMap'));
    }

    /**
     * sets minimap spotting on/off.
     *
     * @param  bool
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetMiniMapSpotting($boolean): string
    {
        return $this->_array2String($this->_clientRequest('vars.miniMapSpotting '.$this->_bool2String($boolean)), 0);
    }

    /**
     * gets true/false, if minimap spotting is enabled or not.
     *
     * @return bool
     * @throws RCONException
     */
    public function adminVarGetMiniMapSpotting(): bool
    {
        return $this->_array2boolean($this->_clientRequest('vars.miniMapSpotting'));
    }

    /**
     * sets nametag on/off.
     *
     * @param  bool
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetNameTag($boolean): string
    {
        return $this->_array2String($this->_clientRequest('vars.nameTag '.$this->_bool2String($boolean)), 0);
    }

    /**
     * gets true/false, if nametag is enabled or not.
     *
     * @return bool
     * @throws RCONException
     */
    public function adminVarGetNameTag(): bool
    {
        return $this->_array2boolean($this->_clientRequest('vars.nameTag'));
    }

    /**
     * sets onlySquadLeaderSpawn on/off.
     *
     * @param  bool
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetOnlySquadLeaderSpawn($boolean): string
    {
        return $this->_array2String($this->_clientRequest('vars.onlySquadLeaderSpawn '.$this->_bool2String($boolean)),
            0);
    }

    /**
     * gets true/false, if onlySquadLeaderSpawn is enabled or not.
     *
     * @return bool
     * @throws RCONException
     */
    public function adminVarGetOnlySquadLeaderSpawn(): bool
    {
        return $this->_array2boolean($this->_clientRequest('vars.onlySquadLeaderSpawn'));
    }

    /**
     * sets the man down time modifier in %.
     *
     * @param $integer
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetPlayerManDownTimeModifier($integer): string
    {
        return $this->_array2String($this->_clientRequest('vars.playerManDownTime '.$integer), 0);
    }

    /**
     * gets the man down time modifier.
     *
     * @return int
     * @throws RCONException
     */
    public function adminVarGetPlayerManDownTimeModifier(): int
    {
        return (int) $this->_array2String($this->_clientRequest('vars.playerManDownTime'));
    }

    /**
     * sets the respawn time modifier in %.
     *
     * @param $integer
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetPlayerRespawnTimeModifier($integer): string
    {
        return $this->_array2String($this->_clientRequest('vars.playerRespawnTime '.$integer), 0);
    }

    /**
     * gets the respawn time modifier.
     *
     * @return int
     * @throws RCONException
     */
    public function adminVarGetPlayerRespawnTimeModifier(): int
    {
        return (int) $this->_array2String($this->_clientRequest('vars.playerRespawnTime'));
    }

    /**
     * gets true/false, if punkbuster is enabled or not
     * FFIXX Only works for Rush right now.
     *
     * @return bool
     * @throws RCONException
     */
    public function adminVarGetPunkbuster(): bool
    {
        $serverInfo = $this->getServerInfo();

        return $this->_array2boolean($serverInfo, 14);
    }

    /**
     * gets true/false if ranked server settings are enabled or not
     * FFIXX Only works for Rush right now.
     *
     * @return bool
     * @throws RCONException
     */
    public function adminVarGetRanked(): bool
    {
        $serverInfo = $this->getServerInfo();

        return $this->_array2boolean($serverInfo, 13);
    }

    /**
     * sets health regeneration on/off.
     *
     * @param  bool
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetRegenerateHealth($boolean): string
    {
        return $this->_array2String($this->_clientRequest('vars.regenerateHealth '.$this->_bool2String($boolean)), 0);
    }

    /**
     * gets true/false, if health regeneration is enabled or not.
     *
     * @return bool
     * @throws RCONException
     */
    public function adminVarGetRegenerateHealth(): bool
    {
        return $this->_array2boolean($this->_clientRequest('vars.regenerateHealth'));
    }

    /**
     * sets how many players are required to begin a round.
     *
     * @param  string
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetRoundStartPlayerCount($string): string
    {
        return $this->_array2String($this->_clientRequest('vars.roundStartPlayerCount '.$string), 0);
    }

    /**
     * gets how many players are required to begin a round.
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarGetRoundStartPlayerCount(): string
    {
        return $this->_array2String($this->_clientRequest('vars.roundStartPlayerCount'));
    }

    /**
     * sets at how many players the server switches to pre-round.
     *
     * @param  string
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetRoundRestartPlayerCount($string): string
    {
        return $this->_array2String($this->_clientRequest('vars.roundRestartPlayerCount '.$string), 0);
    }

    /**
     * gets at how many players the server switches to pre-round.
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarGetRoundRestartPlayerCount(): string
    {
        return $this->_array2String($this->_clientRequest('vars.roundRestartPlayerCount'));
    }

    /**
     * sets the server description to given text<br />
     * [ Character '|' doesn't work for now as a 'newline'. ]<br /><br />
     * ##Request from RSPs: In addition being able to enter a new line would be<br />
     * great, BF2142 used the "|" character as newline.
     *
     * @param  string
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetServerDescription($string): string
    {
        return $this->_array2String($this->_clientRequest('vars.serverDescription '.$string), 0);
    }

    /**
     * gets the server description.
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarGetServerDescription(): string
    {
        return $this->_array2String($this->_clientRequest('vars.serverDescription'));
    }

    /**
     * sets a new servername.
     *
     * @param $serverName
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetServername($serverName): string
    {
        return $this->_array2String($this->_clientRequest('vars.serverName '.$serverName), 0);
    }

    /**
     * sets the soldier health modifier in %.
     *
     * @param $soldierHealthInteger
     *
     * @return string
     * @throws RCONException
     * @internal param $soldierHealthInteger
     * @internal param $integer
     *
     */
    public function adminVarSetSoldierHealthModifier($soldierHealthInteger): string
    {
        return $this->_array2String($this->_clientRequest('vars.soldierHealth '.$soldierHealthInteger), 0);
    }

    /**
     * gets the soldier health modifier.
     *
     * @return int
     * @throws RCONException
     */
    public function adminVarGetSoldierHealthModifier(): int
    {
        return (int) $this->_array2String($this->_clientRequest('vars.soldierHealth'));
    }

    /**
     * sets the number of teamkills allowed during one round, before the game kicks
     * the player in question.
     *
     * @param $teamKillCountForKickInteger
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetTeamKillCountForKick($teamKillCountForKickInteger): string
    {
        return $this->_array2String($this->_clientRequest('vars.teamKillCountForKick '.$teamKillCountForKickInteger),
            0);
    }

    /**
     * gets the number of teamkills allowed during one round.
     *
     * @return int
     * @throws RCONException
     */
    public function adminVarGetTeamKillCountForKick(): int
    {
        return (int) $this->_array2String($this->_clientRequest('vars.teamKillCountForKick'));
    }

    /**
     * sets the highest kill-value allowed before a player is kicked for teamkilling<br />
     * set to 0 to disable kill value mechanism.
     *
     * @param $teamKillValueForKickInteger
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetTeamKillValueForKick($teamKillValueForKickInteger): string
    {
        return $this->_array2String($this->_clientRequest('vars.teamKillValueForKick '.$teamKillValueForKickInteger),
            0);
    }

    /**
     * gets the highest kill-value allowed before a player is kicked for teamkilling.
     *
     * @return int
     * @throws RCONException
     */
    public function adminVarGetTeamKillValueForKick(): int
    {
        return (int) $this->_array2String($this->_clientRequest('vars.teamKillValueForKick'));
    }

    /**
     * sets the value of a teamkill (adds to the player's current kill-value).
     *
     * @param $teamKillValueIncreaseInteger
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetTeamKillValueIncrease($teamKillValueIncreaseInteger): string
    {
        return $this->_array2String($this->_clientRequest('vars.teamKillValueIncrease '.$teamKillValueIncreaseInteger),
            0);
    }

    /**
     * gets the value of a teamkill.
     *
     * @return int
     * @throws RCONException
     */
    public function adminVarGetTeamKillValueIncrease(): int
    {
        return (int) $this->_array2String($this->_clientRequest('vars.teamKillValueIncrease'));
    }

    /**
     * sets how much every player's kill-value should decrease per second.
     *
     * @param $teamKillValueDecreaseInteger
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetTeamKillValueDecreasePerSecond($teamKillValueDecreaseInteger): string
    {
        return $this->_array2String($this->_clientRequest('vars.teamKillValueDecreasePerSecond '.$teamKillValueDecreaseInteger),
            0);
    }

    /**
     * gets the decrease value.
     *
     * @return int
     * @throws RCONException
     */
    public function adminVarGetTeamKillValueDecreasePerSecond(): int
    {
        return (int) $this->_array2String($this->_clientRequest('vars.teamKillValueDecreasePerSecond'));
    }

    /**
     * sets the number of teamkill-kicks allowed before the game bans the player in question.
     *
     * @param $teamKillKickForBanInteger
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetTeamKillKickForBan($teamKillKickForBanInteger): string
    {
        return $this->_array2String($this->_clientRequest('vars.teamKillKickForBan '.$teamKillKickForBanInteger), 0);
    }

    /**
     * gets the number of teamkill-kicks allowed.
     *
     * @return int
     * @throws RCONException
     */
    public function adminVarGetTeamKillKickForBan(): int
    {
        return (int) $this->_array2String($this->_clientRequest('vars.teamKillKickForBan'));
    }

    /**
     * sets vehicle spawn on/off.
     *
     * @param  bool
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarSetVehicleSpawnAllowed($boolean): string
    {
        return $this->_array2String($this->_clientRequest('vars.vehicleSpawnAllowed '.$this->_bool2String($boolean)),
            0);
    }

    /**
     * gets true/false, if vehicle spawn is enabled or not.
     *
     * @return bool
     * @throws RCONException
     */
    public function adminVarGetVehicleSpawnAllowed(): bool
    {
        return $this->_array2boolean($this->_clientRequest('vars.vehicleSpawnAllowed'));
    }

    /**
     * sets the vehicle spawn modifier in %.
     *
     * @param $vehicleSpawnDelayInteger
     *
     * @return string
     * @throws RCONException
     * @internal param $vehicleSpawnDelayInteger
     * @internal param $integer
     *
     */
    public function adminVarSetVehicleSpawnDelayModifier($vehicleSpawnDelayInteger): string
    {
        return $this->_array2String($this->_clientRequest('vars.vehicleSpawnDelay '.$vehicleSpawnDelayInteger), 0);
    }

    /**
     * gets the soldier health modifier.
     *
     * @return int
     * @throws RCONException
     */
    public function adminVarGetVehicleSpawnDelayModifier(): int
    {
        return (int) $this->_array2String($this->_clientRequest('vars.vehicleSpawnDelay'));
    }

    /**
     * gets the server type: Official, Ranked, Unranked or Private.
     *
     * @return string
     * @throws RCONException
     */
    public function adminVarGetServerType(): string
    {
        return $this->_array2String($this->_clientRequest('vars.serverType'));
    }

    /**
     * gets true/false, if server is noob only. If true, only players with rank <= 10 can join.
     *
     * @return bool
     * @throws RCONException
     */
    public function adminVarGetNoobJoin(): bool
    {
        return $this->_array2boolean($this->_clientRequest('vars.IsNoobOnlyJoin'));
    }

    /**
     * gets the team name by there faction id.
     *
     * @param  int|null  $integer  Team ID
     *
     * @return array
     * @throws RCONException
     */
    public function adminVarGetTeamFaction($integer = null): array
    {
        $factions = trans('rcon.scoreboard.factions');

        $teamFactions = $this->_clientRequest('vars.teamFactionOverride');

        if ($integer === null) {
            return [
                $factions,
                $teamFactions,
            ];
        }

        return $factions[$teamFactions[$integer]];
    }

    /**
     * gets scale factor for number of tickets to end round, in percent.
     *
     * @return int
     * @throws RCONException
     */
    public function adminVarGetGameModeCounter(): int
    {
        return (int) $this->_array2String($this->_clientRequest('vars.gameModeCounter'), 1);
    }

    /**
     * @return int
     * @throws RCONException
     */
    public function adminVarGetRoundTimeLimit(): int
    {
        return (int) $this->_array2String($this->_clientRequest('vars.roundTimeLimit'), 1);
    }

    /**
     * Tabulates a result containing columns and rows.
     *
     * @param  array  $res
     *
     * @return array
     */
    public function tabulate($res): array
    {
        array_shift($res);
        $nColumns = $res[0];
        $columns = [];

        for ($i = 1; $i <= $nColumns; $i++) {
            $columns[] = $res[$i];
        }

        $nRows = $res[11];
        $rows = [
            'players' => [],
            'columns' => [],
        ];

        for ($n = 0; $n < $nRows; $n++) {
            $row = [];

            foreach ($columns as $jValue) {
                $value = $res[++$i];
                $row[$jValue] = is_numeric($value) ? (int) $value : $value;
            }


            $rows['players'][] = $row;
        }

        $rows['columns'] = $columns;

        return $rows;
    }

    // TODO: server moderation mode
}