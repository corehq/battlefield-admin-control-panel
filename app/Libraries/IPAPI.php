<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/15/20, 10:31 PM
 */

namespace App\Libraries;


/**
 * Class IPAPI
 *
 * @package App\Libraries
 */
class IPAPI
{
    /**
     * @var string|null
     */
    private $ip;

    /**
     * IPAPI constructor.
     *
     * @param  string|null  $ip
     *
     * @throws \Throwable
     */
    public function __construct(?string $ip = null)
    {
        $this->ip = $ip;

        throw_if(empty(config('IPAPI_ACCESS_KEY')),
            new \RuntimeException('No API Key Set. Please set this before continuing.', 1002));
        if (empty(config('IPAPI_ACCESS_KEY'))) {
            throw new \RuntimeException('No API Key Set. Please set this before continuing.', 1002);
        }
    }

    /**
     * @return string|null
     */
    public function getIp(): ?string
    {
        return $this->ip;
    }

    /**
     * @param  string|null  $ip
     *
     * @return IPAPI
     */
    public function setIp(?string $ip): IPAPI
    {
        $this->ip = $ip;
        return $this;
    }


}