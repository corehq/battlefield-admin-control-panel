<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/27/19, 3:07 AM
 */

namespace App\Repository\Battlefield\Scoreboard;


use App\Exceptions\InvalidAdkatsCommand;
use App\Exceptions\InvalidPlayerName;
use App\Exceptions\PlayerNotFound;
use App\Exceptions\RCONException;
use App\Helpers\Battlefield;
use App\Models\Adkats\Ban;
use App\Models\Adkats\Command;
use App\Models\Adkats\Record;
use App\Models\Battlefield\Player;
use App\Models\Battlefield\Server\Server;
use Carbon\Carbon;
use Illuminate\Support\Str;

/**
 * Class Admin
 * @package App\Repository\Battlefield\Scoreboard
 */
class Admin extends LiveScoreboard
{
    /**
     * Admin constructor.
     *
     * @param Server $server
     *
     * @throws \Throwable
     */
    public function __construct(Server $server)
    {
        parent::__construct($server);
    }

    /**
     * @param string $player Name
     * @param string $message Ban message
     * @param int $duration Minutes
     * @param bool $perm Permanent ban, will override $duration
     *
     * @return Ban
     * @throws \Throwable
     */
    public function ban(string $player, string $message = "No Reason Provided", int $duration = 0, bool $perm = false): Ban
    {
        // Make sure the player name is valid.
        $this->validateName($player);

        // Get the player from the database
        $player = $this->getPlayerDbRecord($player);

        // Set the ban type for AdKats to process
        $bantype = $perm ? 'player_ban_perm' : 'player_ban_temp';

        // Set the current timestamp.
        $now = Carbon::now();

        // Clone $now so that we can have the exact same time
        $timestamp = clone $now;

        // If the $duration is greater than zero and is not flagged as a permanent ban, then add the amount of minutes
        // to enforce the ban. If, however, the $duration is less than or equal to zero default to a permanent ban.
        if ($duration > 0 && !$perm) {
            $timestamp = $timestamp->addMinutes($duration);
        } else {
            $timestamp = $timestamp->addYears(20);
        }

        // Log the action to the records table
        $record = $this->log($player, $bantype, $message, $timestamp->diffInMinutes($now), false);

        // Create or update the ban record to reflect the changes.
        $ban = Ban::updateOrCreate(['player_id' => $player->PlayerID], [
            'latest_record_id' => $record->record_id,
            'ban_startTime' => Carbon::now(),
            'ban_endTime' => $timestamp,
            'ban_status' => 'Active',
        ]);

        // Refresh the $ban to load data from the database.
        $ban->fresh();

        return $ban;
    }

    /**
     * @param string $player
     *
     * @return bool
     * @throws \Throwable
     */
    protected function validateName(string $player): bool
    {
        $v = (bool)preg_match('/^[a-zA-Z0-9_\\-]+$/', $player);

        throw_unless($v, new InvalidPlayerName("Invalid Player Name"));

        return $v;
    }

    /**
     * @param string $player
     * @param string $message
     *
     * @return Player
     * @throws \Throwable
     */
    private function getPlayerDbRecord(string $player, string $message = "Player Not Found"): Player
    {
        $p = Player::where('GameID', $this->server->game->GameID)->where('SoldierName', $player)->first();

        throw_unless($p, new PlayerNotFound(sprintf($message, $player),
            404));

        return $p;
    }

    /**
     * @param $targetPlayer
     * @param string $command
     * @param string $message
     * @param int $duration
     * @param bool $system
     *
     * @return Record
     * @throws \Throwable
     */
    private function log($targetPlayer, string $command, string $message = '', int $duration = 0, bool $system = true): Record
    {
        $ts = Carbon::now();

        $command = Command::where('command_key', $command)->first();

        throw_if(!$command, new InvalidAdkatsCommand());

        if (!$targetPlayer instanceof Player && !in_array($command->command_key, ['server_nuke'])) {
            $targetPlayer = $this->getPlayerDbRecord($targetPlayer);
        }

        $target_name = $targetPlayer->SoldierName ?? ($target ?? 'Server');
        $target_id = $targetPlayer->PlayerID ?? null;

        /** @var Record $record */
        $record = Record::create([
            'adkats_read' => $system,
            'adkats_web' => true,
            'command_action' => $command->command_id,
            'command_type' => $command->command_id,
            'command_numeric' => $duration,
            'record_message' => $message,
            'record_time' => $ts,
            'server_id' => $this->server->ServerID,
            'source_name' => 'TBD',
            'source_id' => null,
            'target_name' => $target_name,
            'target_id' => $target_id,
        ]);

        return $record;
    }

    /**
     * @param string $player
     * @param string $message
     *
     * @return Record
     * @throws \Throwable
     */
    public function kill(string $player, string $message): Record
    {
        $original = $message;

        $baseMessage = "You were killed by an admin.";

        $message = !empty($message) ? sprintf($baseMessage . " Reason: %s", $message) : $baseMessage;

        $this->validateName($player);

        $response = $this->rcon->adminKillPlayer($player);
        $this->tell($player, $message, 5, false, true, 1);

        throw_if($response == "InvalidPlayerName", new PlayerNotFound(
            sprintf("No player found with the name '%s'.", $player),
            404
        ));

        throw_if($response == "SoldierNotAlive", new PlayerNotFound(
            sprintf("'%s' is already dead. Can't kill them. Try again...", $player),
            404
        ));

        $this->tell($player, $message, null, false, true);

        return $this->log($player, 'player_kill', $original);
    }

    /**
     * Sends both a yell and say message to the player.
     *
     * @param string $player
     * @param string $message
     * @param int|null $yellDuration
     * @param bool|true $displayAdminName
     * @param bool|false $skipLog
     * @param int $times
     *
     * @return bool
     * @throws \Throwable
     */
    public function tell(
        $player = '',
        $message = '',
        $yellDuration = 10,
        $displayAdminName = true,
        $skipLog = false,
        $times = 7
    ): bool
    {
        for ($i = 0; $i < $times; $i++) {
            $this->say($message, $player, null, 'Player', $displayAdminName, true);
        }

        if (is_null($yellDuration)) {
            $yellDuration = 10;
        }

        $this->yell($message, $player, null, $yellDuration, 'Player', true);

        if (!$skipLog) {
            $this->log($player, 'player_tell', $message);
        }

        return true;
    }

    /**
     * Sends a say to the entire server, team, or player.
     *
     * @param string $message
     * @param null|string $player
     * @param null|string $teamId
     * @param string $type
     * @param bool|true $displayAdminName
     * @param bool|false $skipLog
     *
     * @return bool|Record
     * @throws RCONException
     * @throws \Throwable
     */
    public function say(
        $message = '',
        $player = '',
        $teamId = '',
        $type = 'All',
        $displayAdminName = true,
        $skipLog = false
    )
    {
        // Checks if the message is blank
        throw_if(empty($message), new RCONException('No message provided.'));

        $adminName = auth()->user()->username;

        switch ($type) {
            case 'All':

                if ($displayAdminName) {
                    $this->rcon->adminSayMessageToAll(sprintf('[%s] %s', $adminName, $message));
                } else {
                    $this->rcon->adminSayMessageToAll($message);
                }

                if (!$skipLog) {
                    return $this->log(null, 'admin_say', $message);
                }

                break;

            case 'Team':
                // Checks if a team id was sent
                throw_if(empty($teamId), new RCONException('No team id specified.'));

                throw_if(!is_numeric($teamId) || !in_array($teamId, [1, 2, 3, 4]),
                    new RCONException(sprintf('"%s" is not a valid team id', $teamId)));

                if ($displayAdminName) {
                    $this->rcon->adminSayMessageToTeam($teamId, sprintf('[%s] %s', $adminName, $message));
                } else {
                    $this->rcon->adminSayMessageToTeam($teamId, $message);
                }
                break;

            case 'Player':
                // Remove extra whitespace
                $player = trim($player);

                // Checks if name provided is blank
                throw_if(empty($player), new PlayerNotFound('No player name specified.'));
                $this->validateName($player);

                if ($displayAdminName) {
                    $response = $this->rcon->adminSayMessageToPlayer($player,
                        sprintf('[%s] %s', $adminName, $message));
                } else {
                    $response = $this->rcon->adminSayMessageToPlayer($player, $message);
                }

                // Check if the server returned a player not found error
                throw_if($response == "PlayerNotFound",
                    new PlayerNotFound(sprintf('No player found with the name "%s"', $player)));

                if (!$skipLog) {
                    return $this->log($player, 'player_say', $message);
                }
                break;

            default:
                throw new RconException(sprintf('"%s" is not a valid type.', $type));
        }

        return true;
    }

    /**
     * Sends a yell to the entire server, team, or player.
     *
     * @param string $message
     * @param null|string $player
     * @param int|null $teamId
     * @param int $duration
     * @param string $type
     * @param bool|false $skipLog
     *
     * @return bool|Record
     * @throws RCONException
     * @throws \Throwable
     */
    public function yell(
        $message = '',
        $player = '',
        $teamId = 0,
        $duration = 5,
        $type = 'All',
        $skipLog = false
    )
    {
        // Checks if the message is blank
        throw_if(empty($message), new RCONException('No message provided.'));

        if ($this->server->game->GameName == 'BFHL') {
            $message = Str::limit($message, 100);
        }

        switch ($type) {
            case 'All':
                $this->rcon->adminYellMessage($message, '{%all%}', $duration);

                if (!$skipLog) {
                    return $this->log(null, 'admin_yell', $message, $duration);
                }

                break;

            case 'Team':
                // Checks if a team id was sent
                throw_if(empty($teamId), new RCONException('No team id specified.'));

                // Checks if the team id is a number or not in valid id list
                throw_if(!is_numeric($teamId) || !in_array($teamId, [1, 2, 3, 4]),
                    new RCONException(sprintf('"%s" is not a valid team id', $teamId)));

                $this->rcon->adminYellMessageToTeam($message, $teamId, $duration);
                break;

            case 'Player':
                // Remove extra whitespace
                $player = trim($player);

                // Checks if name provided is blank
                throw_if(empty($player), new RCONException('No player name specified.'));
                $this->validateName($player);

                $response = $this->rcon->adminYellMessage($message, $player, $duration);

                // Check if the server returned a player not found error
                throw_if($response == 'PlayerNotFound',
                    new PlayerNotFound(sprintf('No player found with the name "%s"', $player)));

                if (!$skipLog) {
                    return $this->log($player, 'player_yell', $message, $duration);
                }
                break;

            default:
                throw new RconException(sprintf('"%s" is not a valid type.', $type));
        }

        return true;
    }

    /**
     * Nukes the targeted team.
     *
     * @param int $team
     *
     * @return Record
     * @throws RCONException
     * @throws \Throwable
     */
    public function nuke(int $team): Record
    {
        $players = $this->rcon->tabulate($this->rcon->adminGetPlayerlist())['players'];
        $this->setFactions();
        $teamName = $this->getTeamName($team);

        $message = sprintf("!!! Tactical Nuke Incoming !!!");

        foreach ($players as $player) {
            if ($player['teamId'] == $team) {
                $this->rcon->adminKillPlayer($player['name']);
                $this->tell($player['name'], $message, 5, false, true, 1);
            }
        }

        return $this->log($teamName['full_name'], 'server_nuke', sprintf("Team Nuked {%s}", $teamName['full_name']));
    }

    /**
     * Moves the player to a different team and/or squad.
     *
     * @param            $player
     * @param null $teamId
     * @param int $squadId
     * @param bool|false $locked
     *
     * @return array
     * @throws RCONException
     * @throws \Throwable
     */
    public function movePlayer($player, $teamId = null, $squadId = 0, $locked = false): array
    {
        if (!is_numeric($squadId) || empty($squadId) || !in_array($squadId, range(0, 32))) {
            $squadId = 0;
        }

        if (!is_numeric($teamId) || empty($teamId) || !in_array($teamId, range(1, 4))) {
            $teamId = $this->rcon->getPlayerTeamId($player);
        }

        $teamName = $this->getTeamName($teamId);
        $squadName = app(Battlefield::class)->squad($squadId);

        if (is_array($teamName)) {
            $teamName = $teamName['full_name'];
        }

        $this->validateName($player);

        if ($this->server->game->GameName == 'BF4') {
            // Check if squad is private
            if ($squadId != 0 && $this->rcon->adminGetSquadPrivate($teamId, $squadId)) {
                // Check if squad is full
                $playersInSquad = $this->rcon->adminSquadListPlayer($teamId, $squadId)[1];

                // If squad is full throw an exception with an error message
                // else unlock the squad so we can move them in.
                throw_if($playersInSquad == 5,
                    new RCONException(sprintf('%s squad is full. Cannot switch %s to squad.', $squadName, $player),
                        200));

                $this->rcon->adminSetSquadPrivate($teamId, $squadId, false);
            }
        }

        $response = $this->rcon->adminMovePlayerSwitchSquad($player, (int)$squadId, true, (int)$teamId);

        // Check if the server returned a player not found error
        throw_if($response == "InvalidPlayerName",
            new InvalidPlayerName(sprintf('No player found with the name "%s"', $player)));

        // Lock squad if $locked is truthy
        if ($this->helper->stringToBool($locked)) {
            $this->rcon->adminSetSquadPrivate($teamId, $squadId, $locked);
        }

        if ($response == 'SetSquadFailed') {
            $squadId = 0;
        }

        if ($squadId == 0) {
            $message = sprintf('You were switched to team %s and not placed in a squad.', $teamName);
        } else {
            $message = sprintf('You were switched to team %s and placed in squad %s.', $teamName, $squadName);
        }

        $dbMessage = sprintf('Switched to %s and placed in squad %s', $teamName, $squadName);

        $this->tell($player, $message, 5, false, true, 1);
        $this->log($player, 'player_fmove', $dbMessage);

        return [
            'player' => $player,
            'message' => $dbMessage,
        ];
    }

    /**
     * Kick the player from the server.
     *
     * @param string $player
     * @param null $message
     * @param bool|false $isBan
     *
     * @return array
     * @throws RCONException
     * @throws \Throwable
     */
    public function kick($player = '', $message = null, $isBan = false): array
    {
        if (empty($message)) {
            $message = 'Kicked by administrator';
        }

        $this->validateName($player);

        $response = $this->rcon->adminKickPlayerWithReason($player, $message);

        // Check if the server returned a player not found error
        throw_if(in_array($response, ['PlayerNotFound', 'InvalidPlayerName']),
            new PlayerNotFound(sprintf('No player found with the name "%s"', $player), 404));

        // If adminKick was called from the adminBan function do not send the kick message
        if (!$isBan) {
            // Send a general message to the server about the kicked player
            $this->say(sprintf('%s was kicked from the server. Reason: %s', $player, $message), null, null,
                'All', false, true);
        }

        $this->log($player, 'player_kick', $message);

        return [
            'player' => $player,
            'message' => $message,
        ];
    }

    /**
     * Punish player.
     *
     * @param $player
     * @param $message
     *
     * @return array
     * @throws \Throwable
     */
    public function punish($player, $message): array
    {
        $this->validateName($player);

        $this->getPlayerDbRecord($player, "Unable to punish. %s was not found.");

        throw_if(empty($message), new RCONException("No reason provided."));

        return [
            'player' => $player,
            'message' => $message,
            'record' => $this->log($player, 'player_punish', $message, 0, false),
        ];
    }

    /**
     * Forgive player.
     *
     * @param string $player Name of player
     * @param string $message Message to be sent
     *
     * @return array
     * @throws \Throwable
     */
    public function forgive($player, $message): array
    {
        $this->validateName($player);

        $this->getPlayerDbRecord($player, 'Unable to forgive. %s was not found.');

        throw_if(empty($message), new RCONException("No reason provided."));

        return [
            'player' => $player,
            'message' => $message,
            'record' => $this->log($player, 'player_forgive', $message, 0, false),
        ];
    }

    /**
     * Mute player.
     *
     * @param string $player Name of player
     * @param string $message Message to be sent
     *
     * @return array
     * @throws \Throwable
     */
    public function mute($player, $message): array
    {
        $this->validateName($player);

        $this->getPlayerDbRecord($player, "Unable to mute. %s was not found.");

        throw_if(empty($message), new RCONException("No reason provided."));

        return [
            'player' => $player,
            'message' => $message,
            'record' => $this->log($player, 'player_mute', $message, 0, false),
        ];
    }
}