<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/21/20, 4:54 AM
 */

namespace App\Repository\Battlefield;


use App\Helpers\Main;
use App\Models\Battlefield\Server\Server as ServerDB;
use App\Repository\Base;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

/**
 * Class Server
 *
 * @package App\Repository\Battlefield
 */
class Server extends Base
{
    /**
     * @return Collection
     */
    public function listing(): Collection
    {
        return Cache::remember('dashboard.servers', Carbon::now()->addMinutes(2), static function () {
            /** @var Main $main */
            $main = app(Main::class);
            /** @var ServerDB $servers */
            $servers = ServerDB::with([
                'maps' => static function ($query) {
                    $query->where('TimeRoundEnd', '>=', Carbon::now()->subDay())
                        ->orderBy('TimeRoundEnd');
                },
            ])->active()
                ->orderBy('GameID')
                ->orderBy('ServerName')
                ->get();

            $population = new Collection([
                'used' => $servers->sum('usedSlots'),
                'total' => $servers->sum('maxSlots'),
            ]);

            $population->put('percentage', $main->percent($population->get('used'), $population->get('total')));

            return new Collection([
                'population' => $population,
                'servers' => $servers,
            ]);
        });
    }

    /**
     * @param  ServerDB  $server
     *
     * @return Collection
     */
    public function punishmentHierarchy(ServerDB $server): Collection
    {
        $server->load([
            'adkats' => function ($query) {
                $query->where('setting_name', '=', 'Punishment Hierarchy');
            },
        ]);

        return new Collection([
            'punishments' => $server->adkats->pluck('setting_value'),
            'total' => count($server->adkats->pluck('setting_value')[0]),
        ]);
    }

    /**
     * @return Collection
     */
    public function avgPlayersPerDay(): Collection
    {
        $average = Cache::remember('dashboard.player.sessions.dailyavg', Carbon::now()->addDay(), static function () {
            $average = DB::connection('mysql2')->table(static function ($query) {
                $query->selectRaw('COUNT(SessionID) AS daily_player_average')
                    ->from('tbl_sessions')
                    ->where('EndTime', '>=', Carbon::now()->subMonths(1))
                    ->groupBy(DB::raw('YEAR(EndTime) , MONTH(EndTime) , DAY(EndTime)'));
            }, 'k')->avg('daily_player_average');

            return !empty($average) ? round($average) : 0;
        });

        return new Collection([
            'avg_daily' => $average,
        ]);
    }
}