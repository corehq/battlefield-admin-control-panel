<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/1/19, 11:36 PM
 */

namespace App\Repository\Adkats;


use App\Models\Battlefield\Game;
use App\Models\Battlefield\Reputation as ReputationDB;
use App\Repository\Base;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

/**
 * Class Reputation
 *
 * @package App\Repository\Adkats
 */
class Reputation extends Base
{
    /**
     * @return Collection
     */
    public function top10(): Collection
    {
        $top10 = Cache::remember('adkats.reputation.top10.players', Carbon::now()->addDay(), function () {
            $data = ReputationDB::with('player')
                ->orderBy('total_rep_co', 'desc')
                ->take(10)
                ->get();

            return $data;
        });

        return $top10;
    }

    /**
     * @return Collection
     */
    public function top10ByGame(): Collection
    {
        $top10 = Cache::remember('adkats.reputation.top10.game', Carbon::now()->addDay(), function () {
            $data = Game::with([
                'reputations' => function ($query) {
                    $query->orderBy('total_rep_co', 'desc')
                        ->take(10);
                }, 'reputations.player',
            ])->get();

            return $data;
        });

        return $top10;
    }
}