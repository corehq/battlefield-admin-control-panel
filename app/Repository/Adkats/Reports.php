<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/18/20, 6:46 AM
 */

namespace App\Repository\Adkats;


use App\Models\Adkats\Record;
use App\Repository\Base;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

/**
 * Class Reports
 * @package App\Repository\Adkats
 */
class Reports extends Base
{
    /**
     * @return Collection
     */
    public function reportStats(): Collection
    {
        return Cache::remember('adkats.reports.stats', Carbon::now()->addDay(), function () {
            $ackedReports = Record::whereIn('command_type', [18, 20])
                ->whereIn('command_action', [19, 42, 62])
                ->count();

            $unAckedReports = Record::whereIn('command_type', [18, 20])
                ->whereNotIn('command_action', [19, 42, 62, 145])
                ->count();

            $expiredReports = Record::whereIn('command_type', [18, 20])
                ->where('command_action', '=', 145)
                ->count();

            $totalReports = Record::whereIn('command_type', [18, 20])
                ->count();

            $realTotal = ($totalReports - ($expiredReports == 0 ? $ackedReports : $expiredReports)) + $unAckedReports;

            return new Collection([
                'total' => $totalReports,
                'acked' => $ackedReports,
                'unacked' => $unAckedReports,
                'expired' => $expiredReports,
                'percentage_completed' => $this->helper->percent($ackedReports, $realTotal, 1),
            ]);
        });
    }

    /**
     * Returns the average reports done daily by players. This data is calculated over a 3 month period.
     *
     * @return Collection
     */
    public function avgDailyReports(): Collection
    {
        $average = Cache::remember('adkats.reports.dailyavg', Carbon::now()->addWeek(), static function () {
            $average = DB::connection('mysql2')->table(static function ($query) {
                $query->selectRaw('COUNT(record_id) AS daily_report_avg')
                    ->from('adkats_records_main')
                    ->whereIn('command_type', [18, 20])
                    ->where('record_time', '>=', Carbon::now()->subMonths(3))
                    ->groupBy(DB::raw('YEAR(record_time) , MONTH(record_time) , DAY(record_time)'));
            }, 'k')->avg('daily_report_avg');

            return !empty($average) ? round($average) : 0;
        });

        return new Collection([
            'avg_daily' => $average,
        ]);
    }
}