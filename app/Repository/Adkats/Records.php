<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/18/19, 6:52 AM
 */

namespace App\Repository\Adkats;


use App\Models\Adkats\Ban;
use App\Models\Adkats\Command;
use App\Models\Adkats\Record;
use App\Models\Battlefield\Player;
use App\Repository\Base;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

/**
 * Class Records
 * @package App\Repository\Adkats
 */
class Records extends Base
{
    /**
     * @return Collection
     */
    public function activity(): Collection
    {
        $feed = Cache::remember('adkats.records.activity', Carbon::now()->addMinutes(10), function () {
            $commands = Command::whereIn('command_access', ['Any', 'GlobalVisible'])
                ->where('command_active', 'Active')
                ->where('command_playerInteraction', 1)
                ->where('command_logging', '!=', 'Unable')
                ->pluck('command_id')
                ->toArray();

            $records = Record::with('type', 'action', 'server', 'source.adkats.user.role', 'target', 'source')
                ->whereIn('command_type', $commands)
                ->orderBy('record_id', 'desc')
                ->limit(6)
                ->get();

            return $records;
        });

        return $feed;
    }

    /**
     * @return int
     */
    public function killedPlayers(): int
    {
        $killed = Cache::remember('adkats.records.stats.killed', Carbon::now()->addDay(), function () {
            $query = Record::whereIn('command_action', [3, 4, 5, 54])
                ->whereNotNull('target_id')
                ->distinct()
                ->select('target_id')
                ->count();

            return $query;
        });

        return $killed;
    }

    /**
     * @return int
     */
    public function kickedPlayers(): int
    {
        $kicked = Cache::remember('adkats.records.stats.kicked', Carbon::now()->addDay(), function () {
            $query = Record::whereIn('command_action', [6])
                ->whereNotNull('target_id')
                ->whereNotIn('source_name', ['AFKManager', 'PingEnforcer'])
                ->distinct()
                ->select('target_id')
                ->count();

            return $query;
        });

        return $kicked;
    }

    /**
     * @return int
     */
    public function bannedPlayers(): int
    {
        $banned = Cache::remember('adkats.records.stats.banned.total', Carbon::now()->addDay(), function () {
            $query = Record::whereIn('command_action', [7, 8, 72, 73])
                ->whereNotNull('target_id')
                ->distinct()
                ->select('target_id')
                ->count();

            return $query;
        });

        return $banned;
    }

    /**
     * @return int
     */
    public function bannedActivePlayers(): int
    {
        $banned = Cache::remember('adkats.records.stats.banned.active', Carbon::now()->addDay(), function () {
            $query = Ban::where('ban_status', 'Active')
                ->where('ban_endTime', '>', Carbon::now())
                ->select('ban_id')
                ->count();

            return $query;
        });

        return $banned;
    }

    /**
     * @return Collection
     */
    public function feedback(): Collection
    {
        $feedback = Cache::remember('adkats.records.player.feedback', Carbon::now()->addMinutes(30), function () {
            $query = Record::with('server', 'source')
                ->where('command_type', '=', 118)
                ->get();

            return $query;
        });

        return $feedback;
    }

    public function recordsHeatmap(Player $player)
    {
        $sql = "SELECT 
    MONTH(record_time) AS 'month',
    MONTHNAME(record_time) AS 'monthname',
    YEAR(record_time) AS 'year',
    COUNT(record_id) AS 'total'
FROM
    procon.adkats_records_main
WHERE
    source_id = ?
        AND command_type IN (SELECT 
            command_id
        FROM
            procon.adkats_commands
        WHERE
            command_playerInteraction = 1
                AND command_active = 'Active')
GROUP BY YEAR(record_time) DESC , MONTH(record_time) ASC";

        /** @var Collection $records */
        $records = Record::selectRaw("MONTH(record_time) AS 'month', MONTHNAME(record_time) AS 'monthname', YEAR(record_time) AS 'year', COUNT(record_id) AS 'total'")
            ->where('source_id', '=', $player->PlayerID)
            ->whereIn('command_type', Command::admin()->type('Active')->pluck('command_id'))
            ->groupBy(DB::raw('YEAR(record_time), MONTH(record_time)'))
            ->get();

        $data = [];

        foreach ($records as $k => $v) {
            $data[] = [
                'name' => $v['year'],
                'data' => [
                    'x' => $v['monthname'],
                    'y' => $v['total'],
                ],
            ];
        }


        return $records;
    }
}
