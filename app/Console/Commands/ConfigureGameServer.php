<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/27/19, 3:08 AM
 */

namespace App\Console\Commands;

use App\Models\Battlefield\Server\Server;
use App\Models\Battlefield\Server\Setting;
use Illuminate\Console\Command;

class ConfigureGameServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bfacp:configure-gameserver {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Configure the BFACP for a game server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $server_id = $this->argument('id');

        $server = Server::findOrFail($server_id);

        $this->info(sprintf("Found server: [%s] %s", $server->game->Name, $server->ServerName));

        if (strtoupper($this->ask("Is this the correct server? [Y|N]", 'N')) != 'Y') {
            $this->warn("Incorrect server. Exiting");
            exit();
        }

        if (!$server->is_active) {
            if ($enableServer = strtoupper(
                    $this->ask("Would you like to enable this server to show on the BFACP? [Y|N]", 'Y')) != 'Y'
            ) {
                $this->warn("Will not be shown anywhere on the BFACP.");
                $server->ConnectionState = 'off';
            } else {
                $server->ConnectionState = 'on';
            }
        }

        do {
            $choice = $this->choice("What do you want to change?", [
                'RCON Password',
                'Battlelog GUID',
                'Uptime Robot Monitor Key',
                'Server Name Filter',
            ]);

            $settings = is_null($server->setting) ? Setting::firstOrNew(['server_id' => $server_id]) : $server->setting;

            switch ($choice) {
                case "RCON Password":
                    do {
                        $rconPasswod = $this->secret("Enter new RCON password");
                        if (!empty($rconPasswod)) {
                            $settings->rcon_password = $rconPasswod;
                        }
                    } while (empty($rconPasswod));
                    break;

                case "Battlelog GUID":
                    $battlelogGuid = $this->ask("Enter battlelog GUID");
                    $settings->battlelog_guid = $battlelogGuid;
                    break;

                case "Uptime Robot Monitor Key":
                    $uptimeRobot = $this->ask("Enter uptime robot monitor key");
                    $settings->uptime_robot_monitor_key = $uptimeRobot;
                    break;

                case "Server Name Filter":
                    $serverNameFilter = $this->ask("Enter the words you want to remove. Use a comma (,) to separate each entry.");
                    $settings->servername_filter = $serverNameFilter;
                    break;

                default:
                    $this->warn("Invalid Option. Exiting.");
                    exit();
            }

            $server->save();
            $settings->save();
        } while (strtoupper($this->ask("Would you like to do another action? [Y|N]", "N")) != 'N');
    }
}
