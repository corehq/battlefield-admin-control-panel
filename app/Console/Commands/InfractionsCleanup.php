<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/11/19, 6:08 AM
 */

namespace App\Console\Commands;

use App\Models\Adkats\Infractions\Overall;
use App\Models\Adkats\Infractions\Server;
use App\Models\Adkats\Record;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class InfractionsCleanup
 *
 * @package App\Console\Commands
 */
class InfractionsCleanup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bfacp:infractions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleans up infractions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {
        try {
            /** @var Collection $players */
            $players = Server::where('total_points', '<', 0)->get();

            $bar = $this->output->createProgressBar($players->count());

            DB::transaction(function () use ($bar, $players) {
                $players->each(function ($v, $k) use ($bar) {
                    // If a player doesn't have any punish points but they have forgive points then we will wipe all the forgive
                    // points on the server.
                    if ($v->punish_points == 0 && $v->forgive_points > 0) {
                        Record::where('server_id', '=', $v->server_id)
                            ->where('target_id', '=', $v->player_id)
                            ->where('command_type', '=', 10)
                            ->delete();
                    } else {
                        if ($v->forgive_points > $v->punish_points) {
                            $diff = abs($v->forgive_points - $v->punish_points);

                            Record::where('server_id', '=', $v->server_id)
                                ->where('target_id', '=', $v->player_id)
                                ->where('command_type', '=', 10)
                                ->orderBy('record_id', 'desc')
                                ->limit($diff)
                                ->delete();
                        }
                    }

                    $bar->advance();
                });

                $bar->finish();

                $this->info("\nWiping zeroed out infractions.");
                $total = Server::where('punish_points', '=', 0)
                    ->where('forgive_points', '=', 0)
                    ->where('total_points', '=', 0)
                    ->delete();

                $total += Overall::where('punish_points', '=', 0)
                    ->where('forgive_points', '=', 0)
                    ->where('total_points', '=', 0)
                    ->delete();

                $this->info('Total Records Deleted: '.number_format($total));
            });
        } catch (\Exception $e) {
            Log::error($e->getMessage(), $e->getTrace());
        };
    }
}
