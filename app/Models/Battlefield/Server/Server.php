<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/21/20, 1:45 PM
 */

namespace App\Models\Battlefield\Server;

use App\Helpers\Battlefield;
use App\Helpers\Main;
use App\Libraries\Battlelog\Server as BattlelogServer;
use App\Models\Adkats\Setting as AdkatsSetting;
use App\Models\Battlefield\Game;
use App\Models\Battlefield\Round;
use App\Models\Battlefield\Scoreboard\Scoreboard;
use App\Models\Battlefield\Scoreboard\Scores;
use App\Models\Elegant;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

/**
 * @property mixed maps_file_path
 * @property mixed port
 * @property mixed ip
 * @property mixed teams_file_path
 * @property mixed squads_file_path
 * @property mixed modes_file_path
 * @property mixed map_image_paths
 * @property mixed in_queue
 * @property mixed setting
 * @property mixed game
 * @property mixed ServerID
 * @property mixed IP_Address
 * @property mixed usedSlots
 * @property mixed maxSlots
 * @property mixed ServerName
 * @property mixed mapName
 * @property mixed Gamemode
 * @property mixed ConnectionState
 */
class Server extends Elegant
{
    /**
     * Should model handle timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'tbl_server';

    /**
     * Table primary key.
     *
     * @var string
     */
    protected $primaryKey = 'ServerID';

    /**
     * Fields not allowed to be mass assigned.
     *
     * @var array
     */
    protected $guarded = ['ServerID'];

    /**
     * Append custom attributes to output.
     *
     * @var array
     */
    protected $appends = [
        'percentage',
        'ip',
        'port',
        'server_name_short',
        'in_queue',
        'maps_file_path',
        'modes_file_path',
        'squads_file_path',
        'teams_file_path',
        'current_map',
        'current_gamemode',
        'map_image_paths',
        'is_active',
        'slug',
    ];

    /**
     * The attributes excluded form the models JSON response.
     *
     * @var array
     */
    protected $hidden = ['maps_file_path', 'modes_file_path', 'squads_file_path', 'teams_file_path'];

    /**
     * Models to be loaded automatically.
     *
     * @var array
     */
    protected $with = ['game', 'setting'];

    /**
     * @return BelongsTo
     */
    public function game(): BelongsTo
    {
        return $this->belongsTo(Game::class, 'GameID');
    }

    /**
     * @return BelongsTo
     */
    public function stats(): BelongsTo
    {
        return $this->belongsTo(Stats::class, 'ServerID');
    }

    /**
     * @return HasMany
     */
    public function maps(): HasMany
    {
        return $this->hasMany(Maps::class, 'ServerID');
    }

    /**
     * @return HasMany
     */
    public function rounds(): HasMany
    {
        return $this->hasMany(Round::class, 'server_id');
    }

    /**
     * @return HasMany
     */
    public function scoreboard(): HasMany
    {
        return $this->hasMany(Scoreboard::class, 'ServerID');
    }

    /**
     * @return HasOne
     */
    public function setting(): HasOne
    {
        return $this->hasOne(Setting::class, 'server_id');
    }

    /**
     * @return HasMany
     */
    public function scores(): HasMany
    {
        return $this->hasMany(Scores::class, 'ServerID');
    }

    /**
     * Returns all adkats settings for server.
     *
     * @return HasMany
     */
    public function adkats(): HasMany
    {
        return $this->hasMany(AdkatsSetting::class, 'server_id');
    }

    /**
     * Only return servers that should be active.
     *
     * @param  Builder  $query
     *
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('ConnectionState', 'on');
    }

    /**
     * Returns the server name with the strings that are
     * to be removed from it.
     *
     * @return string|null
     */
    public function getServerNameShortAttribute(): ?string
    {
        if (empty($this->setting->servername_filter)) {
            return null;
        }

        $strings = explode(',', $this->setting->servername_filter);

        return preg_replace('/\s+/', ' ', trim(str_replace($strings, null, $this->ServerName)));
    }

    /**
     * Calculates how full the server is represented by a percentage.
     *
     * @return float
     */
    public function getPercentageAttribute(): ?float
    {
        return (app(Main::class))->percent($this->usedSlots, $this->maxSlots);
    }

    /**
     * Gets the IP Address.
     *
     * @return string
     */
    public function getIPAttribute(): ?string
    {
        $host = explode(':', $this->IP_Address)[0];

        return gethostbyname($host);
    }

    /**
     * Gets the RCON port from the IP Address.
     *
     * @return int
     */
    public function getPortAttribute(): ?int
    {
        $port = explode(':', $this->IP_Address)[1];

        return (int) $port;
    }

    /**
     * Gets the human readable name of the current map.
     *
     * @return string
     */
    public function getCurrentMapAttribute(): string
    {
        return (app(Battlefield::class))->mapName($this->mapName, $this->maps_file_path, $this->Gamemode);
    }

    /**
     * Gets the human readable name of the current mode.
     *
     * @return string
     */
    public function getCurrentGamemodeAttribute(): string
    {
        return (app(Battlefield::class))->playmodeName($this->Gamemode, $this->modes_file_path);
    }

    /**
     * Gets the number of players currently in queue and caches the result for 5 minutes.
     *
     * @return int|null
     */
    public function getInQueueAttribute(): ?int
    {
        if ($this->setting === null) {
            return null;
        }

        return Cache::remember('server.'.$this->ServerID.'.queue', 5, function () {
            /** @var BattlelogServer $battlelog */
            $battlelog = app(BattlelogServer::class);

            return $battlelog->server($this)->inQueue();
        });
    }

    /**
     * Gets the path of the maps xml file.
     *
     * @return string
     */
    public function getMapsFilePathAttribute(): string
    {
        return app_path().DIRECTORY_SEPARATOR.'ThirdParty'.DIRECTORY_SEPARATOR.strtoupper($this->game->Name).DIRECTORY_SEPARATOR.'mapNames.xml';
    }

    /**
     * Gets the path of the gamemodes xml file.
     *
     * @return string
     */
    public function getModesFilePathAttribute(): string
    {
        return app_path().DIRECTORY_SEPARATOR.'ThirdParty'.DIRECTORY_SEPARATOR.strtoupper($this->game->Name).DIRECTORY_SEPARATOR.'playModes.xml';
    }

    /**
     * Gets the path of the squads xml file.
     *
     * @return string
     */
    public function getSquadsFilePathAttribute(): string
    {
        return app_path().DIRECTORY_SEPARATOR.'ThirdParty'.DIRECTORY_SEPARATOR.strtoupper($this->game->Name).DIRECTORY_SEPARATOR.'squadNames.xml';
    }

    /**
     * Gets the path of the teams xml file.
     *
     * @return string
     */
    public function getTeamsFilePathAttribute(): string
    {
        return app_path().DIRECTORY_SEPARATOR.'ThirdParty'.DIRECTORY_SEPARATOR.strtoupper($this->game->Name).DIRECTORY_SEPARATOR.'teamNames.xml';
    }

    /**
     * Gets the current map image banner.
     *
     * @return array
     */
    public function getMapImagePathsAttribute(): array
    {
        $base_path = sprintf('images/games/%s/maps', strtolower($this->game->Name));

        $ext = in_array($this->game->Name, ['BFHL', 'BFBC2']) ? 'png' : 'jpg';

        if ($this->game->Name === 'BFBC2') {
            $image = sprintf('%s.%s', strtolower(substr($this->mapName, 0, -2)), $ext);
        } else {
            $image = sprintf('%s.%s', strtolower($this->mapName), $ext);
        }

        $large = sprintf('%s/large/%s', $base_path, $image);

        $medium = sprintf('%s/medium/%s', $base_path, $image);

        $wide = sprintf('%s/wide/%s', $base_path, $image);

        return [
            'large' => $large,
            'medium' => $medium,
            'wide' => in_array($this->game->Name, ['BF4', 'BFHL', 'BFBC2']) ? $wide : $large,
        ];
    }

    /**
     * Is the server enabled?
     *
     * @return bool
     */
    public function getIsActiveAttribute(): bool
    {
        return $this->ConnectionState == 'on';
    }

    /**
     * @return string
     */
    public function getSlugAttribute(): string
    {
        return Str::slug($this->ServerName);
    }
}
