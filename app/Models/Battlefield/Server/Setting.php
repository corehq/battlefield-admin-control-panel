<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more
 * information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/14/19, 12:36 AM
 */

namespace App\Models\Battlefield\Server;

use App\Exceptions\RCONException;
use App\Models\Battlefield\Server\Server;
use App\Models\Elegant;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Crypt;

/**
 * Class Setting.
 */
class Setting extends Elegant
{
    /**
     * Should model handle timestamps.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Use prefix connection
     *
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'server_metadata';

    /**
     * Table primary key.
     *
     * @var string
     */
    protected $primaryKey = 'server_id';

    /**
     * Fields not allowed to be mass assigned.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes excluded form the models JSON response.
     *
     * @var array
     */
    protected $hidden = ['rcon_password', 'uptime_robot_monitor_key'];

    /**
     * Date fields to convert to carbon instances.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * Append custom attributes to output.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * Models to be loaded automatically.
     *
     * @var array
     */
    protected $with = [];

    /**
     * @return Model|BelongsTo
     */
    public function server()
    {
        return $this->belongsTo(Server::class, 'server_id');
    }

    /**
     * Decypts the RCON Password.
     *
     * @return string
     * @throws RCONException
     */
    public function getPassword()
    {
        if (empty($this->rcon_password)) {
            throw new RCONException('RCON password not set', 500);
        }

        try {
            return Crypt::decryptString($this->rcon_password);
        } catch (DecryptException $e) {
            throw new RCONException("Unable to decrypt RCON password. Did the encryption key change?", 500, $e);
        }
    }

    /**
     * Encrypts the password to be safely stored.
     *
     * @param string $value
     */
    public function setRconPasswordAttribute($value)
    {
        $this->attributes['rcon_password'] = Crypt::encryptString(trim($value));
    }
}
