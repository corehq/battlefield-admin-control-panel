<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/15/20, 10:03 AM
 */

namespace App\Models\Other\Plugins\EventLogger;

use App\Models\Elegant;

/**
 * Class Event
 *
 * This modal is for the procon plugin "Event Logger". Which can be downloaded from here.
 *
 * @link    https://myrcon.net/applications/core/interface/file/attachment.php?id=26
 *
 * @property-read int    ID
 * @property-read string gameserver
 * @property-read string event
 * @property-read int    timestamp
 * @property-read string playername
 * @property-read string msg
 *
 * @package App\Models\Other\Plugins\EventLogger
 */
class Event extends Elegant
{
    /**
     * Should model handle timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'event_logger';

    /**
     * Table primary key.
     *
     * @var string
     */
    protected $primaryKey = 'ID';

    /**
     * Fields not allowed to be mass assigned.
     *
     * @var array
     */
    protected $guarded = ['*'];

    /**
     * Date fields to convert to carbon instances.
     *
     * @var array
     */
    protected $dates = ['timestamp'];

    /**
     * Append custom attributes to output.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * Models to be loaded automatically.
     *
     * @var array
     */
    protected $with = [];
}