<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/31/19, 10:12 AM
 */

namespace App\Models\Adkats;

use App\Models\Battlefield\Player;
use App\Models\Battlefield\Server\Server;
use App\Models\Elegant;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Record.
 *
 * @property int      record_id
 * @property int      server_id
 * @property int      command_type
 * @property int      command_action
 * @property int      command_numeric
 * @property string   target_name
 * @property int|null target_id
 * @property string   source_name
 * @property int|null source_id
 * @property string   record_message
 * @property string   record_time
 * @property string   adkats_read
 * @property bool     adkats_web
 */
class Record extends Elegant
{
    /**
     * Should model handle timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'adkats_records_main';

    /**
     * Table primary key.
     *
     * @var string
     */
    protected $primaryKey = 'record_id';

    /**
     * Fields not allowed to be mass assigned.
     *
     * @var array
     */
    protected $guarded = ['record_id'];

    /**
     * Date fields to convert to carbon instances.
     *
     * @var array
     */
    protected $dates = ['record_time'];

    /**
     * Append custom attributes to output.
     *
     * @var array
     */
    protected $appends = ['is_web', 'stamp', 'source_url', 'target_url'];

    /**
     * Models to be loaded automatically.
     *
     * @var array
     */
    protected $with = [];

    /**
     * @return BelongsTo
     */
    public function target()
    {
        return $this->belongsTo(Player::class, 'target_id');
    }

    /**
     * @return BelongsTo
     */
    public function source()
    {
        return $this->belongsTo(Player::class, 'source_id');
    }

    /**
     * @return BelongsTo
     */
    public function server()
    {
        return $this->belongsTo(Server::class, 'server_id');
    }

    /**
     * @return BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(Command::class, 'command_type');
    }

    /**
     * @return BelongsTo
     */
    public function action()
    {
        return $this->belongsTo(Command::class, 'command_action');
    }

    /**
     * Was record issued from the web.
     *
     * @return bool
     */
    public function getIsWebAttribute()
    {
        if (!array_key_exists('adkats_web', $this->attributes)) {
            return null;
        }

        return $this->attributes['adkats_web'] == 1;
    }

    /**
     * @return mixed
     */
    public function getStampAttribute()
    {
        if (!array_key_exists('record_time', $this->attributes)) {
            return null;
        }

        return $this->record_time->toIso8601String();
    }

    /**
     * @param $value
     */
    public function setAdkatsReadAttribute($value)
    {
        $this->attributes['adkats_read'] = $value ? 'Y' : 'N';
    }

    /**
     * @param $value
     *
     * @return bool
     */
    public function getAdkatsReadAttribute($value)
    {
        return $value == 'Y';
    }

    /**
     * @return string|null
     */
    public function getTargetUrlAttribute()
    {
        if (empty($this->target_id)) {
            return null;
        }

        return route('player.profile', $this->target_id);
    }

    /**
     * @return string|null
     */
    public function getSourceUrlAttribute()
    {
        if (empty($this->source_id)) {
            return null;
        }

        return route('player.profile', $this->source_id);
    }
}
