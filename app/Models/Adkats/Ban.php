<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/14/20, 2:28 AM
 */

namespace App\Models\Adkats;

use App\Models\Battlefield\Player;
use App\Models\Elegant;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Ban.
 *
 * @property mixed record
 * @property mixed ban_endTime
 * @property mixed ban_startTime
 * @property int   latest_record_id
 */
class Ban extends Elegant
{
    /**
     * Should model handle timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'adkats_bans';

    /**
     * Table primary key.
     *
     * @var string
     */
    protected $primaryKey = 'ban_id';

    /**
     * Fields not allowed to be mass assigned.
     *
     * @var array
     */
    protected $guarded = ['ban_id', 'ban_sync'];

    /**
     * Date fields to convert to carbon instances.
     *
     * @var array
     */
    protected $dates = ['ban_startTime', 'ban_endTime'];

    /**
     * Append custom attributes to output.
     *
     * @var array
     */
    protected $appends = [
        'is_active',
        'is_expired',
        'is_unbanned',
        'is_perm',
        'ban_enforceName',
        'ban_enforceGUID',
        'ban_enforceIP',
        'ban_issued',
        'ban_expires',
        'color_class',
    ];

    /**
     * Models to be loaded automatically.
     *
     * @var array
     */
    protected $with = [];

    /**
     * @return BelongsTo
     */
    public function record(): BelongsTo
    {
        return $this->belongsTo(Record::class, 'latest_record_id');
    }

    /**
     * @return BelongsTo
     */
    public function player(): BelongsTo
    {
        return $this->belongsTo(Player::class, 'player_id');
    }

    /**
     * Only retrieve active bans.
     *
     * @param  Builder  $query
     *
     * @return Builder
     */
    public function scopeActive($query): Builder
    {
        return $query->where('ban_status', 'Active');
    }

    /**
     * Gets the bans done yesterday (UTC).
     *
     * @param  Builder  $query
     *
     * @return Builder
     */
    public function scopeYesterday($query): Builder
    {
        return $query->where('ban_startTime', '>=', Carbon::yesterday())
            ->where('ban_startTime', '<=', Carbon::today());
    }

    /**
     * Get the bans that the player ids have done.
     *
     * @param  object  $query
     * @param  array   $playerIds
     * @param  int     $limit
     *
     * @return Ban
     */
    public function scopePersonal($query, $playerIds = [], $limit = 30): Ban
    {
        if (empty($playerIds)) {
            return $this;
        }

        return $query->join('adkats_records_main', 'adkats_bans.latest_record_id', '=',
            'adkats_records_main.record_id')->whereIn('adkats_records_main.source_id',
            $playerIds)->take($limit);
    }

    /**
     * @return HasMany
     */
    public function previous(): HasMany
    {
        return $this->hasMany(Record::class, 'target_id', 'player_id')
            ->whereIn('command_action', [7, 8, 72, 73]);
    }

    /**
     * @return string
     */
    public function getBanIssuedAttribute(): string
    {
        return $this->ban_startTime->toIso8601String();
    }

    /**
     * @return string
     */
    public function getBanExpiresAttribute(): string
    {
        return $this->ban_endTime->toIso8601String();
    }

    /**
     * Is ban enforced by name.
     *
     * @return bool
     */
    public function getBanEnforceNameAttribute(): bool
    {
        return $this->attributes['ban_enforceName'] == 'Y';
    }

    /**
     * IS ban enforced by guid.
     *
     * @return bool
     */
    public function getBanEnforceGUIDAttribute(): bool
    {
        return $this->attributes['ban_enforceGUID'] == 'Y';
    }

    /**
     * Is ban enforced by ip.
     *
     * @return bool
     */
    public function getBanEnforceIPAttribute(): bool
    {
        return $this->attributes['ban_enforceIP'] == 'Y';
    }

    /**
     * Is ban active.
     *
     * @return bool
     */
    public function getIsActiveAttribute(): bool
    {
        return $this->attributes['ban_status'] == 'Active';
    }

    /**
     * Is ban expired.
     *
     * @return bool
     */
    public function getIsExpiredAttribute(): bool
    {
        return $this->attributes['ban_status'] == 'Expired';
    }

    /**
     * Is unbanned.
     *
     * @return bool
     */
    public function getIsUnbannedAttribute(): bool
    {
        if (array_key_exists('ban_status', $this->attributes)) {
            return $this->attributes['ban_status'] == 'Disabled' || $this->attributes['ban_status'] == 'Expired';
        }

        return false;
    }

    /**
     * Is ban permanent?
     *
     * @return bool
     */
    public function getIsPermAttribute(): bool
    {
        return $this->record->command_action == 8;
    }

    /**
     * @return array
     */
    public function getColorClassAttribute(): array
    {
        $data = [
            'frontend' => [
                'table' => '',
                'text' => 'text-info',
                'label' => 'label-info',
            ],
            'backend' => [
                'table' => '',
                'text' => 'text-info',
                'label' => 'label-info',
                'badge' => 'badge-info',
            ],
        ];

        if ($this->is_expired || !$this->is_active) {
            $data['frontend']['table'] = 'success';
            $data['frontend']['text'] = 'success';
            $data['frontend']['label'] = 'label-success';

            $data['backend']['table'] = 'success';
            $data['backend']['text'] = 'text-success';
            $data['backend']['label'] = 'label-success';
            $data['backend']['badge'] = 'badge-success';

        } else {
            if ($this->is_perm) {
                $data['frontend']['table'] = 'danger';
                $data['frontend']['text'] = 'danger';
                $data['frontend']['label'] = 'label-danger';

                $data['backend']['table'] = 'danger';
                $data['backend']['text'] = 'text-danger';
                $data['backend']['label'] = 'label-danger';
                $data['backend']['badge'] = 'badge-danger';
            }
        }

        return $data;
    }
}
