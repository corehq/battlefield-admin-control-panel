<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/7/20, 9:41 AM
 */

namespace App\Models\Adkats;

use App\Helpers\Main;
use App\Models\Battlefield\Game;
use App\Models\Battlefield\Player;
use App\Models\Battlefield\Server\Server;
use App\Models\Elegant;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

//use App\Models\Facades\Main as MainHelper;

/**
 * Class Special.
 * @property mixed player_expiration
 * @property mixed player_effective
 */
class Special extends Elegant
{
    /**
     * Should model handle timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'adkats_specialplayers';

    /**
     * Table primary key.
     *
     * @var string
     */
    protected $primaryKey = 'specialplayer_id';

    /**
     * Fields not allowed to be mass assigned.
     *
     * @var array
     */
    protected $guarded = ['specialplayer_id'];

    /**
     * Date fields to convert to carbon instances.
     *
     * @var array
     */
    protected $dates = ['player_effective', 'player_expiration'];

    /**
     * Append custom attributes to output.
     *
     * @var array
     */
    protected $appends = ['effective_stamp', 'expiration_stamp', 'group'];

    /**
     * Models to be loaded automatically.
     *
     * @var array
     */
    protected $with = [];

    /**
     * @return BelongsTo
     */
    public function player()
    {
        return $this->belongsTo(Player::class, 'player_id');
    }

    /**
     * @return BelongsTo
     */
    public function game()
    {
        return $this->belongsTo(Game::class, 'player_game');
    }

    /**
     * @return BelongsTo
     */
    public function server()
    {
        return $this->belongsTo(Server::class, 'server_id');
    }

    /**
     * @return mixed
     */
    public function getEffectiveStampAttribute()
    {
        return $this->player_effective->toIso8601String();
    }

    /**
     * @return mixed
     */
    public function getExpirationStampAttribute()
    {
        return $this->player_expiration->toIso8601String();
    }

    /**
     * @return mixed
     */
    public function getGroupAttribute()
    {
        return (app(Main::class))->specialGroups($this->player_group);
    }
}
