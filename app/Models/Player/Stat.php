<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/14/19, 4:08 AM
 */

namespace App\Models\Player;

use App\Models\Elegant;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Stat.
 * @property mixed LastSeenOnServer
 * @property mixed FirstSeenOnServer
 */
class Stat extends Elegant
{
    /**
     * Should model handle timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'tbl_playerstats';

    /**
     * Table primary key.
     *
     * @var string
     */
    protected $primaryKey = 'StatsID';

    /**
     * Fields not allowed to be mass assigned.
     *
     * @var array
     */
    protected $guarded = ['*'];

    /**
     * Date fields to convert to carbon instances.
     *
     * @var array
     */
    protected $dates = ['FirstSeenOnServer', 'LastSeenOnServer'];

    /**
     * Append custom attributes to output.
     *
     * @var array
     */
    protected $appends = ['first_seen', 'last_seen'];

    /**
     * Models to be loaded automatically.
     *
     * @var array
     */
    protected $with = [];

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function server()
    {
        return $this->belongsToMany(\App\Models\Battlefield\Server\Server::class, 'tbl_server_player', 'StatsID',
            'ServerID');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function weapons()
    {
        return $this->belongsToMany(Weapon::class, 'tbl_server_player', 'StatsID', 'StatsID');
    }

    /**
     * @return string
     */
    public function getFirstSeenAttribute()
    {
        return $this->FirstSeenOnServer->toIso8601String();
    }

    /**
     * @return string
     */
    public function getLastSeenAttribute()
    {
        return $this->LastSeenOnServer->toIso8601String();
    }
}
