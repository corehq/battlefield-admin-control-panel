<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/31/19, 9:05 AM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Elegant.
 */
class Elegant extends Model
{
    /**
     * Use mysql2 connection to disable prefix
     *
     * @var string
     */
    protected $connection = 'mysql2';
}
