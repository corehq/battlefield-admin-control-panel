<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/13/19, 11:24 PM
 */

namespace App\Http\Resources;

use App\Helpers\Main;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class ApiResource
 *
 * @package App\Http\Resources
 */
class StandardResponse extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection,
            'status' => 'ok',
            'meta' => [
                'execution' => (app(Main::class))->executionTime(),
            ],
        ];
    }
}
