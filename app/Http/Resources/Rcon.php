<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/20/19, 8:07 AM
 */

namespace App\Http\Resources;

use App\Helpers\Main;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class Rcon
 *
 * @package App\Http\Resources
 */
class Rcon extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection,
            'metadata' => [
                'execution' => (app(Main::class))->executionTime(),
            ],
        ];
    }
}
