<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/15/20, 10:29 PM
 */

namespace App\Http\Controllers\Battlefield;


use App\Http\Controllers\Controller;
use App\Http\Requests\Player\Search as SearchValidation;
use App\Models\Battlefield\Player;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Cache;

/**
 * Class PlayerController
 *
 * @package App\Http\Controllers\Battlefield
 */
class PlayerController extends Controller
{
    /**
     * @param  SearchValidation  $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(SearchValidation $request)
    {
        $players = Player::with('reputation', 'battlelog', 'ban.record');

        // Check if the search request is valid
        if ($request->hasAny(['playerName', 'playerIp', 'eaguid', 'pbguid']) && $request->validated()) {
            // Are we looking for players by name?
            if ($request->has('playerName') && !empty($request->get('playerName'))) {
                // Explode and trim the player name search query and filter any empty values
                $playersQuery = array_filter(array_map('trim', explode(',', $request->get('playerName'))));

                $isWildCard = $request->has('wildcard') && (int) $request->get('wildcard') === 1;

                // Loop over the names and build the query
                foreach ($playersQuery as $playerName) {
                    // Check if we need to preform a wildcard search or not.
                    if ($isWildCard) {
                        $queryString = '%'.$playerName.'%';
                        $searchOperator = 'LIKE';
                    } else {
                        $queryString = $playerName;
                        $searchOperator = '=';
                    }

                    // Check for previous names used
                    $players = $players->orWhereHas('aliases',
                        static function (Builder $query) use ($searchOperator, $queryString) {
                            $query->where('record_message', $searchOperator, $queryString);
                        });

                    // The main search
                    $players = $players->orWhere('SoldierName', $searchOperator, $queryString);
                }

                if (count($playersQuery) > 0) {
                    $eaguids = Player::groupBy('EAGUID');

                    foreach ($playersQuery as $playerName) {
                        // Check if we need to preform a wildcard search or not.
                        if ($isWildCard) {
                            $queryString = '%'.$playerName.'%';
                            $searchOperator = 'LIKE';
                        } else {
                            $queryString = $playerName;
                            $searchOperator = '=';
                        }

                        $eaguids = $eaguids->orWhere('SoldierName', $searchOperator, $queryString);
                    }

                    $players->orWhereIn('EAGUID', $eaguids->pluck('EAGUID'));
                }
            }

            // Are we looking for an IP?
            if ($request->has('playerIp') && !empty($request->get('playerIp'))) {
                $players = $players->orWhere('IP_Address', '=', $request->get('playerIp'));
            }

            // Are we looking for the EA GUID of a player?
            if ($request->has('eaguid') && !empty($request->get('eaguid'))) {
                $players = $players->orWhere('EAGUID', '=', $request->get('eaguid'));
            }

            // Are we looking for the Punkbuster GUID of a player?
            if ($request->has('pbguid') && !empty($request->get('pbguid'))) {
                $players = $players->orWhere('PBGUID', '=', $request->get('pbguid'));
            }
        }

        // Do a simple pagination query.
        $players = $players->simplePaginate(15);

        // Beep boop we're done, send transmission to starfleet command.
        return view('pages.battlefield.player.list', compact('players'));
    }

    /**
     * @param  Player  $player
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Player $player)
    {
        // Cache relationship data as it takes the most time.
        $player = Cache::remember($player->cacheKeys('web'), Carbon::now()->addMinutes(5),
            static function () use (
                $player
            ) {
                return $player->load(
                    'stats',
                    'specialGroups',
                    'adkats.user.role',
                    'reputation',
                    'battlelog'
                );
            });

        $previous = Player::where('PlayerID', '<', $player->PlayerID)->max('PlayerID');
        $next = Player::where('PlayerID', '>', $player->PlayerID)->min('PlayerID');

        return view('pages.battlefield.player.profile', compact(
            'player',
            'previous',
            'next'
        ));
    }
}