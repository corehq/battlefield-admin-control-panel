<?php

/*
 *  Google2FA: This file handles the 2FA logic.
 *  Enable 2FA, Validate 2FA password, Disable 2FA
 */

namespace App\Http\Controllers\Auth\Otp;

use Google2FA;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class OtpController extends Controller
{
    /*
     *  enable2fa: Generate google2fa_secret and inline QR code and then return to the view
     *  If the user already has google2fa_secret then it will use the same secret to generate the QR Code
     */
    public function enable2fa(Request $request)
    {
        $user = $request->user();
        if (!$user->google2fa_secret) {
            $user->update([
                'google2fa_secret' => $secret = Google2FA::generateSecretKey(),
            ]);
        }

        // generate the QR image
        $QRCode_img = Google2FA::getQRCodeInline(
            config('app.name'),
            $user->username,
            $user->google2fa_secret
        );

        // Pass the QR barcode image to our view.
        return view('2fa.enable2fa', [
            'QRCode_img' => $QRCode_img,
            'secret' => $user->google2fa_secret,
        ]);
    }


    /*
     *  validate2fa: Soon after scanning the QR code in mobile using the Authenticator app,
     *  User has to enter the otp from the authenticator app.
     *  This will validate the otp for the first time setup.
     *  If the user enters the wrong otp then they will be redirected back to validate2fa view.
     */
    public function validate2fa(Request $request)
    {
        $this->validate($request, [
            'otp' => 'required',
        ]);
        $user = $request->user();

        if (!Google2FA::verifyKey($user->google2fa_secret, $request->otp)) {
            return back()->with('error', 'Invalid OTP!');
        }

        $user->update([
            'google2fa_enabled' => true,
        ]);
        return redirect('/profile');

    }


    /*
     *  disable2fa: Disable the 2FA for the user
     *  Validate user's current password and then disable 2FA
     *  Set google2fa_enabled to false, google2fa_secret to null
     */
    public function disable2fa(Request $request)
    {
        $user = $request->user();
        $this->validate($request, [
            'password' => [
                'required',
                function ($attribute, $value, $fail) use ($request, $user) {
                    if (!Hash::check($value, $user->password)) {
                        $fail('Incorrect password');
                    }
                },
            ],
        ]);
        $user->update([
            'google2fa_secret' => null,
            'google2fa_enabled' => false,
        ]);

        return redirect('/dashboard')->with('success', '2FA disabled');
    }

}
