<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more
 * information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/3/19, 12:00 PM
 */

namespace App\Http\Controllers\Auth;

use App\User;
use Google2FA;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\View\View;

/**
 * Class LoginController
 *
 * @package App\Http\Controllers\Auth
 */
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    /*
     *  Post Authentication user will be logged out if they have 2fa enabled and
     *  redirect to otp page to validate
     *  update user id in the session for post otp verification login
     * */

    /**
     * @return Factory|View
     */
    public function displayLogin()
    {
        return view('auth.login');
    }

    /**
     * @return string
     */
    public function username()
    {
        return 'username';
    }

    /**
     * @return string
     */
    public function redirectTo()
    {
        return route('dashboard');
    }


    private function authenticated(Request $request, Authenticatable $user)
    {
        if ($user->google2fa_enabled) {
            Auth::logout();
            $request->session()->put('userId', $user->id);
            return redirect('otp');
        }
        return redirect()->intended($this->redirectTo);
    }

    /*
     *  Route to otp page.
     * */
    public function showotp()
    {
        if (session('userId')) {
            return view('2fa.otp');
        }
        return redirect('login');

    }

    /*
     *  Validate OTP entered by the user
     * */
    /**
     * @param  Request  $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validateotp(Request $request)
    {
        $this->validate($request, [
            'otp' => 'required | digits:6'
        ]);
        if(session('userId'))
        {
            $id =  $request->session()->pull('userId');
            $user = User::findOrFail($id);

            if(!Google2FA::verifyKey($user->google2fa_secret, $request->otp))
            {
                $request->session()->put('userId', $id);
                return  back()->with('otp', 'Incorrect OTP!');
            }

            Auth::loginUsingId($id);
            return redirect()->intended($this->redirectTo);
        }
        return redirect('login');

    }
}
