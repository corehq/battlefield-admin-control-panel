<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/3/19, 12:01 PM
 */

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Class DashboardController
 *
 * @package App\Http\Controllers
 */
class DashboardController extends Controller
{
    /**
     * @return Factory|View
     */
    public function __invoke()
    {
        $pageConfigs = [
            'pageHeader' => false
        ];

        if (request()->has('demo')) {
            return view('pages.dashboard-analytics', [
                'pageConfigs' => $pageConfigs,
            ]);
        }

        return view('pages.dashboard', [
            'pageConfigs' => $pageConfigs
        ]);
    }
}

