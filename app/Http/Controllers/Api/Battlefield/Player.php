<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/13/20, 2:14 AM
 */

namespace App\Http\Controllers\Api\Battlefield;


use App\Http\Controllers\Controller;
use App\Http\Resources\StandardResponse;
use App\Libraries\AntiCheat;
use App\Models\Battlefield\Player as PlayerDB;
use App\Models\Battlefield\Server\Server;
use App\Repository\Adkats\Setting as AdkatsSetting;
use App\Repository\Battlefield\Player\Player as PlayerRepo;

/**
 * Class Player
 *
 * @package App\Http\Controllers\Api\Battlefield
 */
class Player extends Controller
{
    /**
     * @param  PlayerDB  $player
     *
     * @return StandardResponse
     */
    public function commandUsage(PlayerDB $player): StandardResponse
    {
        /** @var PlayerRepo $repo */
        $repo = app(PlayerRepo::class);
        return new StandardResponse($repo->cmdOverviewChart($player));
    }

    /**
     * @param  PlayerDB  $player
     *
     * @return StandardResponse
     */
    public function linkedAccounts(PlayerDB $player): StandardResponse
    {
        /** @var PlayerRepo $repo */
        $repo = app(PlayerRepo::class);
        return new StandardResponse($repo->linkedAccounts($player));
    }

    /**
     * @param  PlayerDB  $player
     *
     * @return StandardResponse
     */
    public function history(PlayerDB $player): StandardResponse
    {
        /** @var PlayerRepo $repo */
        $repo = app(PlayerRepo::class);
        return new StandardResponse($repo->history($player));
    }

    /**
     * @param  PlayerDB  $player
     *
     * @return StandardResponse
     * @throws \App\Exceptions\BattlelogException
     * @throws \App\Exceptions\GameNotSupported
     */
    public function acs(PlayerDB $player): StandardResponse
    {
        $acs = new AntiCheat($player);

        return new StandardResponse($acs->parse()->get());
    }

    /**
     * @param  PlayerDB  $player
     *
     * @return StandardResponse
     */
    public function aliases(PlayerDB $player): StandardResponse
    {
        /** @var PlayerRepo $repo */
        $repo = app(PlayerRepo::class);
        return new StandardResponse($repo->aliases($player));
    }

    /**
     * @param  PlayerDB  $player
     *
     * @return StandardResponse
     */
    public function bans(PlayerDB $player): StandardResponse
    {
        $player->load(
            'ban.previous.server',
            'ban.previous.type',
            'ban.record.server'
        );

        $servers = (Server::where('GameID', '=', $player->GameID)->active()->get())->mapToGroups(static function ($v) {
            return [
                $v->game->Name => [
                    'ServerID' => $v->ServerID,
                    'ServerName' => $v->ServerName,
                ],
            ];
        });

        /** @var AdkatsSetting $adkats */
        $adkats = app(AdkatsSetting::class);
        $premessages = $adkats->predefinedMessages();

        $response = array_merge($player->only([
            'profile_url',
            'ban',
        ]), [
            'servers' => $servers,
            'premessages' => $premessages,
        ]);

        return new StandardResponse($response);
    }

    /**
     * @param  PlayerDB  $player
     *
     * @return StandardResponse
     */
    public function infractions(PlayerDB $player): StandardResponse
    {
        $player->load(
            'infractionsGlobal.history.server',
            'infractionsGlobal.history.action',
            'infractionsServer.server'
        );

        return new StandardResponse($player->only([
            'infractionsGlobal',
            'infractionsServer',
        ]));
    }
}