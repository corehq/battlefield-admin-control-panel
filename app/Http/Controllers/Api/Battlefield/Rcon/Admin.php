<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/27/19, 2:31 AM
 */

namespace App\Http\Controllers\Api\Battlefield\Rcon;


use App\Http\Controllers\Controller;
use App\Models\Battlefield\Player;
use App\Models\Battlefield\Server\Server;
use App\Repository\Battlefield\Scoreboard\Admin as AdminRcon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class Admin extends Controller
{
    /**
     * @var AdminRcon
     */
    private $rcon;

    /**
     * @var Request
     */
    private $request;

    /**
     * Admin constructor.
     *
     * @param Request $request
     */
    public function __construct()
    {
        parent::__construct();

        $this->request = app(Request::class);
    }

    /**
     * @param Server $server
     * @param Player $player
     *
     * @return \App\Models\Adkats\Ban
     * @throws \Throwable
     */
    public function ban(Server $server, Player $player)
    {
        $this->_validate([
            'message' => 'required',
            'duration' => 'required_if:type,temp|numeric|nullable',
            'type' => Rule::in(['temp', 'perm']),
        ]);

        $this->connectRcon($server);

        $message = $this->request->get('message');

        $duration = $this->request->get('duration', 0);

        $type = $this->request->get('type') == 'perm';

        $ban = $this->rcon->ban($player->SoldierName, $message, $duration, $type);

        return $ban;
    }

    /**
     * @param array $rules
     *
     * @return array
     */
    private function _validate(array $rules)
    {
        $validatedData = $this->request->validate($rules);

        return $validatedData;
    }

    /**
     * @param Server $server
     *
     * @return $this
     * @throws \Throwable
     */
    private function connectRcon(Server $server): Admin
    {
        $this->rcon = new AdminRcon($server);

        return $this;
    }
}