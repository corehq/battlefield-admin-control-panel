<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/11/19, 9:25 PM
 */

namespace App\Http\Controllers\Api\Battlefield;


use App\Http\Controllers\Controller;
use App\Http\Resources\StandardResponse;
use App\Models\Battlefield\Server\Server as ServerDB;
use App\Repository\Battlefield\Server as ServerRepo;
use Illuminate\Support\Collection;

/**
 * Class Server
 *
 * @package App\Http\Controllers\Api\Battlefield
 */
class Server extends Controller
{
    /**
     * @return StandardResponse
     */
    public function listing(): StandardResponse
    {
        $servers = ServerDB::active()->get();

        $population = new Collection([
            'used' => $servers->sum('usedSlots'),
            'total' => $servers->sum('maxSlots'),
        ]);

        return new StandardResponse(new Collection([
            'population' => $population,
            'servers' => $servers,
        ]));
    }

    /**
     * @param  ServerDB  $server
     *
     * @return StandardResponse
     */
    public function punishments(ServerDB $server): StandardResponse
    {
        $punishments = app(ServerRepo::class)->punishmentHierarchy($server);

        return new StandardResponse($punishments);
    }
}