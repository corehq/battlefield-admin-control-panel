<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/21/20, 4:51 AM
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Resources\StandardResponse;
use App\Repository\Adkats\Bans;
use App\Repository\Adkats\Records;
use App\Repository\Adkats\Reports;
use App\Repository\Adkats\Reputation;
use App\Repository\Battlefield\Player\Player;
use App\Repository\Battlefield\Server;
use Illuminate\Support\Collection;

/**
 * Class Dashboard
 *
 * @package App\Http\Controllers\Api
 */
class Dashboard extends Controller
{
    /**
     * @return StandardResponse
     */
    public function players()
    {
        /** @var Player $playerRepo */
        $playerRepo = app(Player::class);

        return new StandardResponse(new Collection([
            'history' => $playerRepo->newPlayersHistory(),
            'total' => $playerRepo->totalUniquePlayers(),
        ]));
    }

    /**
     * @return StandardResponse
     */
    public function bans()
    {
        /** @var Bans $banRepo */
        $banRepo = app(Bans::class);

        return new StandardResponse(new Collection([
            'yesterday' => $banRepo->yesterday(),
            'avg' => $banRepo->average(),
            'history' => $banRepo->bansIssuedGraph(),
        ]));
    }

    /**
     * @return StandardResponse
     */
    public function reports()
    {
        /** @var Reports $reportRepo */
        $reportRepo = app(Reports::class);

        return new StandardResponse($reportRepo->reportStats());
    }

    /**
     * @return StandardResponse
     */
    public function activityFeed()
    {
        /** @var Records $recordsRepo */
        $recordsRepo = app(Records::class);

        return new StandardResponse($recordsRepo->activity());
    }

    /**
     * @return StandardResponse
     */
    public function adkats()
    {
        /** @var Records $recordRepo */
        $recordRepo = app(Records::class);

        return new StandardResponse(new Collection([
            'killed_players' => $recordRepo->killedPlayers(),
            'kicked_players' => $recordRepo->kickedPlayers(),
            'banned_total' => $recordRepo->bannedPlayers(),
            'banned_active' => $recordRepo->bannedActivePlayers(),
        ]));
    }

    /**
     * @return StandardResponse
     */
    public function servers()
    {
        /** @var Server $serversRepo */
        $serversRepo = app(Server::class);

        return new StandardResponse($serversRepo->listing());
    }

    /**
     * @return StandardResponse
     */
    public function reputation()
    {
        /** @var Reputation $reputationRepo */
        $reputationRepo = app(Reputation::class);

        return new StandardResponse(new Collection([
            'players' => $reputationRepo->top10(),
        ]));
    }

    /**
     * @return StandardResponse
     */
    public function feedback(): StandardResponse
    {
        /** @var Records $recordRepo */
        $recordRepo = app(Records::class);

        return new StandardResponse(new Collection($recordRepo->feedback()));
    }

    /**
     * @return StandardResponse
     */
    public function avgDailyReport(): StandardResponse
    {
        /** @var Reports $reportRepo */
        $reportRepo = app(Reports::class);

        return new StandardResponse($reportRepo->avgDailyReports());
    }

    /**
     * @return StandardResponse
     */
    public function avgDailyPlayers(): StandardResponse
    {
        /** @var Server $serverRepo */
        $serverRepo = app(Server::class);

        return new StandardResponse($serverRepo->avgPlayersPerDay());
    }
}