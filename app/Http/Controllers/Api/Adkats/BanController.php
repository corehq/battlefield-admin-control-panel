<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/19/20, 1:42 AM
 */

namespace App\Http\Controllers\Api\Adkats;

use App\Http\Controllers\Controller;
use App\Http\Resources\StandardResponse;
use App\Models\Adkats\Ban;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class BanController
 *
 * @package App\Http\Controllers\Api\Adkats
 */
class BanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return StandardResponse
     */
    public function index(): StandardResponse
    {
        $bans = Ban::with('player', 'record.server')->active();
        $limit = 30;

        if (\request()->has('limit') && is_numeric(request()->get('limit'))) {
            $limit = \request()->get('limit');
        }

        return new StandardResponse($bans->paginate($limit));
    }

    /**
     * @return StandardResponse
     */
    public function latest(): StandardResponse
    {
        $limit = 30;

        if (request()->has('limit') && is_numeric(request()->get('limit'))) {
            $limit = request()->get('limit');
            if ($limit < 1) {
                $limit = 30;
            }
        }

        $bans = Ban::with('player', 'record.server')->active()->latest('ban_startTime')->take($limit)->get();

        return new StandardResponse($bans);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  Ban  $ban
     *
     * @return Response
     */
    public function show(Ban $ban)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Ban  $ban
     *
     * @return Response
     */
    public function edit(Ban $ban)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Ban      $ban
     *
     * @return Response
     */
    public function update(Request $request, Ban $ban)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Ban  $ban
     *
     * @return Response
     */
    public function destroy(Ban $ban)
    {
        //
    }
}
