<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/22/19, 5:08 AM
 */

namespace App\Http\Middleware;

use Closure;

class CheckIfInstallerExists
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Check if the file "installed" exists in the storage directory
        $installerFile = file_exists(storage_path('installed'));

        // Check the current route and see if it matches the installer
        $installerRoute = $request->routeIs([
                'LaravelInstaller::*',
                'LaravelUpdater::*',
            ]
        );

        // If the installer file doesn't exist and we are not any of the installer routes then redirect the user to the
        // installer wizard.
        if (!$installerFile && !$installerRoute) {
            return redirect('/install', 302);
        }

        return $next($request);
    }
}
