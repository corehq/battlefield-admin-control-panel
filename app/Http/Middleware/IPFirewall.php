<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/3/19, 4:50 AM
 */

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;

class IPFirewall
{
    /**
     * Handle an incoming request.
     *
     * Function from https://scotch.io/tutorials/protecting-laravel-sites-with-ip-intelligence
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ip = $request->ip();
        $insecureRequest = Cache::remember("firewall_$ip", Carbon::now()->addDay(), function () use ($ip) {
            // build parameters
            $key = config('services.ipapi.key');
            $url = "http://api.ipapi.com/api/{$ip}?access_key={$key}&security=1";

            // make request
            $client = new Client();
            $response = $client->request('GET', $url);
            $data = json_decode((string)$response->getBody(), true);
            if (!array_key_exists('security', $data)) {
                return false;
            }
            return $data['security']['threat_level'] === 'high' ?? false;
        });
        return $insecureRequest ? abort(403) : $next($request);
    }
}
