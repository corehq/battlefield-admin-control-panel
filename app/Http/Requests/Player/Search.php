<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/6/19, 1:57 AM
 */

namespace App\Http\Requests\Player;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class Search
 *
 * @package App\Http\Requests\Player
 */
class Search extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('view.player.listing');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'playerName' => 'nullable|min:3',
            'playerIp' => 'nullable|ip',
            'eaguid' => 'nullable|regex:/^EA_([0-9A-Z]{32}+)$/',
            'pbguid' => 'nullable|regex:/^([a-f0-9]{32}+)$/',
            'wildcard' => 'nullable|boolean',
        ];
    }
}
