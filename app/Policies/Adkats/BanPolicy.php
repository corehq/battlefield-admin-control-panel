<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/19/20, 1:47 AM
 */

namespace App\Policies\Adkats;

use App\Models\Adkats\Ban;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BanPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any bans.
     *
     * @param  \App\User  $user
     *
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the ban.
     *
     * @param  \App\User               $user
     * @param  \App\Models\Adkats\Ban  $ban
     *
     * @return mixed
     */
    public function view(User $user, Ban $ban)
    {
        //
    }

    /**
     * Determine whether the user can create bans.
     *
     * @param  \App\User  $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the ban.
     *
     * @param  \App\User               $user
     * @param  \App\Models\Adkats\Ban  $ban
     *
     * @return mixed
     */
    public function update(User $user, Ban $ban)
    {
        //
    }

    /**
     * Determine whether the user can delete the ban.
     *
     * @param  \App\User               $user
     * @param  \App\Models\Adkats\Ban  $ban
     *
     * @return mixed
     */
    public function delete(User $user, Ban $ban)
    {
        //
    }

    /**
     * Determine whether the user can restore the ban.
     *
     * @param  \App\User               $user
     * @param  \App\Models\Adkats\Ban  $ban
     *
     * @return mixed
     */
    public function restore(User $user, Ban $ban)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the ban.
     *
     * @param  \App\User               $user
     * @param  \App\Models\Adkats\Ban  $ban
     *
     * @return mixed
     */
    public function forceDelete(User $user, Ban $ban)
    {
        //
    }
}
