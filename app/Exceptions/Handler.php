<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/14/19, 2:26 AM
 */

namespace App\Exceptions;

use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param Exception $exception
     *
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        Log::error($exception->getMessage(), $exception->getTrace());
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  Request    $request
     * @param  Exception  $exception
     *
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function render($request, Exception $exception)
    {
        switch (get_class($exception)) {
            case RCONException::class:
            case BattlelogException::class:
            case InvalidPlayerName::class:
            case InvalidAdkatsCommand::class:
            case PlayerNotFound::class:
            case QueryException::class:
                return response()->json([
                    'data' => [],
                    'status' => 'error',
                    'message' => $exception->getMessage(),
                ], $exception->getCode());
                break;
        }

        return parent::render($request, $exception);
    }
}
