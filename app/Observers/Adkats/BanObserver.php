<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/27/19, 3:13 AM
 */

namespace App\Observers\Adkats;

use App\Models\Adkats\Ban;
use Illuminate\Support\Facades\Cache;

class BanObserver
{
    /**
     * Handle the ban "created" event.
     *
     * @param \App\Models\Adkats\Ban $ban
     *
     * @return void
     */
    public function created(Ban $ban)
    {
        Cache::forget('guest.home.latestbans');
    }

    /**
     * Handle the ban "updated" event.
     *
     * @param \App\Models\Adkats\Ban $ban
     *
     * @return void
     */
    public function updated(Ban $ban)
    {
        Cache::forget('guest.home.latestbans');
    }

    /**
     * Handle the ban "deleted" event.
     *
     * @param \App\Models\Adkats\Ban $ban
     *
     * @return void
     */
    public function deleted(Ban $ban)
    {
        //
    }

    /**
     * Handle the ban "restored" event.
     *
     * @param \App\Models\Adkats\Ban $ban
     *
     * @return void
     */
    public function restored(Ban $ban)
    {
        //
    }

    /**
     * Handle the ban "force deleted" event.
     *
     * @param \App\Models\Adkats\Ban $ban
     *
     * @return void
     */
    public function forceDeleted(Ban $ban)
    {
        //
    }
}
