<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/13/19, 11:22 PM
 */

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            ['key' => 'site.user.registration', 'content' => false, 'type' => 'boolean'],
            ['key' => 'site.bf4db.enable', 'content' => false, 'type' => 'boolean'],
            ['key' => 'site.bf4db.api_key', 'content' => null, 'type' => 'string'],
            ['key' => 'site.uptimerobot.enable', 'content' => false, 'type' => 'boolean'],
            ['key' => 'site.uptimerobot.api_key', 'content' => null, 'type' => 'string'],
            ['key' => 'site.ipapi.enable', 'content' => false, 'type' => 'boolean'],
            ['key' => 'site.ipapi.api_key', 'content' => null, 'type' => 'string'],
            ['key' => 'site.pusher.enable', 'content' => false, 'type' => 'boolean'],
            ['key' => 'site.pusher.api_key', 'content' => null, 'type' => 'string'],
            ['key' => 'site.pusher.app_id', 'content' => null, 'type' => 'numeric'],
            ['key' => 'site.pusher.app_secret', 'content' => null, 'type' => 'string'],
            ['key' => 'site.pusher.cluster', 'content' => 'us3', 'type' => 'string'],
        ];

        Setting::insert($settings);
    }
}
