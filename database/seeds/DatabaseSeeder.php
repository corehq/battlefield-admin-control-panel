<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/8/20, 3:18 PM
 */

use App\Models\Battlefield\Game;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $this->call(RolesAndPermissionsSeeder::class);
        //$this->call(SettingsSeeder::class);

        if (($env = env('APP_ENV')) !== null && $env === "testing") {
            $adkats = DB::connection('mysql2')->unprepared(
                file_get_contents(storage_path('sql/tests/adkats_tables_unit_test.sql')));
            $statlogger = DB::connection('mysql2')->unprepared(
                file_get_contents(storage_path('sql/tests/statlogger_tables_unit_test.sql')));

            $tables = [
                'tbl_games',
                'adkats_records_main',
            ];

            foreach ($tables as $table) {
                if (!Schema::connection('mysql2')->hasTable($table)) {
                    throw new \RuntimeException("Testing schema failed table check \"$table\". Aborting...");
                }
            }

            Game::create([
                'Name' => 'BF4',
            ]);
        }
    }
}
