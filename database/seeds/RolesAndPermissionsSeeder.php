<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/10/20, 2:31 AM
 */

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

/**
 * Class RolesAndPermissionsSeeder
 */
class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        foreach (self::permissionsList() as $permission) {
            Permission::updateOrCreate(['name' => $permission['name']], [
                'friendly_name' => $permission['friendly_name'],
                'description' => $permission['description'],
            ]);
        }

        $role = Role::updateOrCreate(['name' => 'Administrator']);
        $role->givePermissionTo(Permission::all());
        $defaultRole = Role::updateOrCreate(['name' => 'Default']);
        $defaultRole->givePermissionTo([
            'view.dashboard',
            'view.player.profile',
            'view.player.listing',
            'view.player.history',
        ]);
        $user = $this->createDefaultUser();
        $user->assignRole('Administrator');
    }

    /**
     * Creates the default administrator account.
     *
     * @return \App\User
     */
    private function createDefaultUser()
    {
        $user = new \App\User();
        $user->username = "admin";
        $user->display_name = "Administrator";
        $user->email = "example@example.com";
        $user->password = "admin";
        $user->api_token = Str::random(60);
        $user->email_verified_at = now();
        $user->terms_accept = true;
        $user->privacy_accept = true;
        $user->save();

        return $user;
    }

    /**
     * Application permissions
     *
     * @return array
     */
    public static function permissionsList()
    {
        $permissions = [
            [
                'name' => 'view.dashboard',
                'friendly_name' => 'View Dashboard',
                'description' => 'Allowed to view the dashboard',
            ],
            [
                'name' => 'view.player.profile',
                'friendly_name' => 'View Player Profile',
                'description' => 'Allowed to view the player profile',
            ],
            [
                'name' => 'view.player.listing',
                'friendly_name' => 'View Player Listing',
                'description' => 'Allowed to view the player listing',
            ],
            [
                'name' => 'view.chatlogs',
                'friendly_name' => 'View Chatlogs',
                'description' => 'Allowed to view the chat logs',
            ],
            [
                'name' => 'view.player.bans',
                'friendly_name' => 'View Player Bans',
                'description' => 'Allowed to view the player bans',
            ],
            [
                'name' => 'view.player.infractions',
                'friendly_name' => 'View Player Infractions',
                'description' => '',
            ],
            [
                'name' => 'view.player.ip',
                'friendly_name' => 'View Player IP',
                'description' => '',
            ],
            [
                'name' => 'view.player.eaguid',
                'friendly_name' => 'View Player EAGUID',
                'description' => '',
            ],
            [
                'name' => 'view.player.pbguid',
                'friendly_name' => 'View Player PBGUID',
                'description' => '',
            ],
            [
                'name' => 'view.player.history',
                'friendly_name' => 'View Player History',
                'description' => '',
            ],

            /**
             * RCON Permissions
             */
            [
                'name' => 'admin.rcon.ban.perm',
                'friendly_name' => 'Permanent Ban Player',
                'description' => '',
            ],
            [
                'name' => 'admin.rcon.ban.temp',
                'friendly_name' => 'Temporarily Ban Player',
                'description' => '',
            ],
            [
                'name' => 'admin.rcon.kill',
                'friendly_name' => 'Kill Player',
                'description' => '',
            ],
            [
                'name' => 'admin.rcon.tell',
                'friendly_name' => 'Tell Player',
                'description' => 'Used to send both a Say and Yell, independent of the actual Say or Yell command.',
            ],
            [
                'name' => 'admin.rcon.say',
                'friendly_name' => 'Say Server/Player',
                'description' => 'Sends a chat message to the player or server',
            ],
            [
                'name' => 'admin.rcon.yell',
                'friendly_name' => 'Yell Server/Player',
                'description' => 'Yell a message to the player or server',
            ],
            [
                'name' => 'admin.rcon.nuke',
                'friendly_name' => 'Nuke Team',
                'description' => 'Nuke a specific team',
            ],
            [
                'name' => 'admin.rcon.move',
                'friendly_name' => 'Player Move',
                'description' => 'Move a player to a different team and/or squad',
            ],
            [
                'name' => 'admin.rcon.kick',
                'friendly_name' => 'Kick Player',
                'description' => 'Kick a player from the server',
            ],
            [
                'name' => 'admin.rcon.punish',
                'friendly_name' => 'Punish Player',
                'description' => '',
            ],
            [
                'name' => 'admin.rcon.forgive',
                'friendly_name' => 'Forgive Player',
                'description' => '',
            ],
            [
                'name' => 'admin.rcon.mute',
                'friendly_name' => 'Mute Player',
                'description' => '',
            ],

            /**
             * AdKats Permissions
             */
            [
                'name' => 'admin.adkats.ban.perm',
                'friendly_name' => 'Permanent Ban Player',
                'description' => '',
            ],
            [
                'name' => 'admin.adkats.ban.temp',
                'friendly_name' => 'Temporarily Ban Player',
                'description' => '',
            ],
            [
                'name' => 'admin.adkats.kill',
                'friendly_name' => 'Kill Player',
                'description' => '',
            ],
            [
                'name' => 'admin.adkats.tell',
                'friendly_name' => 'Tell Player',
                'description' => 'Used to send both a Say and Yell, independent of the actual Say or Yell command.',
            ],
            [
                'name' => 'admin.adkats.say',
                'friendly_name' => 'Say Server/Player',
                'description' => 'Sends a chat message to the player or server',
            ],
            [
                'name' => 'admin.adkats.yell',
                'friendly_name' => 'Yell Server/Player',
                'description' => 'Yell a message to the player or server',
            ],
            [
                'name' => 'admin.adkats.nuke',
                'friendly_name' => 'Nuke Team',
                'description' => 'Nuke a specific team',
            ],
            [
                'name' => 'admin.adkats.move',
                'friendly_name' => 'Player Move',
                'description' => 'Move a player to a different team and/or squad',
            ],
            [
                'name' => 'admin.adkats.kick',
                'friendly_name' => 'Kick Player',
                'description' => 'Kick a player from the server',
            ],
            [
                'name' => 'admin.adkats.punish',
                'friendly_name' => 'Punish Player',
                'description' => '',
            ],
            [
                'name' => 'admin.adkats.forgive',
                'friendly_name' => 'Forgive Player',
                'description' => '',
            ],
            [
                'name' => 'admin.adkats.mute',
                'friendly_name' => 'Mute Player',
                'description' => '',
            ],
        ];

        return $permissions;
    }
}
