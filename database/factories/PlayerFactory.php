<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/14/19, 6:09 AM
 */

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Battlefield\Player;
use Faker\Generator as Faker;

$factory->define(Player::class, function (Faker $faker) {
    return [
        'GameID' => function ($player) {
            return \App\Models\Battlefield\Game::where('Name', 'BF4')->pluck('GameID');
        },
        'ClanTag' => $faker->randomLetter(),
        'SoldierName' => $faker->name(),
        'GlobalRank' => $faker->numberBetween(0, 150),
        'PBGUID' => $faker->md5(),
        'EAGUID' => 'EA_' . $faker->md5(),
        'IP_Address' => $faker->ipv4(),
        'CountryCode' => $faker->countryCode(),
        'created_at' => $faker->dateTime(),
        'updated_at' => $faker->dateTime(),
    ];
});
